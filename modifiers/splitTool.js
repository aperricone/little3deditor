function SplitTool()
{
	var tc = this;
	var item = main.contextMenu.AddItem("Split");
	item.onClick = function() { tc.Do(); };

	item.hide();

	selectorTool.AddChangedListener(function()
	{
		if(selectorTool.nSelected!=0 && selectorTool.mode==1)
		{
			item.show();
		} else
		{
			item.hide();
		}
	})
}

SplitTool.prototype.Do = function()
{
	var undo = new GeometriesModifyUndo();
	var dest = new Selection();
	var newSelection = new Selection(SelectorTool.ModeValue.VERTICES);
	for(var i = 0; i<dest.items.length; i++)
	{
		var item = dest.items[i];
		var geom = item.geom;
		for(var j = item.indices.length-1; j>=0; j--)
		{
			var edge = geom.edges[item.indices[j]];
			var newVertex = geom.AddVertex(V3(edge.GetPoint(0)).add(edge.GetPoint(1)).scale(0.5));
			newSelection.AddObj(geom, newVertex);
			var face = geom.faces[edge.faces[0]];
			face.indices.splice(face.indices.indexOf(edge.indices[1]), 0, newVertex);
			face.normals.splice(0, 0, V3());
			face = geom.faces[edge.faces[1]];
			face.indices.splice(face.indices.indexOf(edge.indices[0]), 0, newVertex);
			face.normals.splice(0, 0, V3());
			geom.edges[geom.AddEdge(edge.indices[0], newVertex)].hardness = edge.hardness;
			geom.edges[geom.AddEdge(newVertex, edge.indices[1])].hardness = edge.hardness;
		}
		geom.FillCrossReferences();
		geom.UpdateTriangles();
	}
	selectorTool.LoadSelection(newSelection);
	undo.EndModifier(true);
	main.Draw();
};

splitTool = new SplitTool();
