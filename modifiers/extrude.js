function ExtrudeTool()
{
	var tc = this;
	this.totalMovement = V3();
	var item = main.contextMenu.AddItem("Extrude");
	item.onClick = function() { tc.Do(); };
	item.hide();

	selectorTool.AddChangedListener(function()
	{
		// only face mode
		var show = ( selectorTool.mode==2 && selectorTool.nSelected!=0);
		show = show && (selectorTool.normal.dot(selectorTool.normal)>0);
		if(show)
		{
			item.show();
		} else
		{
			item.hide();
		}
	})
}

ExtrudeTool.prototype = new MoveTool(true);
ExtrudeTool.prototype.constructor = ExtrudeTool;

ExtrudeTool.prototype.Do = function()
{
	this.undo = new GeometriesModifyUndo();
	// Add new by copy for Selection?
	var faces = new Selection();
	this.dest = new Selection();
	this.dest.ChangeMode(0);

	var axis = selectorTool.normal;
	for(var i = 0; i<faces.items.length; i++)
	{
		var faceList = faces.items[i];
		var verticesList = this.dest.items[i];
		/** @type {Geometry} */
		var geometry = faceList.geom;
		var newIdxStart = geometry.vertices.length;
		// 1 - duplicate all vertices
		for(var j = 0; j<verticesList.indices.length; j++)
		{
			geometry.AddVertex(geometry.vertices[verticesList.indices[j]])
		}
		// 2 - for all edge with a face selected and 1 unselected create a face
		var nEdgeStart = geometry.edges.length;
		for(var j = 0; j<nEdgeStart; j++)
		{
			var edge = geometry.edges[j];
			if(geometry.faces[edge.faces[0]].selected!=geometry.faces[edge.faces[1]].selected)
			{
				var a = edge.indices[0];
				var b = edge.indices[1];
				var c = verticesList.indices.indexOf(a)+newIdxStart;
				var d = verticesList.indices.indexOf(b)+newIdxStart;
				if(geometry.faces[edge.faces[0]].selected)
				{
					geometry.AddFace(a, b, d, c)
				} else
				{
					geometry.AddFace(b, a, c, d)
				}
				var newEdge = geometry.edges[geometry.AddEdge(c,d)];
				newEdge.hardness = edge.hardness;
				edge.hardness = false;
			}
		}
		// 3 - change all selected faces ids
		for(var j = 0; j<faceList.indices.length; j++)
		{
			var face = geometry.faces[faceList.indices[j]];
			for(var k = 0; k<face.indices.length; k++)
			{
				var selectionIndex = verticesList.indices.indexOf(face.indices[k]);
				face.indices[k] = selectionIndex+newIdxStart;
			}
		}

		geometry.FillCrossReferences();
	}
	MoveTool.prototype.Do.call(this, axis);
};

ExtrudeTool.prototype.OnMouseMove = function(evt)
{
	MoveTool.prototype.OnMouseMove.call(this, evt);
	var len = this.totalMovement.dot(this.axis1);
	var text = "Extruded of "+len.toFixed(2);
	mouse.SetInfo(0, text, this);
};

ExtrudeTool.prototype.OnMouseUp = function(evt)
{
	if(MoveTool.prototype.OnMouseUp.call(this, evt))
	/*{ KEEP THIS CODE AS COMMENT
	 if( evt.which == 3 )
	 { // remove GeometriesModifyUndo
	 undoRedo.RemoveUndo();
	 }
	 else
	 { // remove MouseMoveUndo
	 undoRedo.RemoveUndo();
	 }*/
		undoRedo.RemoveUndo();

	if(evt.which!=3)
		this.undo.EndModifier(false);
};

extrude = new ExtrudeTool();
