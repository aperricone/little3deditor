function RotateTool()
{
	var tc = this;
	var item = main.contextMenu.AddItem("Rotate");
	var sb = item.AddSubMenu();
	sb.AddItem("X").onClick = function() { tc.Do(V3(1, 0, 0)); };
	sb.AddItem("Y").onClick = function() { tc.Do(V3(0, 1, 0)); };
	sb.AddItem("Z").onClick = function() { tc.Do(V3(0, 0, 1)); };
	sb.AddSeparator();
	var normal = sb.AddItem("Normal");
	normal.onClick = function() { tc.Do(selectorTool.normal); };
	sb.AddItem("Camera").onClick = function()
	{
		var cam = main.renderer.camera;
		tc.Do(cam.at);
	};
	item.hide();

	selectorTool.AddChangedListener(function()
	{
		if(selectorTool.normal.dot(selectorTool.normal)>0)
		{
			normal.show();
		} else
		{
			normal.hide();
		}
		if(selectorTool.nSelected==0)
			item.hide(); else
			item.show();
	})
}

RotateTool.prototype.Do = function(axis)
{
	this.dest = new Selection();
	this.dest.ChangeMode(0);
	var bb = this.dest.GetBBox();
	this.center = bb.center;
	this.axis = axis;
	mouse.LockMouse(this);
	this.lastMouse = undefined;
	this.totalRotation = 0;
};

RotateTool.prototype.Apply = function(dest, center, axis, angle)
{
	this.totalRotation += angle;
	if(dest.constructor!=Selection) throw "invalid parameter";
	if(dest.mode!=SelectorTool.ModeValue.VERTICES) throw "invalid parameter";
	var sa = Math.sin(angle);
	var ca = Math.cos(angle);
	var i = 1-ca;
	/*  Rodrigues' Rotation Formula
	 http://mathworld.wolfram.com/RodriguesRotationFormula.html
	 */
	var mat = [
		(axis.x*axis.x)*i+ca, (axis.x*axis.y)*i-axis.z*sa, (axis.x*axis.z)*i+axis.y*sa, (axis.y*axis.x)*i+axis.z*sa,
		(axis.y*axis.y)*i+ca, (axis.y*axis.z)*i-axis.x*sa, (axis.z*axis.x)*i-axis.y*sa, (axis.z*axis.y)*i+axis.x*sa,
		(axis.z*axis.z)*i+ca ];
	for(var i = 0; i<dest.items.length; i++)
	{
		/** @type {Geometry} */
		var geometry = dest.items[i].geom;
		var indices = dest.items[i].indices;
		for(var j = 0; j<indices.length; j++)
		{
			var vtx = geometry.vertices[indices[j]];
			vtx.sub(center);
			var exVal = V3(vtx);
			for(var k = 0; k<3; k++)
			{
				vtx[k] = 0;
				for(var l = 0; l<3; l++)
				{
					vtx[k] += exVal[l]*mat[l*3+k];
				}
			}
			vtx.add(center);
		}
		geometry.UpdateTriangles();
	}

	main.Draw();
};

RotateTool.prototype.OnMouseMove = function(evt)
{
	if(this.lastMouse==undefined)
	{
		this.lastMouse = { x: evt.clientX, y: evt.clientY };
		return;
	}
	var movementX = (evt.clientX-this.lastMouse.x);
	var movementY = (evt.clientY-this.lastMouse.y);
	this.lastMouse.x = evt.clientX;
	this.lastMouse.y = evt.clientY;
	var rotation = (movementX+movementY)/100;
	this.Apply(this.dest, this.center, this.axis, rotation);

	var text = "rotating of "+(this.totalRotation*180/Math.PI).toFixed(2)+" degrees";
	mouse.SetInfo(0, text, this);
};

RotateTool.Undo = function(dest, center, axis, angle)
{
	this.dest = dest;
	this.center = center;
	this.axis = axis;
	this.angle = angle;
	undoRedo.AddUndo(this);
};

RotateTool.Undo.prototype.Undo = function()
{
	rotate.Apply(this.dest, this.center, this.axis, -this.angle);
};

RotateTool.Undo.prototype.Redo = function()
{
	rotate.Apply(this.dest, this.center, this.axis, this.angle);
};

RotateTool.prototype.OnMouseUp = function(evt)
{
	if(this.lastMouse==undefined)
	{
		this.lastMouse = { x: evt.clientX, y: evt.clientY };
		return;
	}
	if(evt.which==3)
	{
		this.Apply(this.dest, this.center, this.axis, -this.totalRotation);
	} else
	{
		new RotateTool.Undo(this.dest, this.center, this.axis, this.totalRotation);
	}

	mouse.Unlock();
	this.totalRotation = V3();
};

rotate = new RotateTool();
