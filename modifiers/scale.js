function ScaleTool()
{
	var tc = this;
	var item = main.contextMenu.AddItem("Scale");
	var separator = main.contextMenu.AddSeparator();
	var sb = item.AddSubMenu();
	sb.AddItem("Uniform").onClick = function() { tc.Do(); };
	sb.AddSeparator();
	sb.AddItem("X").onClick = function() { tc.Do(V3(1, 0, 0)); };
	sb.AddItem("Y").onClick = function() { tc.Do(V3(0, 1, 0)); };
	sb.AddItem("Z").onClick = function() { tc.Do(V3(0, 0, 1)); };
	sb.AddSeparator();
	sb.AddItem("PlanarX - YZ").onClick = function() { tc.Do(V3(0, 1, 0), V3(0, 0, 1)); };
	sb.AddItem("PlanarY - XZ").onClick = function() { tc.Do(V3(1, 0, 0), V3(0, 0, 1)); };
	sb.AddItem("PlanarZ - XY").onClick = function() { tc.Do(V3(1, 0, 0), V3(0, 1, 0)); };
	sb.AddSeparator();
	var normal = sb.AddItem("Normal");
	normal.onClick = function() { tc.Do(selectorTool.normal); };
	var planarNormal = sb.AddItem("Planar normal");
	planarNormal.onClick = function()
	{

		var myRg, myUp;
		var selNormal = selectorTool.normal;
		if(Math.abs(selNormal.x)<0.9)
			myUp = V3().cross(selNormal, V3(1, 0, 0)).unit(); else
			myUp = V3().cross(selNormal, V3(0, 0, 1)).unit();
		myRg = V3().cross(selNormal, myUp).unit();
		tc.Do(myRg, myUp);
	};
	sb.AddSeparator();
	sb.AddItem("Camera").onClick = function()
	{
		var cam = main.renderer.camera;
		tc.Do(cam.at);
	};
	sb.AddItem("Planar camera").onClick = function()
	{
		var cam = main.renderer.camera;
		tc.Do(cam.rg, cam.up);
	};
	item.hide();
	separator.hide();

	selectorTool.AddChangedListener(function()
	{
		if(selectorTool.normal.dot(selectorTool.normal)>0)
		{
			normal.show();
			planarNormal.show();
		} else
		{
			normal.hide();
			planarNormal.hide();
		}
		if(selectorTool.nSelected==0)
		{
			item.hide();
			separator.hide();
		} else
		{
			item.show();
			separator.show();
		}
	})
}

/**
 *
 * @param [axis1]
 * @param [axis2]
 * @constructor
 */

ScaleTool.prototype.Do = function(axis1, axis2)
{
	this.dest = new Selection();
	this.dest.ChangeMode(0);
	var bb = this.dest.GetBBox();
	this.center = bb.center;
	this.axis1 = axis1;
	this.axis2 = axis2;
	mouse.LockMouse(this);
	this.lastMouse = undefined;
	this.currentScale = 1;
};

/**
 *
 * @param {Selection}dest
 * @param {_Vector}center
 * @param {Number}scale
 * @param {_Vector}axis1
 * @param {_Vector}[axis2]
 */
ScaleTool.prototype.Apply = function(dest, center, scale, axis1, axis2)
{
	if(dest.constructor!=Selection) throw "invalid parameter";
	if(dest.mode!=0) throw "invalid parameter";
	for(var i = 0; i<dest.items.length; i++)
	{
		var item = dest.items[i];
		/** @type {Geometry} */
		var geometry = item.geom;
		var indices = item.indices;
		var saveOriginal = !('original' in item);
		if(saveOriginal) item.original = [];
		for(var j = 0; j<indices.length; j++)
		{
			var vtx = geometry.vertices[indices[j]];
			if(saveOriginal)
			{
				item.original.push(V3(vtx));
			} else
			{
				vtx.x = item.original[j].x;
				vtx.y = item.original[j].y;
				vtx.z = item.original[j].z;
			}
			vtx.sub(center);
			if(axis2)
			{
				var dim1 = vtx.dot(axis1);
				var dim2 = vtx.dot(axis2);
				vtx.addScaled(axis1, dim1*(scale-1));
				vtx.addScaled(axis2, dim2*(scale-1));
			} else if(axis1)
			{
				var dim1 = vtx.dot(axis1);
				vtx.addScaled(axis1, dim1*(scale-1));
			} else
			{
				vtx.scale(scale);
			}
			vtx.add(center);
		}
		geometry.UpdateTriangles();
	}

	main.Draw();
};

ScaleTool.prototype.OnMouseMove = function(evt)
{
	if(this.lastMouse==undefined)
	{
		this.lastMouse = { x: evt.clientX, y: evt.clientY };
		return;
	}
	var movementX = (evt.clientX-this.lastMouse.x);
	var movementY = (evt.clientY-this.lastMouse.y);
	this.lastMouse.x = evt.clientX;
	this.lastMouse.y = evt.clientY;
	this.currentScale += (movementX+movementY)/100;
	this.Apply(this.dest, this.center, this.currentScale, this.axis1, this.axis2);

	var text = "scaling of "+(this.currentScale*100).toFixed(2)+"%";
	mouse.SetInfo(0, text, this);
};

ScaleTool.prototype.OnMouseUp = function(evt)
{
	if(this.lastMouse==undefined)
	{
		this.lastMouse = { x: evt.clientX, y: evt.clientY };
		return;
	}
	if(evt.which==3)
	{
		this.Apply(this.dest, this.center, 1, this.axis1, this.axis2);
	} else
	{
		new ScaleTool.Undo(this.dest, this.center, this.currentScale, this.axis1, this.axis2);
	}

	mouse.Unlock();
};

//region ScaleTool.Undo

ScaleTool.Undo = function(dest, center, scale, axis1, axis2)
{
	this.dest = dest;
	this.center = center;
	this.scale = scale;
	this.axis1 = axis1;
	this.axis2 = axis2;
	undoRedo.AddUndo(this);
};

ScaleTool.Undo.prototype.Undo = function()
{
	scale.Apply(this.dest, this.center, 1, this.axis1, this.axis2);
};

ScaleTool.Undo.prototype.Redo = function()
{
	scale.Apply(this.dest, this.center, this.scale, this.axis1, this.axis2);
};

// endregion

scale = new ScaleTool();
