function ConnectTool()
{
	var tc = this;
	var item = main.contextMenu.AddItem("Connect");
	item.onClick = function() { tc.Do(); };

	item.hide();

	selectorTool.AddChangedListener(function()
	{
		if(selectorTool.nSelected!=0 && selectorTool.mode==0)
		{
			item.show();
			item.enable = ( selectorTool.nSelected>=2);
		} else
		{
			item.hide();
		}
	})
}

ConnectTool.prototype.Do = function()
{
	var undo = new GeometriesModifyUndo();
	var dest = new Selection();
	var newSelection = new Selection(SelectorTool.ModeValue.EDGES);
	for(var i = 0; i<dest.items.length; i++)
	{
		var item = dest.items[i];
		var geom = item.geom;
		var oneDone = false;
		for(var j = 0; j<item.indices.length; j++)
			for(var k = j+1; k<item.indices.length; k++)
			{
				var vIdx = [item.indices[j], item.indices[k]];
				var vertices = [ geom.vertices[ vIdx[0]], geom.vertices[ vIdx[1]]];
				if(geom.GetEdge(vIdx[0], vIdx[1])== -1)
				{
					// check for common faces
					var faceId = -1;
					for(var l = 0; l<vertices[0].faces.length; l++)
					{
						if(vertices[1].faces.indexOf(vertices[0].faces[l])!= -1)
						{
							faceId = vertices[0].faces[l];
							break;
						}
					}
					if(faceId!= -1)
					{
						oneDone = true;
						var face = geom.faces[faceId];
						var aId = face.indices.indexOf(vIdx[0]);
						var bId = face.indices.indexOf(vIdx[1]);
						if(aId>bId)
						{
							var tmp = aId;
							aId = bId;
							bId = tmp;
						}
						var newIndices = face.indices.splice(aId+1, bId-aId-1);
						face.dirty = true;
						newIndices.push(face.indices[aId+1]);
						newIndices.push(face.indices[aId]);
						geom.AddFace(newIndices);

						newSelection.AddObj(geom, geom.AddEdge(vIdx[0], vIdx[1]));
						geom.FillCrossReferences();
					}
				}
			}
		if(oneDone)
		{
			geom.UpdateTriangles();
		}
	}
	selectorTool.LoadSelection(newSelection);
	undo.EndModifier(true);
	main.Draw();
};

connectTool = new ConnectTool();
