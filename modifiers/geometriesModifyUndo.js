function GeometriesModifyUndo()
{
	undoRedo.AddUndo(this);
	this.oldGeometries = [];
	this.newGeometries = [];
	this.geometriesIndices = [];
	this.oldSel = new Selection();
	this.newSel = undefined;
	for(var i = 0; i<this.oldSel.items.length; i++)
	{
		var item = this.oldSel.items[i];
		var geom = item.geom;
		this.geometriesIndices.push(main.geometries.indexOf(geom));
		var copyForOldGeom = new Geometry(main.renderer);
		copyForOldGeom.Copy(geom);
		this.oldGeometries.push(copyForOldGeom);
		this.newGeometries.push(geom);
	}
}

GeometriesModifyUndo.prototype.EndModifier = function(selectionChanged)
{
	if(selectionChanged)
		this.newSel = new Selection();
	for(var i = 0; i<this.oldSel.items.length; i++)
	{
		var item = this.oldSel.items[i];
		var geom = item.geom;
		var copyForNewGeom = new Geometry(main.renderer);
		copyForNewGeom.Copy(geom);
		this.newGeometries[i] = copyForNewGeom;
	}
};


GeometriesModifyUndo.prototype.Undo = function()
{
	for(var i = 0; i<this.geometriesIndices.length; i++)
	{
		main.geometries[this.geometriesIndices[i]].Copy(this.oldGeometries[i]);
	}
	if(this.newSel!=undefined)
		selectorTool.LoadSelection(this.oldSel);
	main.Draw();
};

GeometriesModifyUndo.prototype.Redo = function()
{
	for(var i = 0; i<this.geometriesIndices.length; i++)
	{
		main.geometries[this.geometriesIndices[i]].Copy(this.newGeometries[i]);
	}
	if(this.newSel!=undefined)
		selectorTool.LoadSelection(this.newSel);
	main.Draw();
};
