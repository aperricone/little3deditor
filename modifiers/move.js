function MoveTool(derived)
{
	if(derived) return;
	this.totalMovement = V3();
	var tc = this;
	var item = main.contextMenu.AddItem("Move");
	var sb = item.AddSubMenu();
	sb.AddItem("X").onClick = function() { tc.Do(V3(1, 0, 0)); };
	sb.AddItem("Y").onClick = function() { tc.Do(V3(0, 1, 0)); };
	sb.AddItem("Z").onClick = function() { tc.Do(V3(0, 0, 1)); };
	sb.AddSeparator();
	sb.AddItem("PlanarX - YZ").onClick = function() { tc.Do(V3(0, 1, 0), V3(0, 0, 1)); };
	sb.AddItem("PlanarY - XZ").onClick = function() { tc.Do(V3(1, 0, 0), V3(0, 0, 1)); };
	sb.AddItem("PlanarZ - XY").onClick = function() { tc.Do(V3(1, 0, 0), V3(0, 1, 0)); };
	sb.AddSeparator();
	var normal = sb.AddItem("Normal");
	normal.onClick = function() { tc.Do(selectorTool.normal); };
	var planarNormal = sb.AddItem("Planar Normal");
	planarNormal.onClick = function()
	{
		var cam = main.renderer.camera;
		var myRg;
		var selNormal = selectorTool.normal;
		var myUp = V3().cross(selNormal, cam.rg).unit();
		if(myUp.dot(myUp)==0)
		{
			myRg = V3().cross(selNormal, cam.up).unit();
			myUp = V3().cross(selNormal, myRg).unit();
		} else
		{
			myRg = V3().cross(selNormal, myUp).unit();
		}
		tc.Do(myRg, myUp);
	};
	sb.AddSeparator();
	sb.AddItem("Free").onClick = function()
	{
		var cam = main.renderer.camera;
		tc.Do(cam.rg, cam.up);
	};
	item.hide();

	selectorTool.AddChangedListener(function()
	{
		if(selectorTool.normal.dot(selectorTool.normal)>0)
		{
			planarNormal.show();
			normal.show();
		} else
		{
			planarNormal.hide();
			normal.hide();
		}
		if(selectorTool.nSelected==0)
			item.hide(); else
			item.show();
	})
}

MoveTool.prototype.Do = function(axis1, axis2)
{
	this.dest = new Selection();
	this.dest.ChangeMode(0);
	axis2 = axis2==undefined ? axis1 : axis2;
	var cam = main.renderer.camera;
	var rg1 = axis1.dot(cam.rg);
	var rg2 = axis2.dot(cam.rg);
	if(Math.abs(rg2)>Math.abs(rg1))
	{
		var tmp = axis2;
		axis2 = axis1;
		axis1 = tmp;
		rg1 = rg2;
	}
	if(rg1<0) axis1.scale(-1);
	if(axis2.dot(cam.up)<0) axis2.scale(-1);
	this.axis1 = axis1;
	this.axis2 = axis2;
	keyMan.SetListener(this, 9); // TAB
	this.lastMouse = undefined;
	mouse.LockMouse(this);
	this.totalMovement.Set();
};

MoveTool.prototype.Apply = function(dest, movement)
{
	if(movement)
	{
		this.totalMovement.add(movement);
	}
	if(dest.constructor!=Selection) throw "invalid parameter";
	if(dest.mode!=SelectorTool.ModeValue.VERTICES) throw "invalid parameter";
	for(var i = 0; i<dest.items.length; i++)
	{
		var item = dest.items[i];
		/** @type {Geometry} */
		var geometry = item.geom;
		var indices = item.indices;
		var saveOriginal = !('original' in item);
		if(saveOriginal) item.original = [];
		for(var j = 0; j<indices.length; j++)
		{
			var vtx = geometry.vertices[indices[j]];
			if(saveOriginal)
			{
				item.original.push(V3(vtx));
			} else
			{
				vtx.x = item.original[j].x;
				vtx.y = item.original[j].y;
				vtx.z = item.original[j].z;
			}
			vtx.add(this.totalMovement);
		}
		geometry.UpdateTriangles();
	}

	main.Draw();
};

MoveTool.prototype.OnKeyDown = function()
{
	var tc = this;
	mouse.Unlock();
	simpleNumericForm.title = "Move";
	simpleNumericForm.AddNumericInput("X Movement", this.totalMovement.x, 0.1);
	simpleNumericForm.AddNumericInput("Y Movement", this.totalMovement.y, 0.1);
	simpleNumericForm.AddNumericInput("Z Movement", this.totalMovement.z, 0.1);
	simpleNumericForm.onchange = function(w)
	{
		tc.totalMovement[w] = simpleNumericForm.GetValue(w);
		tc.Apply(tc.dest);
	}
	simpleNumericForm.onOK = function()
	{
		tc.totalMovement.x = simpleNumericForm.GetValue(0);
		tc.totalMovement.y = simpleNumericForm.GetValue(1);
		tc.totalMovement.z = simpleNumericForm.GetValue(2);

		tc.OnMouseUp({which: 1});
	};
	simpleNumericForm.onCancel = function()
	{
		this.lastMouse = undefined;
		mouse.LockMouse(this);
	}
	simpleNumericForm.Show();
};


MoveTool.prototype.OnMouseMove = function(evt)
{
	if(this.lastMouse==undefined)
	{
		this.lastMouse = { x: evt.clientX, y: evt.clientY };
		return;
	}
	var movementX = (evt.clientX-this.lastMouse.x);
	var movementY = (evt.clientY-this.lastMouse.y);
	this.lastMouse.x = evt.clientX;
	this.lastMouse.y = evt.clientY;
	var movement = V3();
	if(movementX!=0) movement.addScaled(this.axis1, movementX/100);
	if(movementY!=0) movement.addScaled(this.axis2, -movementY/100);
	this.Apply(this.dest, movement);

	var text = "moving of "+this.totalMovement.toFixed(2);
	mouse.SetInfo(0, text, this);
};

MoveTool.Undo = function(dest, movement)
{
	this.dest = dest;
	this.movement = movement;
	undoRedo.AddUndo(this);
};

MoveTool.Undo.prototype.Undo = function()
{
	move.totalMovement.Set();
	move.Apply(this.dest)
};

MoveTool.Undo.prototype.Redo = function()
{
	move.totalMovement.Set();
	move.Apply(this.dest, V3(this.movement))
};

/**
 * @return {boolean}
 */
MoveTool.prototype.OnMouseUp = function(evt)
{
	if(this.lastMouse==undefined)
	{
		this.lastMouse = { x: evt.clientX, y: evt.clientY };
		return false;
	}
	if(evt.which==3)
	{
		this.Apply(this.dest, V3(this.totalMovement).scale(-1));
	} else
	{
		new MoveTool.Undo(this.dest, V3(this.totalMovement));
	}
	keyMan.RemoveListener(9);
	mouse.Unlock();
	this.totalMovement.Set();
	return true;
};

move = new MoveTool();
