function DissolveTool()
{
	var tc = this;
	var item = main.contextMenu.AddItem("Dissolve");
	item.onClick = function() { tc.Do(); };

	item.hide();

	selectorTool.AddChangedListener(function()
	{
		if(selectorTool.nSelected!=0)
		{
			item.show();
		} else
		{
			item.hide();
		}
	})
}

DissolveTool.prototype.DoGeometries = function(dest)
{
	var geometries = [];
	for(var i = 0; i<dest.items.length; i++)
	{
		geometries.push(dest.items[i].geom);
	}
	var undo = new RemoveUndo(geometries);
	undo.Redo();
};

DissolveTool.prototype.DoVertices = function(dest)
{
	for(var i = 0; i<dest.items.length; i++)
	{
		var item = dest.items[i];
		var geom = item.geom;
		for(var j = item.indices.length-1; j>=0; j--)
		{
			var idx = item.indices[j];
			geom.RemoveVertex(idx);
		}
		geom.FillCrossReferences();
		geom.UpdateTriangles();
	}
	selectorTool.Clear();
};

DissolveTool.prototype.DoEdges = function(dest)
{
	for(var i = 0; i<dest.items.length; i++)
	{
		var item = dest.items[i];
		var geom = item.geom;
		for(var j = item.indices.length-1; j>=0; j--)
		{
			var idx = item.indices[j];
			var edge = geom.edges[idx];
			if( edge.faces[0] != edge.faces[1] )
			{
				var faces = [];
				var facesIdx = [];
				for(var k = 0; k<2; k++)
				{
					faces.push( geom.faces[edge.faces[k]] );
					facesIdx.push( faces[k].indices.indexOf(edge.indices[1-k]) );
				}
				var newFace = [];
				for(var k = 0; k<2; k++)
				{
					var l = facesIdx[k];
					do {
						newFace.push(faces[k].indices[l]);
						l++;
						if(l==faces[k].indices.length) l = 0;
					} while(l!=facesIdx[k]);
					newFace.pop();
				}
				var firstRemove = 0;
				if(edge.faces[0]<edge.faces[1]) firstRemove = 1;
				geom.faces.splice(edge.faces[firstRemove], 1);
				geom.faces.splice(edge.faces[1-firstRemove], 1);
				geom.AddFace(newFace);
			} else
			{
				var face = geom.faces[edge.faces[0]];
				var facesIdx = [];
				for(var k = 0; k<2; k++)
				{
					facesIdx.push( face.indices.indexOf(edge.indices[1-k]) );
				}
				var startIdx = facesIdx[0] < facesIdx[1] ? facesIdx[0] : facesIdx[1];
				var endIdx = startIdx+2;
				if( endIdx >= face.indices.length ||
					face.indices[endIdx]!=face.indices[startIdx] )
				{
					endIdx = facesIdx[0] > facesIdx[1] ? facesIdx[0] : facesIdx[1];
					startIdx = endIdx-2;
					if(startIdx < 0 ) startIdx+= face.indices.length;
				}
				if( face.indices[endIdx]==face.indices[startIdx] )
				{
					if(startIdx < endIdx )
						face.indices.splice(startIdx,endIdx-startIdx-1);
					else
					{
						face.indices.splice(startIdx);
						face.indices.splice(0,endIdx);
					}
				}
			}
			geom.FillCrossReferences();

		}
		geom.UpdateTriangles();
	}
	selectorTool.Clear();
};

DissolveTool.prototype.Do = function()
{
	var dest = new Selection();
	if(selectorTool.mode==SelectorTool.ModeValue.GEOMETRIES)
	{
		this.DoGeometries(dest);
	} else
	{
		var undo = new GeometriesModifyUndo();
		if(dest.mode==SelectorTool.ModeValue.VERTICES)
		{
			this.DoVertices(dest);
		} else if(dest.mode==SelectorTool.ModeValue.EDGES)
		{
			this.DoEdges(dest);
		} else if(dest.mode==SelectorTool.ModeValue.FACES)
		{
			dest.ChangeMode(SelectorTool.ModeValue.VERTICES);
			this.DoVertices(dest);
		}
		undo.EndModifier(true);
	}
	main.Draw();
};

dissolve = new DissolveTool();
