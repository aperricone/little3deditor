function SetHardnessTool()
{
	var tc = this;
	var item = main.contextMenu.AddItem("Hardness");
	var sb = item.AddSubMenu();
	sb.AddItem("Soft").onClick = function() { tc.Do(false); };
	sb.AddItem("Hard").onClick = function() { tc.Do(true); };

	item.hide();

	selectorTool.AddChangedListener(function()
	{
		if(selectorTool.nSelected!=0 && selectorTool.mode==1)
		{
			item.show();
		} else
		{
			item.hide();
		}
	});
}

SetHardnessTool.prototype.Do = function(how)
{
	this.dest = new Selection();
	new SetHardnessTool.Undo(this.dest, how);
	this.Apply(this.dest, how);
};

SetHardnessTool.prototype.Apply = function(dest, how)
{
	for(var i = 0; i<dest.items.length; i++)
	{
		var item = dest.items[i];
		var geom = item.geom;
		for(var j = 0; j<item.indices.length; j++)
		{
			var edge = geom.edges[item.indices[j]];
			edge.hardness = how;
			geom.faces[ edge.faces[0] ].dirty = true;
			geom.faces[ edge.faces[1] ].dirty = true;
		}
		geom.UpdateTriangles();
	}
	main.Draw();
};

SetHardnessTool.Undo = function(dest, how)
{
	this.dest = dest;
	this.how = how;
	undoRedo.AddUndo(this);
};

SetHardnessTool.Undo.prototype.Undo = function()
{
	setHardnessTool.Apply(this.dest, !this.how)
};

SetHardnessTool.Undo.prototype.Redo = function()
{
	setHardnessTool.Apply(this.dest, this.how)
};

setHardnessTool = new SetHardnessTool();
