function FileSupport()
{
	var tc = this;
	main.openMenuItem.onClick = function() { tc.OpenFile(); };
	main.saveMenuItem.onClick = function() { tc.SaveFile(); };
}

FileSupport.prototype.OpenFile = function()
{
	if(undoRedo.nModifications!=0)
	{
		if(!confirm("The current file is modified, are you sure do you want open another file?"))
			return;
	}
	openFile.title = "Open Little3DEditor's js file";
	openFile.Show();
	var tc = this;
	openFile.onOK = function() { tc.Open(openFile.selectedFile); }
};

FileSupport.prototype.Open = function(file)
{
	main.Reset();
	var currGeom = undefined;
	//noinspection JSUnusedLocalSymbols
	function G(name, lock, visible, wire)
	{
		currGeom = new Geometry(main.renderer);
		currGeom.name = name;
		currGeom.lock = lock;
		currGeom.visible = visible;
		currGeom.wire = wire;
	}

	//noinspection JSUnusedLocalSymbols
	function V(v) { currGeom.AddVertex(v); }

	//noinspection JSUnusedLocalSymbols
	function F(v) { currGeom.AddFace(v); }

	//noinspection JSUnusedLocalSymbols
	function HE(a, b) { currGeom.edges[currGeom.AddEdge(a, b)].hardness = true; }

	//noinspection JSUnusedLocalSymbols
	function END()
	{
		currGeom.FillCrossReferences();
		currGeom.UpdateTriangles();
		main.AddGeometry(currGeom);
		currGeom = undefined;
	}

	var reader = new FileReader();
	reader.readAsText(file);
	reader.onload = function(evt)
	{
		// removes all \r.. in case someone write the file by hand and separates lines
		var txt = evt.target.result.replace(/\r/g, '').split('\n');
		var error = '';
		for(var i = 0; i<txt.length; i++)
		{
			var line = txt[i];
			try
			{
				eval(line);
			} catch(e)
			{
				error += 'line '+(i+1)+': '+e.message+'\n';
			}
		}
		if(error.length!=0)
		{
			error = "There are some errors:\n"+error;
			alert(error);
		}
	}
};

FileSupport.prototype.SaveFile = function()
{
	saveFile.title = "Save Little3DEditor's js file";
	saveFile.Show();
	var tc = this;
	saveFile.onOK = function() { tc.Save(saveFile.fileName); }
};

FileSupport.prototype.Save = function(fileName)
{
	if(fileName.slice(-3).toLowerCase()!=".js")
		fileName += ".js";
	var text = "";
	for(var i = 0; i<main.geometries.length; i++)
	{
		var geom = main.geometries[i];
		text += "G('"+geom.name+"',"+geom.lock+","+geom.visible+","+geom.wire+");\n";
		for(var j = 0; j<geom.vertices.length; j++)
		{
			text += "V("+geom.vertices[j].toFixed(2)+");\n";
		}
		for(var j = 0; j<geom.faces.length; j++)
		{
			text += "F(["+geom.faces[j].indices+"]);\n";
		}
		for(var j = 0; j<geom.edges.length; j++)
		{
			var edge = geom.edges[j];
			if(edge.hardness)
				text += "HE("+edge.indices[0]+","+edge.indices[1]+");\n";
		}
		text += "END();\n";
	}
	var a = document.createElement("a");
	var blob = new Blob([text], { type: 'text/plain'});
	//noinspection JSUnresolvedVariable
	var myURL = window.URL || window.webkitURL;
	//noinspection JSUnresolvedFunction
	a.href = myURL.createObjectURL(blob);
	a.download = fileName;
	a.click();
};

internalJS = new FileSupport();

