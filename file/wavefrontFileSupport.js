function WavefrontFileSupport()
{
	var tc = this;
	main.importItem.subMenu.AddItem("Wavefront obj..").enable = false;
	main.importItem.subMenu.AddItem("Wavefront mtl..").enable = false;
	main.exportItem.subMenu.AddItem("Wavefront obj..").onClick = function() {tc.ExportObjDlg();};
	main.exportItem.subMenu.AddItem("Wavefront mtl..").enable = false;
}

WavefrontFileSupport.prototype.ExportObjDlg = function()
{
	saveFile.title = "Save Wavefront obj file";
	saveFile.Show();
	var tc = this;
	saveFile.onOK = function() { tc.ExportObj(saveFile.fileName); }
};

WavefrontFileSupport.prototype.ExportObj = function(fileName)
{
	var text = "#Exported by Maryka 3D\n";
	var compareNormal = function(n1,n2)
	{
		if( 'idx' in n1 ) n1=n1.n;
		if( 'idx' in n2 ) n2=n2.n;
		var dx = (n1.x - n2.x);
		if( Math.abs(dx)>1e-3 ) return dx;
		var dy = (n1.y - n2.y);
		if( Math.abs(dy)>1e-3 ) return dy;
		return 0;
	}
	var normals = new sortedArray(compareNormal);
	normals.allowDuplicated = false;
	var startId = 1;
	var normalId = 1;
	for(var i = 0; i<main.geometries.length; i++)
	{
		var geom = main.geometries[i];
		text += "o "+geom.name+"\n";
		for(var j = 0; j<geom.vertices.length; j++)
		{
			var vtx = geom.vertices[j];
			text += "v"
			for(var k = 0; k<3; k++)
			{
				text += " " + vtx[k].toFixed(8);
			}
			text+="\n";
		}
		for(var j = 0; j<geom.faces.length; j++)
		{
			var face = geom.faces[j];
			for(var k = 0; k<face.normals.length; k++)
			{
				var nor = {n:face.normals[k],idx:normalId};
				if( normals.insert( nor ) )
				{
					normalId++;
					text += "vn"
					for(var k = 0; k<3; k++)
					{
						text += " " + nor.n[k].toFixed(8);
					}
					text+="\n";
				}
			}
		}

		var sm = new UnionFind(geom.faces.length);
		for(var j = 0; j<geom.edges.length; j++)
		{
			var edge = geom.edges[j];
			if( !edge.hardness )
			{
				sm.union(edge.faces[0],edge.faces[1])
			}
		}
		text += "g "+geom.name+"_default\n";
		text += "usemtl default\n"; //TODO: material support
		var groups = sm.groups();
		var smId = 1;
		for(var j in groups)
		{
			text += "s " + smId +"\n";
			smId++;
			var faces = groups[j];
			for(var k = 0; k<faces.length; k++)
			{
				var face = geom.faces[faces[k]];
				text += "f"
				for(var l = 0; l<face.indices.length; l++)
				{
					text += " " + (face.indices[l]+startId).toString();
					var nId = normals[normals.indexOf(face.normals[l])].idx;
					text += "//" + (nId).toString();
				}
				text+="\n";
			}
		}
		startId += geom.vertices.length;
	}

	if(fileName.slice(-4).toLowerCase()!=".obj")
		fileName += ".obj";

	var a = document.createElement("a");
	var blob = new Blob([text], { type: 'text/plain'});
	//noinspection JSUnresolvedVariable
	var myURL = window.URL || window.webkitURL;
	//noinspection JSUnresolvedFunction
	a.href = myURL.createObjectURL(blob);
	a.download = fileName;
	a.click();
};


obj = new WavefrontFileSupport();
