Menu = {};

//region MenuBar
/**
 * MenuBar is the object that manage a menu bar,
 * it places itself on top of parent.
 * @param {Node} parent The parent element
 * @constructor
 */
Menu.Bar = function(parent)
{
	this.divContext = document.createElement('div');
	this.divContext.className = 'menubar';
	this.divContext.object = this;
	//this.divContext.id = name
	parent.insertBefore(this.divContext, parent.firstChild);
};

/**
 * this method add an Item to a menuBar,
 * CreateMenu.Context is called after this method usually.
 * @param {String} text
 * @return {Menu.Item}
 */
Menu.Bar.prototype.AddItem = function(text)
{
	var r = new Menu.Item(this, text);
	r.isOnMenuBar = true;
	return r;
};

//endregion

//region Context menu
/**
 * Menu.Context is the object that manage a context aka right-click menu
 * @constructor
 */
Menu.Context = function()
{
	this.divContext = document.createElement('div');
	//noinspection SpellCheckingInspection
	this.divContext.className = 'cmenu';
	//noinspection SpellCheckingInspection
	this.divContext.oncontextmenu = function() { return false;};
	this.divContext.object = this;
	//this.divContext.id = name
	document.body.appendChild(this.divContext);
	this.parent = undefined;
	this.item = undefined;
};

//noinspection JSUnusedGlobalSymbols
/**
 * Adds this Menu.Context to an HTML element, so when the user right click on this,
 * This Menu.Context appears instead of the system one.
 * @param {HTMLElement} where
 */
Menu.Context.prototype.AddToElement = function(where)
{
	//noinspection JSUndefinedPropertyAssignment
	where.oncontextmenu = this.GetContextShow();
	this.HideOnClick();

};

/**
 *  Show the context menu at position
 * @param {Number} [x] x-coordinate
 * @param {Number} [y] y-coordinate
 */
Menu.Context.prototype.Show = function(x, y)
{
	//this.divContext.style.display = 'none';
	if(x) this.divContext.style.left = x+'px';
	if(y) this.divContext.style.top = y+'px';
	this.divContext.style.display = 'block';
	if(this.item)
	{
		if(this.parent.constructor==Menu.Context)
			this.item.element.setAttribute('state', 'openContext'); else
			this.item.element.setAttribute('state', 'openBar');
	}
};

/**
 * Hides the Menu.Context
 */
Menu.Context.prototype.Hide = function()
{
	this.divContext.style.display = 'none';
	if(this.item)
	{
		this.item.element.setAttribute('state', 'enable');
	}
};

/**
 * Change the Menu.Context's behaviour, any click outside the menu hide it.
 */
Menu.Context.prototype.HideOnClick = function()
{
	document.addEventListener("mousedown", this.GetContextHide());
};

/**
 * Change the Menu.Context's behaviour, when the mouse goes out it disappears
 */
Menu.Context.prototype.HideOnOut = function()
{
	var cmenu = this;
	this.divContext.onmouseover = function()
	{
		cmenu.Show();
	};
	this.divContext.onmouseout = function(event)
	{
		var x = event.clientX, y = event.clientY;
		var newElement = document.elementFromPoint(x, y);
		var rect = cmenu.divContext.getBoundingClientRect();
		if(cmenu.parent!=newElement && ( rect.left>x || rect.right<x || rect.top>y || rect.bottom<y ))
			cmenu.Hide();
	}
};

/**
 * Creates a function that shows the Menu.Context at mouse position.
 * It can be associate with onMouse* events.
 * @returns {Function}
 */
Menu.Context.prototype.GetContextShow = function()
{
	var cm = this;
	return function(event)
	{
		cm.Show(event.clientX, event.clientY);

		return false;
	}
};

/**
 * Creates a function that hides the Menu.Context
 * @returns {Function}
 */
Menu.Context.prototype.GetContextHide = function()
{
	var cm = this;
	return function()
	{
		cm.Hide()
	}
};

/**
 * This method add an Item to a menuBar,
 * @param {String}text Item's text
 * @returns {Menu.Item}
 */
Menu.Context.prototype.AddItem = function(text)
{
	return new Menu.Item(this, text);
};

/**
 * This method add a separator to a menuBar,
 */
Menu.Context.prototype.AddSeparator = function()
{
	return new Menu.Separator(this);
};

//endregion

//region Separator

Menu.Separator = function(parent)
{
	this.element = document.createElement('hr');
	this.element.object = this;
	parent.divContext.appendChild(this.element)
};

Menu.Separator.prototype.show = function()
{
	this.element.style.display = 'block'
};

Menu.Separator.prototype.hide = function()
{
	this.element.style.display = 'none'
};

//endregion

//region Item

/**
 * Menu.Items are the element of menus
 * @param {Menu.Context|Menu.Bar}cmenu parent
 * @param {String}text The text inside the Menu item
 * @constructor
 */
Menu.Item = function(cmenu, text)
{
	this.element = document.createElement('div');
	this.textNode = document.createTextNode(text);
	this.element.appendChild(this.textNode);
	this.element.className = 'menuitem';
    this.element.style.display = 'block';
	this.element.setAttribute('state', 'enable');
	this.element.object = this;
	cmenu.divContext.appendChild(this.element);
	this.subMenu = undefined;
	this.isOnMenuBar = false;
	this.parent = cmenu;
	var mitem = this;
	this.onClick = undefined;
	this.element.onmousedown = function(evt)
	{
		var p = mitem.parent;
		while(p)
		{
			if("Hide" in p)
				p.Hide();
			p = p.parent
		}
		if(mitem.onClick && mitem.enable)
			mitem.onClick(evt)
	}
	this.shortcut = undefined;
};

Menu.Item.prototype.__defineGetter__('text', function()
{
	return this.textNode.nodeValue;
});

Menu.Item.prototype.__defineSetter__('text', function(v)
{
	this.textNode.nodeValue = v;
});

Menu.Item.prototype.__defineGetter__('completeName', function()
{
	var txt = this.text;
	var ele = this;
	while(ele.parent.item)
	{
		txt = ele.parent.item.text + "|" + txt;
		ele = ele.parent;
	}
	return txt;
});

/**
 * Adds a subMenu to Menu.Item
 * @returns {Menu.Context}
 */
Menu.Item.prototype.AddSubMenu = function()
{
	if(this.subMenu!=undefined)
		return this.subMenu;

	this.subMenu = new Menu.Context();
	if(!this.isOnMenuBar)
	{
		var img = document.createElement('img');
		img.src = "ui/menu_arrow.png";
		this.element.appendChild(img)
	}
	this.subMenu.divContext.setAttribute('parent', 'item');
	var cm = this.subMenu;
	var mItem = this.element;
	var bottom = this.isOnMenuBar;

	this.element.onmouseover = function()
	{
		var rect = mItem.getBoundingClientRect();
		if(bottom)
		{
			cm.Show(rect.left, rect.bottom-1)
		} else
		{
			cm.Show(rect.right-1, rect.top)
		}
	};

	this.element.onmouseout = function(event)
	{
		event = event || window.event;
		var outRightDir = false;
		if(bottom)
		{
			// check for output bottom
			outRightDir = event.offsetX>1;
			if(outRightDir) outRightDir = event.offsetY>1;
			if(outRightDir) outRightDir = event.offsetX<event.target.offsetWidth-1

		} else
		{
			// check for output right
			outRightDir = event.offsetX>1;
			if(outRightDir) outRightDir = event.offsetY>1;
			if(outRightDir) outRightDir = event.offsetY<event.target.offsetHeight-1
		}
		if(!outRightDir)
			cm.Hide()
	};

	this.subMenu.HideOnOut();
	this.subMenu.parent = this.parent;
	this.subMenu.item = this;
	//this.subMenu.divContext.onmouseout = this.subMenu.GetContextHide();

	return this.subMenu;
};

Menu.Item.prototype.__defineGetter__("enable", function()
{
	var value = this.element.getAttribute('state');
	return (value=='enable');

});

Menu.Item.prototype.__defineSetter__("enable", function(v)
{
	if( v )
	{
		this.element.setAttribute('state', 'enable')
	} else
	{
		this.element.setAttribute('state', 'disable')
	}
});

Menu.Item.prototype.__defineGetter__("visible", function()
{
	return this.element.style.display == 'block';
});

Menu.Item.prototype.__defineSetter__("visible", function(v)
{
	if( v )
		this.Show();
	else
		this.Hide();
});

/**
 * Hides the Menu.Item
 */
Menu.Item.prototype.hide = function()
{
	this.element.style.display = 'none'
};

/**
 * Shows the Menu.Item
 */
Menu.Item.prototype.show = function()
{
	this.element.style.display = 'block'
};

/**
 * @note This method is purely visual, it does not add the functionality.
 */
Menu.Item.prototype.AddShortcut = function(text)
{
	if(text && text.length!=0)
	{
		if(!this.shortcut)
		{
			this.shortcut = document.createElement('div');
			this.shortcut.className = "shortcut";
			this.element.appendChild(this.shortcut)
		}
		this.shortcut.innerText = text;
	} else
	{
		if(this.shortcut)
		{
			this.element.removeChild(this.shortcut);
			delete this.shortcut;
		}
	}
}

//endregion
