function SideBar(parent, right)
{
	this.divContext = document.createElement('div');
	if(right)
	{
		this.divContext.className = 'sideBarRight';
	} else
	{
		this.divContext.className = 'sideBarLeft';
	}
	parent.appendChild(this.divContext);
}

/**
 * @param {String} text
 */
SideBar.prototype.AddText = function(text)
{
	var p = document.createElement('div');
	p.className = "text";
	/*
	 for(var i=text.length-1;i>0;i--)
	 {
	 text=text.slice(0, i)+'<br/>'+text.slice(i);
	 }*/
	p.innerHTML = text;
	this.divContext.appendChild(p);
	return p;
};

