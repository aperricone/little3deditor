function MessageBox()
{
	this.InitCommon(false);
	this.container.style.zIndex = 32;
	this.title = "message";
	this.messageCnt = document.createElement("div");
	this.messageCnt.style.textAlign = "center";
	this.container.appendChild(this.messageCnt);
	this.buttons = document.createElement("div");
	this.container.appendChild(this.buttons);
	document.body.removeChild(this.container);
	CommonForm.prototype.Show.call(this);

	this.occluder = document.createElement("div");
	this.occluder.className = "occluder";
}

MessageBox.prototype = new CommonForm();
MessageBox.prototype.constructor = MessageBox;

MessageBox.prototype.__defineSetter__('message', function(val)
{
	this.messageCnt.innerHTML = val;
});

MessageBox.prototype.AddButton = function(text)
{
	var ret = document.createElement("input");
	ret.type = "button";
	ret.value = text;
	this.buttons.appendChild(ret);
	return ret;
};

MessageBox.prototype.Show = function()
{
	document.body.appendChild(this.occluder);
	document.body.appendChild(this.container);
	mouse.LockMouse(this);
};

MessageBox.prototype.OnMouseUp = function() {};

MessageBox.prototype.Hide = function()
{
	document.body.removeChild(this.occluder);
	document.body.removeChild(this.container);
	mouse.Unlock();
};
