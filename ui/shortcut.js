//region ShortcutsDlg
function ShortcutsDlg()
{
	this.InitCommon();
	//TODO: Add export-import buttons
	this.title = "Key mapping";
	/** @type {HTMLElement} */
	var shortcutsContainer = document.createElement('div');
	shortcutsContainer.className = "shortcutsLine";
	this.container.appendChild(shortcutsContainer);
	shortcutsContainer.innerHTML =
		"<div class=\"message\" >\n\tYou can set the shortcut of selected menu item pressing CTRL+INS\n</div>\n<div class=\"list\">\n\t<div class=\"head\">\n\t\t<div class=\"head1\">Command</div>\n\t\t<div class=\"head2\">Shortcut</div>\n\t</div>\n\t<div class=\"elements\">\n\t</div>\n</div>\n";
	//noinspection JSUnresolvedFunction
	this.list = shortcutsContainer.getElementsByClassName("elements")[0];
}

ShortcutsDlg.prototype = new CommonForm();
ShortcutsDlg.prototype.constructor = ShortcutsDlg;

ShortcutsDlg.prototype.Show = function()
{
	CommonForm.prototype.Show.call(this);
	this.Populate();
};

/**
 *
 * @param {Menu.Item} item
 * @param {HTMLInputElement} [btn]
 * @constructor
 */
ShortcutsDlg.prototype.SetShortcut = function(item,btn)
{
	this.msg = new MessageBox();
	this.msg.title = item.completeName;
	this.msg.message = "Press a key combination to set it as shortcut<br>or ESC to unset it.";
	this.currentBtn = btn;
	this.currentItem = item;
	keyMan.locker=this;
    this.msg.Show();
};

//noinspection JSUnusedGlobalSymbols
ShortcutsDlg.prototype.OnKeyUp = function(ev)
{
	// skip CTRL, ALT and SHIFT
	if(ev.keyCode>=16 && ev.keyCode<=18) return;
	if(ev.ctrlKey && ev.keyCode==45) return;
	var sht = undefined;
	if(ev.keyCode!=27)
	{
		sht = new Shortcut(ev);
		shortcuts.SetShortcut(this.currentItem, sht);
		if(this.currentBtn) this.currentBtn.value = sht.name;
	} else
	{
		shortcuts.RemoveShortcut(this.currentItem);
		this.currentItem.AddShortcut();
		if(this.currentBtn) this.currentBtn.value = 'none';
	}
	this.msg.Hide();
	delete this.msg;
    keyMan.locker = undefined;
};

ShortcutsDlg.GetAllItems = function(where)
{
	var r = [];
	for(var i = 0; i<where.divContext.childNodes.length; i++)
	{
		var obj = where.divContext.childNodes[i];
		if(obj.className=='menuitem')
		{
			/** @type {Menu.Item} */
			var item = obj.object;
			if(item.onClick)
			{
				r.push(item);
			}
			if(item.subMenu)
			{
				r = r.concat(ShortcutsDlg.GetAllItems(item.subMenu));
			}
		}
	}
	return r;
};

/**
 * @param {String} name
 * @param {Menu.Bar|Menu.Context} [where]
 * @returns {Menu.Item|undefined}
 */
ShortcutsDlg.GetItemByName = function(name,where)
{
	if(where == undefined)
	{
		var r = ShortcutsDlg.GetItemByName(name,main.menuBar);
		if( r ) return r;
		r = ShortcutsDlg.GetItemByName(name,main.contextMenu);
		return r;
	}
	for(var i = 0; i<where.divContext.childNodes.length; i++)
	{
		var obj = where.divContext.childNodes[i];
		if(obj.className=='menuitem')
		{
			/** @type {Menu.Item} */
			var item = obj.object;
			if(item.onClick)
			{
				if( item.completeName == name ) return item;
			}
			if(item.subMenu)
			{
				var r = ShortcutsDlg.GetItemByName(name,item.subMenu);
				if( r ) return r;
			}
		}
	}
	return undefined;
};

ShortcutsDlg.prototype.Populate = function()
{
	var tc=this;
	var myList = ShortcutsDlg.GetAllItems(main.menuBar);
	myList = myList.concat( ShortcutsDlg.GetAllItems(main.contextMenu) );
	for(var i = 0; i<myList.length; i++)
	{
		var item = myList[i];
		var name = document.createElement('div');
		name.innerText = item.completeName;
		this.list.appendChild(name);
		var button = document.createElement('input');
		button.type = "button";
		var sht = shortcuts.GetShortcut(item);
		if( sht )
			button.value = sht.name;
		else
			button.value = 'none';
		button.target = item;
		button.onclick = function(e) { tc.SetShortcut(e.target.target, e.target); };
		this.list.appendChild(button);
	}
};

//endregion

//region Shortcut
/**
 *
 * @param {Boolean|Event} alt
 * @param {Boolean} [ctrl]
 * @param {Boolean} [shift]
 * @param {Number} [keyCode]
 * @constructor
 */
function Shortcut(alt,ctrl,shift,keyCode)
{
	if( typeof(alt)!= typeof(true) )
	{
		var ev = alt;
		keyCode = ev.keyCode;
		alt     = ev.altKey;
		ctrl    = ev.ctrlKey;
		shift   = ev.shiftKey;
	}
	this.alt     =alt;
	this.ctrl    =ctrl;
	this.shift   =shift;
	this.keyCode =keyCode ;
}

// http://www.selfcontained.us/2009/09/16/getting-keycode-values-in-javascript/
Shortcut.keyCodeMap = {
	8:   "BACKSPACE", 9: "TAB", 13: "RETURN", 16: "SHIFT", 17: "CTRL", 18: "ALT", 19: "PAUSE BREAK", 20: "CAPS LOCK",
	27: "ESCAPE", 32: " ", 33: "PAGE UP",34:  "PAGE DOWN", 35: "END", 36: "HOME", 37: "LEFT", 38: "UP", 39: "RIGHT",
	40: "DOWN", 43: "+", 44: "PRINTSCREEN", 45: "INSERT", 46: "DELETE",
	48:  "0", 49: "1", 50: "2", 51: "3", 52: "4", 53: "5", 54: "6", 55: "7", 56: "8", 57: "9", 59: ";",
	61:  "=", 65: "A", 66: "B", 67: "C", 68: "D", 69: "E", 70: "F", 71: "G", 72: "H", 73: "I", 74: "J", 75: "K", 76: "L",
	77:  "M", 78: "N", 79: "O", 80: "P", 81: "Q", 82: "R", 83: "S", 84: "T", 85: "U", 86: "V", 87: "W", 88: "X", 89: "Y", 90: "Z",
	96:  "0", 97: "1", 98: "2", 99: "3", 100: "4", 101: "5", 102: "6", 103: "7", 104: "8", 105: "9",
	106: "*", 107: "+", 109: "-", 110: ".", 111: "/",
	112: "F1", 113: "F2", 114: "F3", 115: "F4", 116: "F5", 117: "F6", 118: "F7", 119: "F8", 120: "F9", 121: "F10", 122: "F11", 123: "F12",
	144: "NUMLOCK", 145: "SCROLLLOCK", 186: ";", 187: "=", 188: ",", 189: "-", 190: ".", 191: "/", 192: "`", 219: "[", 220: "\\", 221: "]", 222: "'"
};
Shortcut.prototype.__defineGetter__('name',function()
{
	var txt = '';
	if( this.alt ) txt += "ALT+";
	if( this.ctrl ) txt += "CTRL+";
	if( this.shift ) txt += "SHIFT+";
	txt+= Shortcut.keyCodeMap[this.keyCode];
	return txt;
});

/**
 * @param {Event} ev
 * @returns {boolean}
 */
Shortcut.prototype.Check = function(ev)
{
	if(ev.keyCode!=this.keyCode) return false;
	if(ev.altKey!=this.alt) return false;
	if(ev.ctrlKey!=this.ctrl) return false;
	//noinspection RedundantIfStatementJS
	if(ev.shiftKey!=this.shift) return false;
	return true;
};

Shortcut.prototype.equal = function(shortcut)
{
	if(shortcut.keyCode!=this.keyCode) return false;
	if(shortcut.alt!=this.alt) return false;
	if(shortcut.ctrl!=this.ctrl) return false;
	//noinspection RedundantIfStatementJS
	if(shortcut.shift!=this.shift) return false;
	return true;
};

//endregion

//region ShortcutsMan

function ShortcutsMan()
{
	var tc = this;
	this.dlg = new ShortcutsDlg();
	main.optionsItem.AddItem("Shortcuts").onClick = function() { tc.dlg.Show(); };
	keyMan.SetListener(this,45); //INS
	this.shortcuts = {};
	this.shortcuts[45] = [undefined]; // CTRL+INS
}

ShortcutsMan.prototype.GetItem = function(shortcut)
{
	for(var i in this.shortcuts)
	{
		var key = this.shortcuts[i];
		for(var j = 0; j<key.length; j++)
		{
			if( key[j] && key[j].equal(shortcut))
				return key[j].item;
		}
	}

};

ShortcutsMan.prototype.SetShortcut = function(item,shortcut)
{
	//remove old
	this.RemoveShortcut(item);
	var oldItem = this.GetItem(shortcut)
	if( oldItem ) this.RemoveShortcut(oldItem);

	item.AddShortcut(shortcut.name);
	if(!(shortcut.keyCode in this.shortcuts ))
	{
		this.shortcuts[shortcut.keyCode] = [];
		keyMan.SetListener(this,shortcut.keyCode);
	}
	shortcut.item = item;
	this.shortcuts[shortcut.keyCode].push(shortcut);
};

/**
 * @param {Menu.Item} item
 * @returns {Shortcut|undefined}
 */
ShortcutsMan.prototype.GetShortcut = function(item)
{
	for(var i in this.shortcuts)
	{
		var key = this.shortcuts[i];
		for(var j = 0; j<key.length; j++)
		{
			var shortcut = key[j];
			if( shortcut && shortcut.item == item)
				return shortcut;
		}
	}
	return undefined;
};

ShortcutsMan.prototype.RemoveShortcut = function(item)
{
	item.AddShortcut();
	var shortcut = this.GetShortcut(item);
	if( shortcut )
	{
		var key = this.shortcuts[shortcut.keyCode];
		var j =key.indexOf(shortcut);
		key.splice(j,1);
		if( key.length == 0  )
		{
			keyMan.RemoveListener(shortcut.keyCode);
			delete this.shortcuts[shortcut.keyCode];
		}
	}
};

ShortcutsMan.prototype.EventSetShortcut = function()
{
	var ele = document.elementFromPoint(mouse.x, mouse.y);
	if(!ele) return;
	if(ele.className=='menuitem')
	{
		var item = ele.object;
		this.dlg.SetShortcut(item);
	}
};

//noinspection JSUnusedGlobalSymbols
ShortcutsMan.prototype.OnKeyDown = function(ev)
{
	if(ev.ctrlKey && ev.keyCode == 45)
	{
		this.EventSetShortcut();
		return;
	}
	var keys = this.shortcuts[ev.keyCode];
	for(var i = 0; i<keys.length; i++)
	{
		var sht = keys[i];
		if( sht && sht.Check(ev) )
		{
			var item = sht.item;
			if( item.visible && item.enable )
			{
				item.onClick();
				return;
			}
		}
	}
};

ShortcutsMan.prototype.Import = function(txt)
{
	//noinspection JSUnusedLocalSymbols
	function S(alt, ctrl, shift, keyCode)
	{
		if(typeof(alt)!= typeof(true))  throw "invalid parameter";
		if(typeof(ctrl)!= typeof(true))  throw "invalid parameter";
		if(typeof(shift)!= typeof(true))  throw "invalid parameter";
		if(typeof(keyCode)!= typeof(1))  throw "invalid parameter";
		return new Shortcut(alt, ctrl, shift, keyCode)
	}

	var tc = this;
	//noinspection JSUnusedLocalSymbols
	function SHT(itemName, sht)
	{
		var item;
		item = ShortcutsDlg.GetItemByName(itemName);
		if(item==undefined) throw itemName+"not found";
		tc.SetShortcut(item, sht);
	}

	var lines = txt.replace(/\r/g, '').split('\n');
	var error = '';
	for(var i = 0; i<lines.length; i++)
	{
		var line = lines[i];
		try
		{
			eval(line);
		} catch(e)
		{
			error += 'line '+(i+1)+': '+e.message+'\n';
		}
	}
	if(error.length!=0)
	{
		error = "There are some errors:\n"+error;
		alert(error);
	}
};

/**
 * @return {string}
 */
ShortcutsMan.prototype.Export = function()
{
	var txt = '';
	for(var i in this.shortcuts)
	{
		var key = this.shortcuts[i];
		for(var j = 0; j<key.length; j++)
		{
			/** @type {Shortcut} */
			var shortcut = key[j];
            if( shortcut.item )
            {
                txt += "SHT('"+shortcut.item.completeName+"',S(";
                txt += shortcut.alt+","+shortcut.ctrl+","+shortcut.shift+","+shortcut.keyCode;
                txt += "));\n";
            }
		}
	}
	return txt;
};

ShortcutsMan.prototype.FirstInit = function()
{
	  this.Import("SHT(\'File|New\',S(false,false,true,78));\nSHT(\'File|Open...\',S(false,false,true,79));\nSHT(\'File|Save...\',S(false,false,true,83));\nSHT(\'View|Aligned X+\',S(false,false,false,88));\nSHT(\'View|Aligned X-\',S(false,false,true,88));\nSHT(\'View|Aligned Y+\',S(false,false,false,89));\nSHT(\'View|Aligned Y-\',S(false,false,true,89));\nSHT(\'View|Aligned Z+\',S(false,false,false,90));\nSHT(\'View|Aligned Z-\',S(false,false,true,90));\nSHT(\'Edit|Undo\',S(false,true,false,90));\nSHT(\'Edit|Redo\',S(false,true,true,90));\n" );
};

var shortcuts = new ShortcutsMan;

//endregion
