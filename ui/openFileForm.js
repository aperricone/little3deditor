function OpenFileForm(derived)
{
	if(derived) return;
	this.InitCommon();
	var fileContainerElement = document.createElement("div");
	fileContainerElement.className = "line";
	fileContainerElement.style.align = 'center';
	this.fileElement = document.createElement("input");
	this.fileElement.type = 'file';
	this.fileElement.name = 'files[]';
	fileContainerElement.appendChild(this.fileElement);

	fileContainerElement.appendChild(document.createElement("br"));
	fileContainerElement.appendChild(document.createTextNode("or type url here:"));
	this.nameElement = document.createElement("input");
	this.nameElement.type = 'url';
	this.nameElement.disabled = true;
	fileContainerElement.appendChild(this.nameElement);

	this.errorMessage = document.createElement("div");
	//this.errorMessage.style.display = 'none';
	this.errorMessage.className = 'error';
	//this.errorMessage.appendChild(document.createTextNode("This url is invalid!"));
	this.errorMessage.innerText = "TODO!";
	fileContainerElement.appendChild(this.errorMessage);

	this.container.appendChild(fileContainerElement);


	this.okBtn.disabled = true;
	var tc = this;
	this.fileElement.addEventListener('change', function() {tc.checkOK();}, false);
	this.nameElement.addEventListener('input', function() {tc.checkUrl();}, false);
	this.urlValid = false;
}

OpenFileForm.prototype = new CommonForm();
OpenFileForm.prototype.constructor = OpenFileForm;

//http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js
OpenFileForm.prototype.checkUrl = function()
{
	var regExp = /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/;
	this.urlValid = regExp.test(this.nameElement.value);
	this.errorMessage.style.display = this.urlValid ? 'none' : 'block';
	this.checkOK();
};

OpenFileForm.prototype.__defineGetter__('selectedFile', function() { return this.fileElement.files[0]; });

OpenFileForm.prototype.checkOK = function()
{
	this.okBtn.disabled = ( this.fileElement.files.length==0) && ( !this.urlValid);
};

openFile = new OpenFileForm();
