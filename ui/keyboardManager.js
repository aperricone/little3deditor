function KeyboardManager()
{
	this.keyCallbacks = {};
	this.locker = undefined;

	var km = this;
	//noinspection SpellCheckingInspection
	document.onkeydown = function(evt) { km.OnKey(evt, 'Down'); };
	//noinspection SpellCheckingInspection
	document.onkeyup = function(evt) { km.OnKey(evt, 'Up'); };
	//noinspection SpellCheckingInspection
	document.onkeypress = function(evt) { km.OnKey(evt, 'Press'); };
}

/**
 *
 * @param {KeyboardEvent}evt
 * @param {String}which
 * @constructor
 */
KeyboardManager.prototype.OnKey = function(evt, which)
{
	evt.stopPropagation();
	evt.preventDefault();
	if(!("keyCode" in evt))
	{
		//noinspection JSUndefinedPropertyAssignment
		evt.keyCode = evt.which;
	}
	var dest = undefined;
	var Fn = "OnKey"+which;
	if( this.locker )
	{
		dest = this.locker;
	} else
	{
		//noinspection JSUnresolvedVariable
		if(evt.keyCode in this.keyCallbacks)
		{
			//noinspection JSUnresolvedVariable
			dest = this.keyCallbacks[evt.keyCode];
		}
	}
	if( dest && Fn in dest)
	{
		dest[Fn](evt);
	}
	return false;
};

//noinspection JSUnusedGlobalSymbols
KeyboardManager.prototype.SetListener = function(obj, key)
{
	this.keyCallbacks[key] = obj;
};

//noinspection JSUnusedGlobalSymbols
KeyboardManager.prototype.RemoveListener = function(key)
{
	delete this.keyCallbacks[key];
};

keyMan = new KeyboardManager();
