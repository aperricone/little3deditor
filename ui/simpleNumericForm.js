function SimpleNumericForm()
{
	this.InitCommon();
	this.changeListener = undefined;
	this.inputs = [];
	this.nInput = 0;
}

SimpleNumericForm.prototype = new CommonForm();
SimpleNumericForm.prototype.constructor = SimpleNumericForm;

SimpleNumericForm.prototype.__defineSetter__('title', function(v)
{
	this.titleElement.nodeValue = v;
	for(var i = 0; i<this.inputs.length; i++)
		for(; i<this.inputs.length; i++)
		{
			this.inputs[i].row.style.display = 'none';
		}
	this.changeListener = undefined;
	this.nInput = 0;
});

/**
 * @return {Number|undefined}
 */
SimpleNumericForm.prototype.GetValue = function(which)
{
	if(which<0 || which>=this.nInput) return undefined;
	return parseFloat(this.inputs[which].input.value);
};


SimpleNumericForm.prototype.SetValue = function(which, value)
{
	if(which<0 || which>=this.nInput) return;
	this.inputs[which].input.value = value;
};

SimpleNumericForm.prototype.__defineSetter__('onchange', function(fn) { this.SetOnChangeListener(fn); });

SimpleNumericForm.prototype.SetOnChangeListener = function(fn)
{
	this.changeListener = fn;
};

SimpleNumericForm.prototype.onchangeImpl = function(which)
{
	if(this.changeListener)
		this.changeListener(which);
};

SimpleNumericForm.prototype.AddNumericInput = function(name, value, step)
{
	if(this.nInput==this.inputs.length)
	{
		var tc = this;
		var row = {};
		/** @type {HTMLDivElement} */
		row.row = document.createElement("div");
		row.row.className = "line";
		//noinspection JSValidateTypes
		/** @type {HTMLDivElement} */
		var inputName = document.createElement("div");
		row.name = document.createTextNode("");
		inputName.className = "label";
		//noinspection JSValidateTypes
		/** @type {HTMLInputElement} */
		row.input = document.createElement("input");
		row.input.type = "number";
		row.input.className = "number";
		var thisId = this.nInput;
		row.input.onchange = function()
		{
			tc.onchangeImpl(thisId);
		};

		inputName.appendChild(row.name);
		row.row.appendChild(inputName);
		row.row.appendChild(row.input);

		this.inputs.push(row);
		this.container.appendChild(row.row)
	}

	var row = this.inputs[this.nInput];
	row.name.nodeValue = name;
	if(step) row.input.step = step;
	if(value!=undefined) row.input.value = value;
	row.row.style.display = 'block';
	this.nInput++;
};

simpleNumericForm = new SimpleNumericForm();
