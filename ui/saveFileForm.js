function SaveFileForm()
{
	this.InitCommon();
	var fileContainerElement = document.createElement("div");
	fileContainerElement.className = "line";
	fileContainerElement.appendChild(document.createTextNode("Select file name:"));
	this.nameElement = document.createElement("input");
	this.nameElement.type = 'text';
	fileContainerElement.appendChild(this.nameElement);

	this.container.appendChild(fileContainerElement);

	this.okBtn.disabled = true;
	var tc = this;
	this.nameElement.addEventListener('input', function() {tc.checkOK();}, false);
}

SaveFileForm.prototype = new CommonForm();
SaveFileForm.prototype.constructor = SaveFileForm;

SaveFileForm.prototype.checkOK = function()
{
	this.okBtn.disabled = ( this.nameElement.value.length==0);
};

SaveFileForm.prototype.__defineGetter__("fileName", function() { return this.nameElement.value; });

saveFile = new SaveFileForm();
