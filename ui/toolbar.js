/**
 *
 * @param {Node} parent
 * @constructor
 */
function ToolBar(parent)
{
	this.divContext = document.createElement('div');
	this.divContext.className = 'toolbar';
	parent.appendChild(this.divContext);

	this.leftContext = document.createElement('div');
	this.leftContext.id = 'left';
	this.divContext.appendChild(this.leftContext);

	this.rightContext = document.createElement('div');
	this.rightContext.id = 'right';
	this.divContext.appendChild(this.rightContext);

	this.centerContext = document.createElement('div');
	this.centerContext.id = 'center';
	this.divContext.appendChild(this.centerContext)
}

ToolBar.Item = function(img)
{
	this.img = img;
};

ToolBar.Item.prototype.SetImage = function(imgUrl)
{
	this.img.src = imgUrl;
};

ToolBar.Item.prototype.__defineGetter__('checked', function()
{
	return this.img.hasAttribute('check');
});

ToolBar.Item.prototype.__defineSetter__('checked', function(ck)
{
	if(!this.enable) return;
	if(ck)
		this.img.setAttribute('check', 'true'); else
		this.img.removeAttribute('check')
});

ToolBar.Item.prototype.__defineGetter__('enable', function()
{
	return !this.img.hasAttribute('enable');
});

ToolBar.Item.prototype.__defineSetter__('enable', function(ck)
{

	if(ck)
	{
		this.img.removeAttribute('enable');
		this.img.onclick = this.onclick;
	} else
	{
		this.img.setAttribute('enable', 'false');
		this.img.removeAttribute('check');
		this.img.onclick = undefined;
	}
});


ToolBar.Item.prototype.SetCallback = function(fn)
{
	this.onclick = fn;
	if(this.enable)
		this.img.onclick = fn;
};
/**
 *
 * @param {String}imgUrl
 * @param {Number} [position=0] 0 left, 1 center, 2 right
 * @returns {ToolBar.Item}
 */
ToolBar.prototype.AddIcon = function(imgUrl, position)
{
	var img = document.createElement('img');
	img.src = imgUrl;
	img.height = '32';
	switch(position)
	{
	case 1:
		this.centerContext.appendChild(img);
		break;
	case 2:
		this.rightContext.appendChild(img);
		break;
	default:
		this.leftContext.appendChild(img);
		break;
	}
	return new ToolBar.Item(img);
};
