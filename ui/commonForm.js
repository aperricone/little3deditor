function CommonForm()
{

}

// from: http://robertnyman.com/2006/04/24/get-the-rendered-style-of-an-element/
function getStyle(oElm, strCssRule)
{
	var strValue = "";
	if(document.defaultView && document.defaultView.getComputedStyle)
	{
		strValue = document.defaultView.getComputedStyle(oElm, "").getPropertyValue(strCssRule);
	} else if(oElm.currentStyle)
	{
		strCssRule = strCssRule.replace(/\-(\w)/g, function(strMatch, p1)
		{
			return p1.toUpperCase();
		});
		strValue = oElm.currentStyle[strCssRule];
	}
	return strValue;
}

/**
 * @param {Boolean} [btns] if set to false the buttons will NOT be added.
 */
CommonForm.prototype.InitCommon = function(btns)
{
	var tc = this;
	this.container = document.createElement("div");
	this.container.className = "simpleForm";

	var titleElement = document.createElement("div");
	titleElement.className = "title";
	this.titleElement = document.createTextNode("");
	titleElement.appendChild(this.titleElement);
	this.container.appendChild(titleElement);

	titleElement.onmousedown = function(e)
	{
		tc.Move(e);
	};

	if(btns==undefined || btns==true)
	{
		var buttonsElement = document.createElement("div");
		buttonsElement.className = "buttons";
		this.okBtn = document.createElement("input");
		this.okBtn.type = "button";
		this.okBtn.value = "OK";
		this.okBtn.onclick = function()
		{
			if(tc.onOK) tc.onOK();
			tc.Hide();
		};
		this.cancelBtn = document.createElement("input");
		this.cancelBtn.type = "button";
		this.cancelBtn.value = "Cancel";
		this.cancelBtn.onclick = function()
		{
			if(tc.onCancel) tc.onCancel();
			tc.Hide();
		};
		buttonsElement.appendChild(this.okBtn);
		buttonsElement.appendChild(this.cancelBtn);
		this.container.appendChild(buttonsElement);
	}
	document.body.appendChild(this.container);
	this.container.style.marginLeft = getStyle(this.container, "margin-left");
	this.container.style.marginTop = getStyle(this.container, "margin-top");

	this.onOK = undefined;
	this.onCancel = undefined;
};

CommonForm.prototype.__defineSetter__('title', function(v)
{
	this.titleElement.nodeValue = v;
});

CommonForm.prototype.__defineGetter__('title', function()
{
	return this.titleElement.nodeValue;
});

CommonForm.prototype.Show = function()
{
	this.container.style.display = 'block';
};

CommonForm.prototype.Hide = function()
{
	this.container.style.display = 'none';
};

CommonForm.prototype.Move = function(e)
{
	mouse.LockMouse(this);
	this.lastMouse = V2(e.clientX, e.clientY);
	this.moving = true;
};

CommonForm.prototype.OnMouseMove = function(e)
{
	if( this.moving )
	{
		var newPos = V2(e.clientX, e.clientY);
		var delta = V2(newPos).sub(this.lastMouse);
		this.lastMouse = newPos;
		this.container.style.marginLeft = parseInt(this.container.style.marginLeft)+delta.x+'px';
		this.container.style.marginTop = parseInt(this.container.style.marginTop)+delta.y+'px';
	}
};

CommonForm.prototype.OnMouseUp = function()
{
	if( this.moving )
	{
		mouse.Unlock();
	}
};
