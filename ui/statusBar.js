function StatusBar(parent)
{
	this.divContext = document.createElement('div');
	this.divContext.className = 'statusBar';
	parent.appendChild(this.divContext);

	this.text = document.createTextNode('StatusBar');
	this.divContext.appendChild(this.text);
}

/**
 *
 * @param {String} text
 */
StatusBar.prototype.SetText = function(text)
{
	this.text.replaceWholeText(text)
};
