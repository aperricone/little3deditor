function __MouseManager()
{
	this.leftObj = undefined;
	this.rightObj = undefined;
	this.centerObj = undefined;
	this.wheelObj = undefined;
	this.moveObj = undefined;
	this.locker = undefined;

	this.buttonsDown = [false, false, false];

	this.infoText = document.createElement('div');
	this.infoText.style.position = 'absolute';
	this.lines = [];
	this.lines.push(document.createTextNode(""));
	this.lines.push(document.createTextNode(""));
	this.infoText.appendChild(this.lines[0]);
	this.infoText.appendChild(document.createElement('br'));
	this.infoText.appendChild(this.lines[1]);
	document.body.appendChild(this.infoText);
	this.attach(this.infoText,false);
	var tc = this;
	document.onmousemove= function(ev) { tc.x = ev.clientX; tc.y = ev.clientY; }
}

__MouseManager.prototype.SetInfo = function(line, text, caller)
{
	if(this.locker)
	{
		if(caller!=this.locker) return;
	} else
	{
		if(caller!=this.leftObj) return;
	}
	this.lines[line].nodeValue = text;
};

/**
 * @param {HTMLElement} where
 * @param {Boolean} [info]
 */
__MouseManager.prototype.attach = function(where, info)
{
	var rect = where.getBoundingClientRect();
	if(info || info==undefined)
	{
		this.infoText.style.top = rect.top+10+'px';
		this.infoText.style.left = rect.left+10+'px';
		this.infoText.style.zIndex = 10;
	}

	var mm = this;
	//noinspection SpellCheckingInspection
	where.onmousedown = function(evt) { mm.OnMouse(evt, 'Down'); };
	//noinspection SpellCheckingInspection
	where.onmouseup = function(evt) { mm.OnMouse(evt, 'Up'); };
	//noinspection SpellCheckingInspection
	where.onmousemove = function(evt) { mm.OnMouseMove(evt); };
	//noinspection SpellCheckingInspection,JSUndefinedPropertyAssignment
	where.oncontextmenu = function() { return false; };
	//noinspection SpellCheckingInspection
	if('onmousewheel' in where)
	{
		//noinspection SpellCheckingInspection,JSUndefinedPropertyAssignment
		where.onmousewheel = function(evt) { mm.OnMouseWheel(evt) }
	} else
	{
		where.addEventListener('DOMMouseScroll', function(evt) { mm.OnMouseWheel(evt) }, false);
	}
};

__MouseManager.prototype.LockMouse = function(who)
{
	var mm = this;
	this.locker = who;
	for(var i = 0; i<this.lines.length; i++)
	{
		var line = this.lines[i];
		line.exValue = line.nodeValue;
	}
	this.lockMouseMove = function(evt)
	{
		if(!evt)
		{
			evt = window.event;  // Get event details for IE
			evt.which = evt.keyCode; // assign which property (so rest of the code works using e.which)
		}
		if(mm.locker.OnMouseMove)
		{
			mm.locker.OnMouseMove(evt);
		}
	};
	this.lockMouseDown = function(evt)
	{
		if(!evt)
		{
			evt = window.event;  // Get event details for IE
			evt.which = evt.keyCode; // assign which property (so rest of the code works using e.which)
		}
		mm.buttonsDown[evt.which-1] = true;
		if(mm.locker.OnMouseDown)
		{
			mm.locker.OnMouseDown(evt);
		}
	};
	this.lockMouseUp = function(evt)
	{
		if(!evt)
		{
			evt = window.event;  // Get event details for IE
			evt.which = evt.keyCode; // assign which property (so rest of the code works using e.which)
		}
		mm.buttonsDown[evt.which-1] = false;
		if(mm.locker.OnMouseUp)
		{
			mm.locker.OnMouseUp(evt);
		}
	};
	document.documentElement.addEventListener('mousemove', this.lockMouseMove, false);
	document.documentElement.addEventListener('mousedown', this.lockMouseDown, false);
	document.documentElement.addEventListener('mouseup', this.lockMouseUp, false);
};

__MouseManager.prototype.Unlock = function()
{
	if(this.locker!=undefined)
	{
		this.locker = undefined;
		document.documentElement.removeEventListener('mousemove', this.lockMouseMove, false);
		document.documentElement.removeEventListener('mousedown', this.lockMouseDown, false);
		document.documentElement.removeEventListener('mouseup', this.lockMouseUp, false);
		this.lockMouseMove = undefined;
		this.lockMouseDown = undefined;
		this.lockMouseUp = undefined;

		for(var i = 0; i<this.lines.length; i++)
		{
			var line = this.lines[i];
			line.nodeValue = line.exValue;
		}
	}
};

/**
 * @return {boolean}
 */
__MouseManager.prototype.OnMouse = function(evt, whichEvent)
{
	if(this.locker!=undefined) return false;
	if(!evt)
	{
		evt = window.event;  // Get event details for IE
		evt.which = evt.keyCode; // assign which property (so rest of the code works using e.which)
	}
	this.buttonsDown[evt.which-1] = whichEvent=='Down';
	var dest;
	switch(evt.which)
	{
	case 1:
		dest = this.leftObj;
		break;
	case 2:
		dest = this.centerObj;
		break;
	case 3:
		dest = this.rightObj;
		break;
	}
	var Fn = 'OnMouse'+whichEvent;
	if(dest && Fn in dest)
	{
		return dest[Fn](evt, this);
	}
	return false;
};

/**
 * @return {boolean}
 */
__MouseManager.prototype.OnMouseMove = function(evt)
{
	if(this.locker!=undefined) return false;
	if(!evt)
	{
		evt = window.event;  // Get event details for IE
		evt.which = evt.keyCode; // assign which property (so rest of the code works using e.which)
	}
	if(this.moveObj && this.moveObj.OnMouseMove)
	{
		return this.moveObj.OnMouseMove(evt, this);
	}
	return false;
};


/**
 * @return {boolean}
 */
__MouseManager.prototype.OnMouseWheel = function(evt)
{
	//if(this.locker!=undefined) return false;
	if(!evt)
	{
		evt = window.event;  // Get event details for IE
		evt.which = evt.keyCode; // assign which property (so rest of the code works using e.which)
	}
	evt.wheelDelta = evt.detail ? evt.detail*(-120) : evt.wheelDelta;
	if(this.wheelObj && this.wheelObj.OnMouseWheel)
	{
		return this.wheelObj.OnMouseWheel(evt, this);

	}
	return false;
};

var mouse = new __MouseManager();
