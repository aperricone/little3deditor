/**
 *
 * @constructor
 */
function Main()
{
	this.createMenus();
	this.createCenterDiv();

	this.DrawList = [];
	this.geometries = [];
	this.onGeometriesChanged = [];
	this.onReset = [];

	this.createCanvas();
	this.createContextMenu();
	// Debug test - DE-COMMENT TO TEST
	//this.addTestCode();

	this.Draw();

}

//region initialization

Main.prototype.createMenus = function()
{
	var tc = this;
	this.menuBar = new Menu.Bar(document.body);
	this.fileMenu = this.menuBar.AddItem("File").AddSubMenu();
	this.fileMenu.AddItem("New").onClick = function() { tc.Reset(); };
	this.openMenuItem = this.fileMenu.AddItem("Open...");
	this.saveMenuItem = this.fileMenu.AddItem("Save...");
	this.fileMenu.AddSeparator();
	this.optionsItem = this.fileMenu.AddItem("Options").AddSubMenu();
	this.fileMenu.AddSeparator();
	this.importItem = this.fileMenu.AddItem("Import");
	this.exportItem = this.fileMenu.AddItem("Export");
	this.importItem.AddSubMenu();
	this.exportItem.AddSubMenu();


	var e = this.menuBar.AddItem("Edit");
	this.editMenu = e.AddSubMenu();

	var t = this.menuBar.AddItem("Tools");
	this.toolMenu = t.AddSubMenu();

	this.toolBar = new ToolBar(document.body);
	this.toolBar.AddIcon("img/fileOpen.svg", 0);

	this.statusBar = new StatusBar(document.body);

	this.rightBar = new SideBar(document.body, true);
	//this.rightBar.AddText('Geometries');

	//	this.geometriesList = new GeometriesList();
};

Main.prototype.createCenterDiv = function()
{
	this.centerDiv = document.createElement('div');
	this.centerDiv.className = 'mainDiv';
	document.body.appendChild(this.centerDiv);

	// for AddBorder
	this.centerDiv.style.right = getStyle(this.centerDiv, 'right');

	/*this.geometriesList.container.style.top=mbs+tbs+'px';
	 this.geometriesList.container.style.bottom=sbs+'px';
	 this.geometriesList.container.style.right=rbs+'px';*/
};

Main.prototype.ResizeCheck = function()
{
	var rect = this.centerDiv.getBoundingClientRect();
	this.canvas.width = rect.width;
	this.canvas.height = rect.height;
	this.renderer.onResize();
	this.cameraMan.aspectRatio = this.canvas.width/this.canvas.height;
	this.cameraMan.UpdateCamera();
	this.Draw()
};

Main.prototype.createCanvas = function()
{
	this.canvas = document.createElement('canvas');
	this.canvas.width = window.innerWidth;
	this.canvas.height = window.innerHeight;
	this.centerDiv.appendChild(this.canvas);

	mouse.attach(this.canvas);

	this.renderer = new WebGLRenderer(this.canvas);

	this.renderer.camera = new Camera();

	this.renderer.camera.LookAt(V3(4, 4, 4), V3(), V3(0, 1, 0));

	this.cameraMan = new Camera_Movement(this.renderer.camera, function() { tc.Draw(); }, this.toolBar);
	this.cameraMan.aspectRatio = this.canvas.width/this.canvas.height;
	var tc = this;
	window.addEventListener('resize', function() { tc.ResizeCheck() });
	this.ResizeCheck();
	this.cameraMan.UpdateCamera();
};

//noinspection JSUnusedGlobalSymbols
Main.prototype.addTestCode = function()
{
	var itemTriangulate = this.contextMenu.AddItem("Test");
	itemTriangulate.onClick = function()
	{
		/*
		 simpleNumericForm.title="Test 1 line";
		 simpleNumericForm.AddNumericInput("single line input", 2.0, 0.1);
		 simpleNumericForm.onOK=function() { simpleNumericForm.Hide(); };
		 simpleNumericForm.onCancel=function() { simpleNumericForm.Hide(); };
		 simpleNumericForm.Show();
		 //*/
		/*
		 if( selectorTool.mode == 2 && selectorTool.nSelected == 1 )
		 {
		 var item = (new Selection()).items[0];
		 var face = item.geom.faces[item.indices[0]];
		 face.Triangulate();
		 }
		 //*/
	}
};

Main.prototype.createContextMenu = function()
{
	var tc = this;
	this.contextMenu = new Menu.Context();
	var rightObj = {};
	rightObj.OnMouseUp = function(evt) { tc.OnRightMouseUp(evt); };
	mouse.rightObj = rightObj;
	this.contextMenu.HideOnClick();
};

//endregion

Main.prototype.AddBorder = function(w)
{
	this.centerDiv.style.right = parseInt(this.centerDiv.style.right)+w+'px';
	this.ResizeCheck();
};

Main.prototype.AddDraw = function(DrawFn)
{
	this.DrawList.push(DrawFn)
};

Main.prototype.AddResetListener = function(fn)
{
	this.onReset.push(fn);
};

Main.prototype.Reset = function()
{
	this.geometries = [];
	this.cameraMan.reset();
	for(var i = 0; i<this.onReset.length; i++)
		this.onReset[i]();
	this.Draw();
};

Main.prototype.AddGeometriesListener = function(fn)
{
	this.onGeometriesChanged.push(fn);
};

Main.prototype.AddGeometry = function(geom)
{
	this.geometries.push(geom);
	for(var i = 0; i<this.onGeometriesChanged.length; i++)
		this.onGeometriesChanged[i](geom, 1);
	main.Draw();
};

Main.prototype.RemoveGeometry = function(geom)
{
	var geomId;
	if(geom)
	{
		geomId = this.geometries.indexOf(geom);
	} else
	{
		geomId = this.geometries.length-1;
		geom = this.geometries[geomId];
	}
	this.geometries.splice(geomId, 1);
	for(var i = 0; i<this.onGeometriesChanged.length; i++)
		this.onGeometriesChanged[i](geom, -1);
	main.Draw();
	return geom;
};


Main.prototype.Normalize2DPoint = function(x, y)
{
	var rect = this.centerDiv.getBoundingClientRect();
	x = x-rect.left;
	y = y-rect.top;
	var normX = -1+2*x/rect.width;
	var normY = 1-2*y/rect.height;
	return {x: normX, y: normY  }
};

Main.prototype.InvNormalize2DPoint = function(normX, normY)
{
	var rect = this.centerDiv.getBoundingClientRect();
	var x = ((normX+1)*rect.width)/2;
	var y = ((1-normY)*rect.height)/2;
	x = x+rect.left;
	y = y+rect.top;
	return {x: x, y: y  }
};

Main.prototype.Draw = function()
{
	this.renderer.Begin();
	var i;
	for(i = 0; i<this.geometries.length; i++)
	{
		this.geometries[i].Draw();
	}
	for(i = 0; i<this.DrawList.length; i++)
	{
		this.DrawList[i]();
	}
};

Main.prototype.OnRightMouseUp = function(event)
{
	this.contextMenu.Show(event.clientX, event.clientY);
};

Main.prototype.GetBBox = function()
{
	if(this.geometries.length>0)
	{
		var ret = this.geometries[0].GetBBox();
		for(var i = 1; i<this.geometries.length; i++)
		{
			ret.Grow(this.geometries[i].GetBBox());
		}
		return ret;
	} else
	{
		var r = new BBox(V3(-1, -1, -1));
		r.Grow(V3(1, 1, 1));
		return r;
	}
};

var main = new Main();

