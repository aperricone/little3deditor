/**
 * Creates a new _Plane object, the input can be: 4 numbers, a point and a number, 2 vector3 or 3 vector3:
 * 0 values: plane is y=0
 * 1 plane: copy the plane
 * 1 array or a _vector: normal is the parameter and d is the 4th element of array or 0
 * 2 _vector: normal is a, and d is a dot b
 * 1 _vector and 1 number: normal is a, and d is b
 * 3 _vector: the plane contains the triangle
 * 4 number: set the plane as ax+by+cz-d=0
 * @param {Number|Array|_Vector} [a=0] the value to assign a x element or the Array where copy the element from or the _Vector to copy
 * @param {Number|_Vector} [b=1] the value to assign to y element
 * @param {Number|_Vector} [c=0] the value to assign to z element
 * @param {Number} [d=0]the value to assign to d
 * @constructor
 */
function Plane(a, b, c, d)
{
	Plane.prototype.Set.apply(this, arguments);
}

Plane.prototype.calcNormal = function(a, b, c)
{
	var n = V3();
	n.x = (b.y-a.y)*(c.z-a.z)-(b.z-a.z)*(c.y-a.y);
	n.y = (b.z-a.z)*(c.x-a.x)-(b.x-a.x)*(c.z-a.z);
	n.z = (b.x-a.x)*(c.y-a.y)-(b.y-a.y)*(c.x-a.x);
	n.unit();
	//V3().cross(V3().sub(b, a), V3().sub(c, a)).unit()
	return n;
};

Plane.prototype.Set = function()
{
	switch(arguments.length)
	{
	case 0:
		this.normal = V3(0, 1, 0);
		this.distance = 0;
		break;
	case 1:
		if('normal' in arguments[0])
		{
			this.normal = arguments[0].normal;
			this.distance = arguments[0].distance
		} else
		{
			this.normal = V3(arguments[0]);
			this.distance = arguments[0][3] || 0;
		}
		break;
	case 2:
		this.normal = V3(arguments[0]);
		var b = arguments[1];
		if(typeof(b)== typeof(123))
		{
			this.distance = b;
		} else
		{
			this.distance = V3(b).dot(this.normal);
		}
		break;
	case 3:
		var a = arguments[0], b = arguments[1], c = arguments[2];
		this.normal = this.calcNormal(a, b, c);
		this.distance = a.dot(this.normal);
		break;
	case 4:
		this.normal = V3(arguments[0], arguments[1], arguments[2]);
		this.distance = arguments[3];
		break;
	}
};

/**
 *
 * @param {_Vector}start
 * @param {_Vector}direx
 * @returns {Number}
 */
Plane.prototype.IntersectWithLine = function(start, direx)
{
	var den = direx.dot(this.normal);
	if(Math.abs(den)<1e-3) return Infinity;
	return (this.distance-start.dot(this.normal))/den
};

/**
 * @return {number}
 */
Plane.prototype.Check = function(point)
{
	return this.normal.dot(point)-this.distance;
};