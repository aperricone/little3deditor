/**
 * default comparison function
 * TODO: it is already present in javascript?
 * @param a first element to compare
 * @param b second element to compare
 * @returns {number} a value ==0 if a and b are the same, a value >0 if a is greater than b, a value <0 if a is lower than b,
 */
function defaultSort(a,b) { return a-b; }

/**
 * sorted array, an derived of array that maintains its element sorted by given comparison function
 * @param {function} [cmpFn] a comparison function that takes 2 arguments and returns 0 if they are equals, 1 if the
 * first is greater than second, and -1 otherwise
 * @property {function} cmpFn
 * @property {Boolean} allowDuplicated
 * @constructor
 */
function sortedArray(cmpFn)
{
	Array.call(this,0);
	this.cmpFn = cmpFn;
	this.allowDuplicated = true;
	if( cmpFn == undefined )
		this.cmpFn = defaultSort;
}

sortedArray.prototype = [];
sortedArray.prototype.constructor = sortedArray;

/**
 * overloaded from array, the second and the third element are internal use
 * @param {*} element element to search
 * @param {Number} [start] start index where search
 * @param {Number} [end] end index where search
 * @returns {Number} index of element or the index or index of element just before it if it is not present
 */
sortedArray.prototype.indexOf = function(element,start,end)
{
	if( this.length == 0 ) return 0;
	start = start || 0;
	end = end!=undefined? end : this.length;
	var pivot = Math.floor(start + (end - start) / 2);
	var cmpResult = this.cmpFn(this[pivot],element);
	if(cmpResult == 0) return pivot;
	if( end-start <= 1 )
		return cmpResult < 0? pivot+1 : pivot;
	if(cmpResult < 0)
	{
		return this.indexOf(element, pivot, end);
	} else
	{
		return this.indexOf(element, start, pivot);
	}
};

/**
 * Insert an element in the sorted array in the right position.
 * @param {*} element element to add
 * @returns {boolean} true if element was inserted
 */
sortedArray.prototype.insert = function(element)
{
	if( this.length == 0 )
	{
		this.push(element);
		return true;
	}
	var idx = this.indexOf(element);
	if( !this.allowDuplicated && this[idx]!=undefined && this.cmpFn(this[idx],element) == 0 )
		return false;
	this.splice(idx,0,element);
	return true;
};
