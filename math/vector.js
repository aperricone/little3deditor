/**
 * Creates a new 2D vector
 * @param {Number|Array|_Vector}[x]
 * @param {Number}[y]
 * @returns {_Vector} the new vector with 2Dimension
 */
function V2(x, y)
{
	var p = new _Vector(2);
	p.Set.apply(p, arguments);
	return p;
}

/**
 *
 * @param {Number|Array|_Vector}[x]
 * @param {Number}[y]
 * @param {Number}[z]
 * @returns {_Vector}
 */
function V3(x, y, z)
{
	var p = new _Vector(3);
	p.Set.apply(p, arguments);
	return p;
}

/**
 *
 * @param {Number|Array|_Vector}[x]
 * @param {Number}[y]
 * @param {Number}[z]
 * @param {Number}[w]
 * @returns {_Vector}
 */
function V4(x, y, z, w)
{
	var p = new _Vector(4);
	p.Set.apply(p, arguments);
	return p;
}

/**
 *
 * @param {Number}len the dimension of vector.
 */
function _Vector(len)
{
	if(typeof(len)!= typeof(123)) throw "invalid call";
	this.length = len;
	for(var i = 0; i<this.length; i++) this[i] = 0;
	this.dirty = true;
}

_Vector.prototype = [];
_Vector.prototype.constructor = _Vector;

_Vector.prototype.Set = function(a)
{
	var src = arguments;
	if(a && typeof(a)!= typeof(123) && 'length' in a)
	{
		src = a;
	}
	var n = this.length<src.length ? this.length : src.length;
	for(var i = 0; i<n; i++)
	{
		if(typeof(src[i])== typeof(123))
		{
			this[i] = src[i];
		}
	}
	for(; i<this.length; i++)
	{
		this[i] = 0;
	}
	this.dirty = true;
};

/**
 * Return true if obj is a vector and, if request, its length is len
 * @param {*}obj
 * @param {Number}[len]
 * @returns {Boolean}
 */
_Vector.isVector = function(obj, len)
{
	if('_isVector' in obj) return obj._isVector(len);
	return false;
};

_Vector.prototype._isVector = function(len)
{
	if(len!=undefined)
	{
		if(this.length!=len)
			return false;
	}
	return true;
};

_Vector.prototype.setWithTest = function(id, v)
{
	if(this.length<=id)
		throw "invalid call";
	this[id] = v;
	this.dirty = true;
};

_Vector.prototype.__defineGetter__('x', function()
{
	if(this.length<1) throw "invalid call";
	return this[0];
});
_Vector.prototype.__defineGetter__('y', function()
{
	if(this.length<2) throw "invalid call";
	return this[1];
});
_Vector.prototype.__defineGetter__('z', function()
{
	if(this.length<3) throw "invalid call";
	return this[2];
});
_Vector.prototype.__defineGetter__('w', function()
{
	if(this.length<4) throw "invalid call";
	return this[3];
});
_Vector.prototype.__defineSetter__('x', function(v) { this.setWithTest(0, v); });
_Vector.prototype.__defineSetter__('y', function(v) { this.setWithTest(1, v); });
_Vector.prototype.__defineSetter__('z', function(v) { this.setWithTest(2, v); });
_Vector.prototype.__defineSetter__('w', function(v) { this.setWithTest(3, v); });

_Vector.prototype.add = function(a, b)
{
	if(!_Vector.isVector(a, this.length)) throw "invalid parameter";
	if(b!=undefined)
	{
		if(!_Vector.isVector(b, this.length)) throw "invalid parameter";
		for(var i = 0; i<this.length; i++) this[i] = a[i]+b[i];
	} else
	{
		for(var i = 0; i<this.length; i++) this[i] += a[i];
	}
	this.dirty = true;
	return this;
};

_Vector.prototype.sub = function(a, b)
{
	if(!_Vector.isVector(a, this.length)) throw "invalid parameter";
	if(b!=undefined)
	{
		if(!_Vector.isVector(b, this.length)) throw "invalid parameter";
		for(var i = 0; i<this.length; i++) this[i] = a[i]-b[i];
	} else
	{
		for(var i = 0; i<this.length; i++) this[i] -= a[i];
	}
	this.dirty = true;
	return this;
};

_Vector.prototype.scale = function(a)
{
	if(typeof(a)!= typeof(123)) throw "invalid parameter";
	for(var i = 0; i<this.length; i++) this[i] *= a;
	this.dirty = true;
	return this
};

/**
 * Dot product
 * @param {_Vector} a
 * @returns {number}
 */
_Vector.prototype.dot = function(a)
{
	if(!_Vector.isVector(a, this.length))  throw "invalid parameter";
	var r = 0;
	for(var i = 0; i<this.length; i++) r += this[i]*a[i];
	return r;
};

_Vector.prototype.distanceSquare = function(a)
{
	if(!_Vector.isVector(a, this.length))  throw "invalid parameter";
	var r = 0;
	for(var i = 0; i<this.length; i++)
	{
		var e = (this[i]-a[i]);
		r += e*e;
	}
	return r;
};

_Vector.prototype.cross = function(a, b)
{
	for(var i = 0; i<arguments.length; i++)
	{
		var v = arguments[i];
		if(!_Vector.isVector(v, this.length))  throw "invalid parameter";
	}
	if(arguments.length!=this.length-1)
	{ // it is a warning
		if(arguments.length<this.length-1)
		{
			throw "Cross takes length-1 parameters";
		} else
			console.log("WARNING: Cross takes length-1 parameters");
	}
	if(this.length==2)
	{
		//noinspection JSSuspiciousNameCombination
		this.x = a.y;
		this.y = -a.x;
		return this;
	} else if(this.length==3)
	{
		this.x = a.y*b.z-a.z*b.y;
		this.y = a.z*b.x-a.x*b.z;
		this.z = a.x*b.y-a.y*b.x;
		this.dirty = true;
	} else
	{ //noinspection SpellCheckingInspection
		{
			// with a class matrix it is possible do cross for all vector of len >= 3, for example:
			// cross 4D (a,b,c)
			// |  x  y  z  w |
			// | ax ay az aw |
			// | bx by bz bw |
			// | cx cy cz cw |
			//          | ay az aw |          | ax az aw |          | ax ay aw |          | ax ay az |
			// rx = Det | by bz bw | ry = Det | bx bz bw | rz = Det | bx by bw | rw = Det | bx by bz |
			//          | cy cz cw |          | cx cz cw |          | cx cy cw |          | cx cy cz |
			// so cross 5D takes 4 parameters and so on... like Cross::nonn1 of Wolfram Mathematica 9
			throw "invalid type";
		}
	}
	return this;
};

_Vector.prototype.unit = function()
{
	var len = this.dot(this);
	if(len>0)
		this.scale(1/Math.sqrt(len));
	return this
};

_Vector.prototype.addScaled = function(a, s)
{
	if(!_Vector.isVector(a, this.length)) throw "invalid parameter";
	if(typeof(s)!= typeof(123)) throw "invalid parameter";
	for(var i = 0; i<this.length; i++) this[i] += a[i]*s;
	this.dirty = true;
	return this;
};

/**
 * Converts to string
 * @param {Number}[numberDecimal=2]
 * @returns {string}
 */
_Vector.prototype.toFixed = function(numberDecimal)
{
	if(typeof(numberDecimal)!= typeof(123)) numberDecimal = 2;
	var ret = "[ ";
	for(var i = 0; i<this.length; i++)
	{
		if(i>0) ret += ", ";
		ret += this[i].toFixed(numberDecimal);
	}
	ret += " ]";
	return ret;
};
