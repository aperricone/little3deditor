function BBox(len)
{
	if(typeof(len)== typeof(123))
	{
		this.length = len;
	} else if('length' in len)
	{
		this.length = len.length;
	} else
	{
		throw "invalid call";
	}
	this.min = new _Vector(this.length);
	this.max = new _Vector(this.length);
	if(typeof(len)== typeof(123))
	{
		for(var i = 0; i<this.min.length; i++)
		{
			this.min[i] = Infinity;
			this.max[i] = -Infinity;
		}
	} else
	{
		this.min.Set(len);
		this.max.Set(len);
	}
}

BBox.prototype.__defineGetter__('center', function()
{
	return new _Vector(this.length).add(this.min, this.max).scale(0.5);
});

BBox.prototype.__defineGetter__('size', function()
{
	return new _Vector(this.length).sub(this.max, this.min);
});

BBox.prototype.Grow = function(point)
{
	if(point.length!=this.length) throw "invalid parameter";
	if('min' in point)
	{ // parameter is a BBox
		this.Grow(point.min);
		this.Grow(point.max);
		return;
	}

	for(var i = 0; i<this.length; i++)
	{
		if(point[i]<this.min[i]) this.min[i] = point[i];
		if(point[i]>this.max[i]) this.max[i] = point[i];
	}
};
