/**
 * Utility algorithm that connect element and quickly says if they are connect.
 * {@link http://www.cs.princeton.edu/~rs/AlgsDS07/01UnionFind.pdf|Better explanation here}
 * @param {Number} n number of element in the group
 * @constructor
 */
function UnionFind(n)
{
	this.id = new Array(n);
	this.sz = new Array(n);
	for(var i = 0; i<n; i++)
	{
		this.id[i] = i;
		this.sz[i] = 1;
	}
}

/**
 * Util function that return the current root of element i-th
 * @param {Number} i element id
 * @returns {Number} root Id
 */
UnionFind.prototype.root = function(i)
{
	while(i!=this.id[i])
	{
		this.id[i] = this.id[this.id[i]];
		i = this.id[i];
	}
	return i;
};

/**
 * Check if 2 element are connected
 * @param {Number} p first element to check
 * @param {Number} q second element to check
 * @returns {boolean} true if they are connect
 */
UnionFind.prototype.connected = function(p, q)
{
	return this.root(p)==this.root(q);
};

/**
 * Connect 2 element
 * @param {Number} p first element to connect
 * @param {Number} q second element to connect
 */
UnionFind.prototype.union = function(p, q)
{
	var i = this.root(p);
	var j = this.root(q);
	if( i!=j )
	{
		if (this.sz[i] < this.sz[j]) { this.id[i] = j; this.sz[j] += this.sz[i]; }
								else { this.id[j] = i; this.sz[i] += this.sz[j]; }
	}
};

/**
 * it returns grouped elements.
 * @returns {{}} groups
 */
UnionFind.prototype.groups = function()
{
	var ret = {};
	for(var i = 0; i<this.id.length; i++)
	{
		var ri = this.root(i).toString();
		if(!(ri in ret)) ret[ri] = [];
		ret[ri].push(i);
	}
	return ret;
};

