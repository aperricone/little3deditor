function DynamicGLBuffer(gl)
{
	this.gl = gl;
	this.length = 0;
	this.array = undefined;
	this.buffer = gl.createBuffer();
}

DynamicGLBuffer.prototype.Update = function(array)
{
	this.length = array.length;
	var needUpdate = false;
	var needResize = false;
	if(this.array==undefined || this.array.length<array.length)
	{
		needUpdate = true;
		needResize = true;

	} else
	{
		for(var i = 0; i<array.length; i++)
		{
			if(this.array[i]!=array[i])
			{
				needUpdate = true;
				break;
			}
		}
	}
	if(needUpdate)
	{
		var gl = this.gl;
		if(needResize)
		{
			this.array = new Float32Array(array.length);
		}
		this.array.set(array);
		gl.bindBuffer(gl.ARRAY_BUFFER, this.buffer);
		if(needResize)
		{
			gl.bufferData(gl.ARRAY_BUFFER, this.array, gl.DYNAMIC_DRAW);
		} else
		{
			gl.bufferSubData(gl.ARRAY_BUFFER, 0, this.array);
		}
	}
}

DynamicGLBuffer.prototype.Setup = function(vtxSize,indx,size)
{
	var gl = this.gl;
	var offset=0;
	vtxSize*=4;
	gl.bindBuffer(gl.ARRAY_BUFFER, this.buffer);
	for(var i=1;i<arguments.length;i+=2)
	{
		indx = arguments[i];
		size = arguments[i+1];
		gl.enableVertexAttribArray(indx);
		gl.vertexAttribPointer(indx, size, gl.FLOAT, false, vtxSize, offset);
		offset += 4*size;
	}
}
