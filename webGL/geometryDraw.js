Geometry.GeometryDraw = function(parent,rr)
{
	this.parent = parent;
	this.renderer = rr;
	// draw vertices
	this.verticesBuffer = new DynamicGLBuffer(rr.gl);
	// draw edge
	this.edgesBuffer = new DynamicGLBuffer(rr.gl);
	// draw triangles
	this.triBuffer  = new DynamicGLBuffer(rr.gl);

	this.facesVisibility = [];

};

Geometry.prototype.Draw = function()
{
	if(this.visible)
		this.drawer.Draw();
};

Geometry.GeometryDraw.prototype.Draw = function()
{
	this.UpdateFacesVisibility();
	if(( selectorTool.mode==0 || selectorTool.mode==4 ) && !this.lock)
	{
		this.UpdateVerticesArray();
		this.DrawVertices();
	}
	var thick = (( selectorTool.mode==1 || selectorTool.mode==4 ) && !this.lock);

	this.UpdateEdgesArray();
	this.DrawEdges(thick);

	//this.DrawNormals();

	if(!this.parent.wire)
	{
		this.UpdateTrianglesArray();
		this.DrawTriangles();
	}
};

Geometry.GeometryDraw.prototype.UpdateFacesVisibility = function()
{
	if(this.facesVisibility.length!=this.parent.faces.length)
	{
		this.facesVisibility = new Array(this.parent.faces.length);
	}
	var c = this.renderer.camera;
	for(var i = 0; i<this.parent.faces.length; i++)
	{
		var face = this.parent.faces[i];
		if(c.ortho)
			this.facesVisibility[i] = face.normal.dot(c.at)>0; else
			this.facesVisibility[i] = face.normal.dot(V3(c.pos).sub(face.center))>0;
	}
};

Geometry.GeometryDraw.prototype.UpdateVerticesArray = function()
{
	var array = [];
	for(var i = 0; i<this.parent.vertices.length; i++)
	{
		var vtx = this.parent.vertices[i];
		var visible = false;
		for(var j = 0; j<vtx.faces.length; j++)
		{
			if(this.facesVisibility[vtx.faces[j]])
			{
				visible = true;
				break;
			}
		}
		if(visible)
		{
			var color = V3(0, 0, 0);
			if(vtx.selected) color.x = 0.75;
			if(vtx.marked) color.y = 0.75;
			array.push( vtx.x,vtx.y,vtx.z, color.x, color.y, color.z )
		}
	}
	this.verticesBuffer.Update(array);
};

Geometry.GeometryDraw.prototype.DrawVertices = function()
{
	this.renderer.DrawPoints(this.verticesBuffer,6);
};

Geometry.GeometryDraw.prototype.UpdateEdgesArray = function()
{
	var array = [];
	var arrayThick = [];
	for(var i = 0; i<this.parent.edges.length; i++)
	{
		var edge = this.parent.edges[i];
		var visible = false;
		for(var j = 0; j<edge.faces.length; j++)
		{
			var faceId = edge.faces[j];
			if(faceId!=undefined && this.facesVisibility[faceId])
			{
				visible = true;
				break;
			}
		}
		if(visible)
		{
			var color = V3(0, 0, 0);
			if(!this.lock)
			{
				if(edge.selected) color.x = 0.75;
				if(edge.marked) color.y = 0.75;
			}
			if(edge.hardness) color.z = 0.75;
			var a = edge.GetPoint(0);
			var b = edge.GetPoint(1);
			arrayThick.push( a.x, a.y, a.z, 1, b.x, b.y, b.z, color.x,color.y,color.z);
			arrayThick.push( a.x, a.y, a.z,-1, b.x, b.y, b.z, color.x,color.y,color.z);
			arrayThick.push( b.x, b.y, b.z, 1, a.x, a.y, a.z, color.x,color.y,color.z);
			arrayThick.push( b.x, b.y, b.z,-1, a.x, a.y, a.z, color.x,color.y,color.z);
			array.push( a.x, a.y, a.z, color.x,color.y,color.z);
			array.push( b.x, b.y, b.z, color.x,color.y,color.z);
		}
	}
	this.edgesBuffer.Update(array);
};

Geometry.GeometryDraw.prototype.DrawEdges = function(thick)
{
	if( thick )
	{
		this.renderer.SetLineWidth(3);
		for(var i = 0; i<this.parent.edges.length; i++)
		{
			var edge = this.parent.edges[i];
			var visible = false;
			for(var j = 0; j<edge.faces.length; j++)
			{
				var faceId = edge.faces[j];
				if(faceId!=undefined && this.facesVisibility[faceId])
				{
					visible = true;
					break;
				}
			}
			if(visible)
			{
				var color = V3(0, 0, 0);
				if(!this.lock)
				{
					if(edge.selected) color.x = 0.75;
					if(edge.marked) color.y = 0.75;
				}
				if(edge.hardness) color.z = 0.75;
				var a = edge.GetPoint(0);
				var b = edge.GetPoint(1);
				this.renderer.DrawLine(a,b,color);
			}
		}
		this.renderer.SetLineWidth(1);
	} else
		this.renderer.DrawLines(this.edgesBuffer);
};

Geometry.GeometryDraw.prototype.UpdateTrianglesArray = function()
{
	var array = [];
	for(var i = 0; i<this.facesVisibility.length; i++)
	{
		if(this.facesVisibility[i])
		{
			var face = this.parent.faces[i];
			var color = V3(0.8, 0.8, 0.7);
			if(!this.lock)
			{
				if(face.marked || this.parent.marked)
				{
					color.Set(0.25, 0.75, 0.25);
				} else if(face.selected || this.parent.selected)
				{
					color.Set(0.75, 0.25, 0.25);
				}
			}
			for(var j = 0; j<face.triangles.length; j++)
			{
				var tri = face.triangles[j];
				for(var k = 0; k<3; k++)
				{
					var v = face.GetPoint(tri[k]);
					var n = face.normals[tri[k]];
					array.push(v.x, v.y, v.z, n.x, n.y, n.z, color.x, color.y, color.z );
				}
			}
		}
	}
	this.triBuffer.Update(array);

};

Geometry.GeometryDraw.prototype.DrawTriangles = function()
{
	this.renderer.DrawTriangles(this.triBuffer);
};

//DEBUG
Geometry.GeometryDraw.prototype.DrawNormals = function()
{
	var color = V3(0, 0, 0);
	for(var i = 0; i<this.facesVisibility.length; i++)
	{
		if(this.facesVisibility[i])
		{
			var face = this.parent.faces[i];
			for(var j = 0; j<face.indices.length; j++)
			{
				this.renderer.DrawLine(face.GetPoint(j), V3(face.GetPoint(j)).add(face.normals[j]), color);
			}
		}
	}
};

