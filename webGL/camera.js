/**
 * @class
 * @property {Float32Array} perspective The perspective matrix
 * @property {Float32Array} view The view matrix
 * @property {Float32Array} __pv The multiplication between perspective and view
 * @property {Float32Array} __pv_inv The inverse of multiplication between perspective and view
 * @property {Boolean} __pv_dirty A flag that indicates when __pv is dirty and needs to be recalculate
 * @property {Boolean} __pv_inv_dirty A flag that indicates when __pv_inv is dirty and needs to be recalculate
 */
function Camera()
{
	this.perspective = new Float32Array([1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 ]);
	this.view = new Float32Array([1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 ]);

	this.__pv = new Float32Array([1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 ]);
	this.__pv_dirty = false;
	this.__pv_inv = new Float32Array([1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 ]);
	this.__pv_inv_dirty = false;
	this.ortho = true;
	this.rg = V3(1, 0, 0);
	this.up = V3(0, 1, 0);
	this.at = V3(0, 0, 1);
	this.pos = V3(0, 0, 0);
	// height of pixel normalized, where the screen is tall 2 = zFixed + z * zFact
	this.zFact = 0;
	this.zFixed = 1;
	this.__frustum = new Array(6);
	this.__frustum_dirty = true;
	for(var i = 0; i<6; i++)
	{
		this.__frustum[i] = new Plane();
	}
}

/**
 * Return the
 * @returns {Float32Array} The multiplication between perspective and view
 * @constructor
 */
Camera.prototype.GetPV = function()
{
	if(this.__pv_dirty)
	{
		for(var rc = 0; rc<4; rc++)
		{
			for(var rr = 0; rr<4; rr++)
			{
				var idx = rr+rc*4;
				this.__pv[idx] = 0;
				for(var e = 0; e<4; e++)
				{
					this.__pv[idx] += this.view[e+rc*4]*this.perspective[rr+e*4]
				}
			}
		}
		this.__pv_dirty = false;
		this.__pv_inv_dirty = true;
	}
	return this.__pv;
};

Camera.prototype.GetPVInv = function()
{
	if(this.__pv_dirty) this.GetPV();
	if(this.__pv_inv_dirty)
	{
		var m = this.__pv;
		var m_inv = [];
		m_inv[ 0] = m[ 5]*m[10]*m[15]-m[ 5]*m[11]*m[14]-m[ 9]*m[ 6]*m[15]+m[ 9]*m[ 7]*m[14]+m[13]*m[ 6]*m[11]-m[13]*m[ 7]*m[10];
		m_inv[ 4] = -m[ 4]*m[10]*m[15]+m[ 4]*m[11]*m[14]+m[ 8]*m[ 6]*m[15]-m[ 8]*m[ 7]*m[14]-m[12]*m[ 6]*m[11]+m[12]*m[ 7]*m[10];
		m_inv[ 8] = m[ 4]*m[ 9]*m[15]-m[ 4]*m[11]*m[13]-m[ 8]*m[ 5]*m[15]+m[ 8]*m[ 7]*m[13]+m[12]*m[ 5]*m[11]-m[12]*m[ 7]*m[ 9];
		m_inv[12] = -m[ 4]*m[ 9]*m[14]+m[ 4]*m[10]*m[13]+m[ 8]*m[ 5]*m[14]-m[ 8]*m[ 6]*m[13]-m[12]*m[ 5]*m[10]+m[12]*m[ 6]*m[ 9];
		m_inv[ 1] = -m[ 1]*m[10]*m[15]+m[ 1]*m[11]*m[14]+m[ 9]*m[ 2]*m[15]-m[ 9]*m[ 3]*m[14]-m[13]*m[ 2]*m[11]+m[13]*m[ 3]*m[10];
		m_inv[ 5] = m[ 0]*m[10]*m[15]-m[ 0]*m[11]*m[14]-m[ 8]*m[ 2]*m[15]+m[ 8]*m[ 3]*m[14]+m[12]*m[ 2]*m[11]-m[12]*m[ 3]*m[10];
		m_inv[ 9] = -m[ 0]*m[ 9]*m[15]+m[ 0]*m[11]*m[13]+m[ 8]*m[ 1]*m[15]-m[ 8]*m[ 3]*m[13]-m[12]*m[ 1]*m[11]+m[12]*m[ 3]*m[ 9];
		m_inv[13] = m[ 0]*m[ 9]*m[14]-m[ 0]*m[10]*m[13]-m[ 8]*m[ 1]*m[14]+m[ 8]*m[ 2]*m[13]+m[12]*m[ 1]*m[10]-m[12]*m[ 2]*m[ 9];
		m_inv[ 2] = m[ 1]*m[ 6]*m[15]-m[ 1]*m[ 7]*m[14]-m[ 5]*m[ 2]*m[15]+m[ 5]*m[ 3]*m[14]+m[13]*m[ 2]*m[ 7]-m[13]*m[ 3]*m[ 6];
		m_inv[ 6] = -m[ 0]*m[ 6]*m[15]+m[ 0]*m[ 7]*m[14]+m[ 4]*m[ 2]*m[15]-m[ 4]*m[ 3]*m[14]-m[12]*m[ 2]*m[ 7]+m[12]*m[ 3]*m[ 6];
		m_inv[10] = m[ 0]*m[ 5]*m[15]-m[ 0]*m[ 7]*m[13]-m[ 4]*m[ 1]*m[15]+m[ 4]*m[ 3]*m[13]+m[12]*m[ 1]*m[ 7]-m[12]*m[ 3]*m[ 5];
		m_inv[14] = -m[ 0]*m[ 5]*m[14]+m[ 0]*m[ 6]*m[13]+m[ 4]*m[ 1]*m[14]-m[ 4]*m[ 2]*m[13]-m[12]*m[ 1]*m[ 6]+m[12]*m[ 2]*m[ 5];
		m_inv[ 3] = -m[ 1]*m[ 6]*m[11]+m[ 1]*m[ 7]*m[10]+m[ 5]*m[ 2]*m[11]-m[ 5]*m[ 3]*m[10]-m[ 9]*m[ 2]*m[ 7]+m[ 9]*m[ 3]*m[ 6];
		m_inv[ 7] = m[ 0]*m[ 6]*m[11]-m[ 0]*m[ 7]*m[10]-m[ 4]*m[ 2]*m[11]+m[ 4]*m[ 3]*m[10]+m[ 8]*m[ 2]*m[ 7]-m[ 8]*m[ 3]*m[ 6];
		m_inv[11] = -m[ 0]*m[ 5]*m[11]+m[ 0]*m[ 7]*m[ 9]+m[ 4]*m[ 1]*m[11]-m[ 4]*m[ 3]*m[ 9]-m[ 8]*m[ 1]*m[ 7]+m[ 8]*m[ 3]*m[ 5];
		m_inv[15] = m[ 0]*m[ 5]*m[10]-m[ 0]*m[ 6]*m[ 9]-m[ 4]*m[ 1]*m[10]+m[ 4]*m[ 2]*m[ 9]+m[ 8]*m[ 1]*m[ 6]-m[ 8]*m[ 2]*m[ 5];

		var det = m[0]*m_inv[0]+m[1]*m_inv[4]+m[2]*m_inv[8]+m[3]*m_inv[12];
		if(det!=0)
		{
			for(var i = 0; i<16; i++)
				m_inv[i] /= det;
		}
		//noinspection JSValidateTypes
		this.__pv_inv.set(m_inv);
		this.__pv_inv_dirty = false;
	}
	return this.__pv_inv;
};

/**
 * Setup the perspective matrix as perspective view from human-friendly values
 * @param {Number}fovY the vertical field of view in degrees
 * @param {Number}aspect the aspect ratio of viewport
 * @param {Number}zNear the near clip plane distance
 * @param {Number}zFar the far clip plane distance
 */
Camera.prototype.SetupPerspective = function(fovY, aspect, zNear, zFar)
{
	var yMax = zNear*Math.tan(fovY*Math.PI/360.0);
	var yMin = -yMax;
	var xMin = yMin*aspect;
	var xMax = yMax*aspect;

	this.SetupFrustum(xMin, xMax, yMin, yMax, zNear, zFar);
};

/**
 * Setup the perspective matrix as a perspective view from frustum size
 * @param {Number}left the left limit of viewport
 * @param {Number}right the right limit of viewport
 * @param {Number}bottom the bottom limit of viewport
 * @param {Number}top the top limit of viewport
 * @param {Number}zNear the near clip plane distance
 * @param {Number}zFar the far clip plane distance
 */
Camera.prototype.SetupFrustum = function(left, right, bottom, top, zNear, zFar)
{
	var X = 2*zNear/(right-left);
	var Y = 2*zNear/(top-bottom);
	var A = (right+left)/(right-left);
	var B = (top+bottom)/(top-bottom);
	var C = -(zFar+zNear)/(zFar-zNear);
	var D = -2*zFar*zNear/(zFar-zNear);
	// top-bottom is the size at z = near so.. at z=2*near the size is top-bottom*2
	// so.. at z=1 size is: top-bottom/near
	this.zFixed = 0;
	this.zFact = (top-bottom)/zNear;

	//noinspection JSValidateTypes
	this.perspective.set([  X, 0, 0, 0, 0, Y, 0, 0, A, B, C, -1, 0, 0, D, 0]);
	this.ortho = false;
	this.__pv_dirty = true;
};

/**
 * Setup the perspective matrix as orthographic view from human-friendly values
 * @param {Number}sizeY the vertical size of view
 * @param {Number}aspect the aspect ratio of viewport
 * @param {Number}zNear the near clip plane distance
 * @param {Number}zFar the far clip plane distance
 */
Camera.prototype.SetupOrthographic = function(sizeY, aspect, zNear, zFar)
{
	var dy = sizeY/2;
	var dx = dy*aspect;
	this.SetupOrtho(-dx, dx, -dy, dy, zNear, zFar)
};

/**
 * Setup the perspective matrix as a orthographic view from frustum size
 * @param {Number}left the left limit of viewport
 * @param {Number}right the right limit of viewport
 * @param {Number}bottom the bottom limit of viewport
 * @param {Number}top the top limit of viewport
 * @param {Number}zNear the near clip plane distance
 * @param {Number}zFar the far clip plane distance
 */
Camera.prototype.SetupOrtho = function(left, right, bottom, top, zNear, zFar)
{
	var tx = -(right+left)/(right-left);
	var ty = -(top+bottom)/(top-bottom);
	var tz = -(zFar+zNear)/(zFar-zNear);
	// top-bottom is the size at every z distance
	this.zFixed = (top-bottom);
	this.zFact = 0;

	//noinspection JSValidateTypes
	this.perspective.set([
		2/(right-left), 0, 0, 0, 0, 2/(top-bottom), 0, 0, 0, 0, -2/(zFar-zNear), 0, tx, ty, tz, 1]);
	this.__pv_dirty = true;
	this.ortho = true;
};

/**
 * Setup the view matrix for look at point
 * @param {_Vector}eye Camera position
 * @param {_Vector}center center of view
 * @param {_Vector}[up=V3(0,1,0)] up vector of view
 */
Camera.prototype.LookAt = function(eye, center, up)
{
	up = up || V3(0, 1, 0);
	this.at.sub(eye, center).unit();
	this.rg.cross(up, this.at).unit();
	this.up.cross(this.at, this.rg).unit();
	this.pos.Set(eye);

	var ps = V3(-this.rg.dot(eye), -this.up.dot(eye), -this.at.dot(eye));

	//noinspection JSValidateTypes
	this.view.set(
		[   this.rg.x, this.up.x, this.at.x, 0, this.rg.y, this.up.y, this.at.y, 0, this.rg.z, this.up.z, this.at.z, 0,
			ps.x, ps.y, ps.z, 1 ], 0);
	this.__pv_dirty = true;
};

/**
 * Converts a 3D point to 2D coordinate
 * @param {_Vector}point the point to convert
 * @returns {_Vector} a 2d point between -1 and 1 in all axis
 */
Camera.prototype.To2D = function(point)
{
	if(!_Vector.isVector(point))
		point = V3.apply(undefined, arguments);
	var pv = this.GetPV();
	var r = [0, 0, 0, 0];
	var p = [point.x, point.y, point.z, 1];
	for(var i = 0; i<4; i++)
		for(var j = 0; j<4; j++)
		{
			r[i] += p[j]*pv[j*4+i]
		}
	r[3] = Math.abs(r[3]);
	return V3(r[0]/r[3], r[1]/r[3], r[2]/r[3])
};

/**
 * Converts a 2D point to 3D coordinate
 * @param {Number|Array|_Vector}point the point to convert
 * @returns {_Vector} a 3d point
 */
Camera.prototype.To3D = function(point)
{
	if(!_Vector.isVector(point))
		point = V3.apply(undefined, arguments);
	var pvi = this.GetPVInv();
	var r = [0, 0, 0, 0];
	var p = [point.x, point.y, point.z, 1];
	for(var i = 0; i<4; i++)
		for(var j = 0; j<4; j++)
		{
			r[i] += p[j]*pvi[j*4+i]
		}
	return V3(r[0]/r[3], r[1]/r[3], r[2]/r[3])
};

//noinspection JSUnusedGlobalSymbols
Camera.prototype.GetFrustum = function()
{
	if(this.__frustum_dirty)
	{
		var p = [ this.To3D(-1, -1, -1), this.To3D(1, -1, -1), this.To3D(-1, 1, -1), this.To3D(1, 1, -1),
			this.To3D(-1, -1, 1), this.To3D(1, -1, 1), this.To3D(-1, 1, 1)/*,this.To3D( 1, 1, 1)*/ ];
		this.__frustum_dirty = false;
		this.__frustum[0].set(p[0], p[1], p[2]); //near
		this.__frustum[1].set(p[4], p[5], p[6]); //far
		this.__frustum[2].set(p[0], p[2], p[4]); //left
		this.__frustum[3].set(p[1], p[3], p[5]); //right
		this.__frustum[4].set(p[2], p[3], p[6]); //top
		this.__frustum[5].set(p[0], p[1], p[4]); //bottom
	}
	return this.__frustum;
};
