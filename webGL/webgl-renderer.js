/**
 *
 * @param {HTMLCanvasElement}canvas
 * @constructor
 */
function WebGLRenderer(canvas)
{
	//******** INIT WebGL
	/** @type {HTMLCanvasElement} */
	this.canvas = canvas;
	/** @type {WebGLRenderingContext} */
	this.gl = null;
	//noinspection SpellCheckingInspection
	var possibilities = ["webgl", "experimental-webgl", "moz-webgl", "webkit-3d", "3d"];
	for(var i = 0; i<possibilities.length; i++)
	{
		try
		{
			this.gl = this.canvas.getContext(possibilities[i]);
			if(this.gl!=null)
			{
				break
			}
		} catch(e)
		{
		}
	}
	if(this.gl==null) return;
	//******** INIT OpenGL
	var gl = this.gl;
	gl.clearColor(0.75, 0.75, 0.75, 1.0);
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
	gl.enable(gl.BLEND);
	gl.depthFunc(gl.LEQUAL);
	gl.viewport(0, 0, this.canvas.width, this.canvas.height);
	gl.cullFace(gl.BACK);
	gl.enable(gl.CULL_FACE);
	//gl.disable(gl.CULL_FACE);
	this.camera = null;

	// *************** Setup flat rendering
	this.SetupShaders();
	this.drawLineVBuffer = undefined;
	this.drawTriangleVBuffers = undefined;
	this.drawQuadVBuffers = undefined;
}

WebGLRenderer.prototype.SetupShaders = function()
{
	var gl = this.gl;

	this.constantShader = this.GetProgram("attribute vec3 aVertexPosition;\nuniform mat4 uPVMatrix;\n\nvoid main(void)\n{\n\tgl_Position = uPVMatrix * vec4(aVertexPosition, 1.0);\n}\n",
		"#ifdef GL_ES\nprecision mediump float;\n#endif\nuniform vec3 color;\n\nvoid main(void)\n{\n\tgl_FragColor = vec4(color,1.0);\n}\n");
	this.constantShader.posAttribute = gl.getAttribLocation(this.constantShader, "aVertexPosition");
	this.constantShader.pvMatrixUniform = gl.getUniformLocation(this.constantShader, "uPVMatrix");
	this.constantShader.colorUniform = gl.getUniformLocation(this.constantShader, "color");

	this.lineShader = this.GetProgram("attribute vec4 aVertexPosition;\nattribute vec3 aOtherPos;\nuniform mat4 uViewMatrix;\nuniform mat4 uPVMatrix;\nuniform vec3 uZFacts; //zFixed,zFact, sizeOf_3px\nuniform vec3 camAt;\n\nvoid main(void)\n{\n\tvec3 delta = normalize(cross( camAt, (aOtherPos.xyz - aVertexPosition.xyz)));\n\tdelta *= aVertexPosition.w;\n\tdelta += camAt;\n\tvec4 transformed = uViewMatrix * vec4(aVertexPosition.xyz, 1.0);\n\tfloat zFact = (uZFacts.x - uZFacts.y * transformed.z) * uZFacts.z;\n\tdelta *= zFact;\n\t\n\tgl_Position = uPVMatrix * vec4(aVertexPosition.xyz + delta, 1.0);\n}\n\n\t\t",
		"#ifdef GL_ES\nprecision mediump float;\n#endif\nuniform vec3 color;\n\nvoid main(void)\n{\n\tgl_FragColor = vec4(color,1.0);\n}\n");
	this.lineShader.posAttribute = gl.getAttribLocation(this.lineShader, "aVertexPosition");
	this.lineShader.pos2Attribute = gl.getAttribLocation(this.lineShader, "aOtherPos");
	this.lineShader.viewMatrixUniform = gl.getUniformLocation(this.lineShader, "uViewMatrix");
	this.lineShader.pvMatrixUniform = gl.getUniformLocation(this.lineShader, "uPVMatrix");
	this.lineShader.zFactsUniform = gl.getUniformLocation(this.lineShader, "uZFacts");
	this.lineShader.camAtUniform = gl.getUniformLocation(this.lineShader, "camAt");
	this.lineShader.colorUniform = gl.getUniformLocation(this.lineShader, "color");
	this.simulateLineWidth = true;
/*
	this.pointShader = this.GetProgram("attribute vec3 aVertexPosition;\nuniform mat4 uPVMatrix;\nuniform float uPointSize;\n\nvoid main(void)\n{\n\tgl_PointSize = uPointSize;\n\tgl_Position = uPVMatrix * vec4(aVertexPosition.xyz, 1.0);\n}\n",
		"#ifdef GL_ES\nprecision mediump float;\n#endif\nuniform vec3 color;\n\nvoid main(void)\n{\n\tgl_FragColor = vec4(color,1.0);\n}\n");
	this.pointShader.posAttribute = gl.getAttribLocation(this.pointShader, "aVertexPosition");
	this.pointShader.pvMatrixUniform = gl.getUniformLocation(this.pointShader, "uPVMatrix");
	this.pointShader.pointSizeUniform = gl.getUniformLocation(this.pointShader, "uPointSize");
	this.pointShader.colorUniform = gl.getUniformLocation(this.pointShader, "color");
*/
	this.pointShaderPosCol = this.GetProgram("attribute vec3 aVertexPosition;\nattribute vec3 aColor;\nuniform mat4 uPVMatrix;\nuniform mat4 uViewMatrix;\nuniform float uPointSize;\nuniform vec3 uZFacts; //zFixed,zFact, sizeOf_3px\nuniform vec3 camAt;\n\nvarying vec4 color;\n\nvoid main(void)\n{\n\tgl_PointSize = uPointSize;\n\t\n\tvec4 transformed = uViewMatrix * vec4(aVertexPosition.xyz, 1.0);\n\tfloat zFact = (uZFacts.x - uZFacts.y * transformed.z) * uZFacts.z;\n\tvec3 delta = camAt * zFact;\n\t\n\tgl_Position = uPVMatrix * vec4(aVertexPosition.xyz + delta , 1.0);\n\tcolor = vec4(aColor,1.0);\n}\n",
		"#ifdef GL_ES\nprecision mediump float;\n#endif\nvarying vec4 color;\n\nvoid main(void)\n{\n\tgl_FragColor = color;\n}\n");
	this.pointShaderPosCol.posAttribute = gl.getAttribLocation(this.pointShaderPosCol, "aVertexPosition");
	this.pointShaderPosCol.colorAttribute = gl.getAttribLocation(this.pointShaderPosCol, "aColor");
	this.pointShaderPosCol.pvMatrixUniform = gl.getUniformLocation(this.pointShaderPosCol, "uPVMatrix");
	this.pointShaderPosCol.pointSizeUniform = gl.getUniformLocation(this.pointShaderPosCol, "uPointSize");

	this.pointShaderPosCol.viewMatrixUniform = gl.getUniformLocation(this.pointShaderPosCol, "uViewMatrix");
	this.pointShaderPosCol.zFactsUniform = gl.getUniformLocation(this.pointShaderPosCol, "uZFacts");
	this.pointShaderPosCol.camAtUniform = gl.getUniformLocation(this.pointShaderPosCol, "camAt");

	this.constantShaderPosCol = this.GetProgram("attribute vec3 aVertexPosition;\nattribute vec3 aVertexColor;\nuniform mat4 uPVMatrix;\nvarying vec4 color;\n\nvoid main(void)\n{\n\tgl_Position = uPVMatrix * vec4(aVertexPosition, 1.0);\n\tcolor = vec4(aVertexColor, 1.0);\n}\n",
		"#ifdef GL_ES\nprecision mediump float;\n#endif\nvarying vec4 color;\n\nvoid main(void)\n{\n\tgl_FragColor = color;\n}\n");
	this.constantShaderPosCol.posAttribute = gl.getAttribLocation(this.constantShaderPosCol, "aVertexPosition");
	this.constantShaderPosCol.colorAttribute = gl.getAttribLocation(this.constantShaderPosCol, "aVertexColor");
	this.constantShaderPosCol.pvMatrixUniform = gl.getUniformLocation(this.constantShaderPosCol, "uPVMatrix");

	/*
	this.shadedShader = this.GetProgram("attribute vec3 aVertexPosition;\nattribute vec3 aVertexNormal;\nuniform mat4 uPVMatrix;\nvarying float intensity;\n\nvoid main(void)\n{\n\tgl_Position = uPVMatrix * vec4(aVertexPosition, 1.0);\n\tintensity = dot(aVertexNormal, vec3(0.75,1,0.5) );\n\tif( intensity<0.0  ) intensity *= -0.75;\n}\n",
		"#ifdef GL_ES\nprecision mediump float;\n#endif\nuniform vec3 color;\nvarying float intensity;\n\nvoid main(void)\n{\n\tgl_FragColor.xyz = color*intensity;\n\tgl_FragColor.w = 1.0;\n}\n");
	this.shadedShader.posAttribute = gl.getAttribLocation(this.shadedShader, "aVertexPosition");
	this.shadedShader.norAttribute = gl.getAttribLocation(this.shadedShader, "aVertexNormal");
	this.shadedShader.pvMatrixUniform = gl.getUniformLocation(this.shadedShader, "uPVMatrix");
	this.shadedShader.colorUniform = gl.getUniformLocation(this.shadedShader, "color");
	                     */
	this.shadedShaderPosCol = this.GetProgram("attribute vec3 aVertexPosition;\nattribute vec3 aVertexNormal;\nattribute vec3 aVertexColor;\n\nuniform mat4 uPVMatrix;\nvarying vec3 color;\n\nvoid main(void)\n{\n\tgl_Position = uPVMatrix * vec4(aVertexPosition, 1.0);\n\tfloat intensity = dot(aVertexNormal, vec3(0.75,1,0.5) );\n\tif( intensity<0.0  ) intensity *= -0.75;\n\tcolor = aVertexColor * intensity;\n}\n",
		"#ifdef GL_ES\nprecision mediump float;\n#endif\nvarying vec3 color;\n\nvoid main(void)\n{\n\tgl_FragColor = vec4(color,1.0);\n}\n");
	this.shadedShaderPosCol.posAttribute = gl.getAttribLocation(this.shadedShaderPosCol, "aVertexPosition");
	this.shadedShaderPosCol.norAttribute = gl.getAttribLocation(this.shadedShaderPosCol, "aVertexNormal");
	this.shadedShaderPosCol.colorAttribute = gl.getAttribLocation(this.shadedShaderPosCol, "aVertexColor");
	this.shadedShaderPosCol.pvMatrixUniform = gl.getUniformLocation(this.shadedShaderPosCol, "uPVMatrix");

	this.constantShaderTxt = this.GetProgram("attribute vec3 aVertexPosition;\nattribute vec2 aVertexTxt;\nuniform mat4 uPVMatrix;\nvarying vec2 texCoord;\n\nvoid main(void)\n{\n\tgl_Position = uPVMatrix * vec4(aVertexPosition, 1.0);\n\ttexCoord = aVertexTxt; \n}\n",
		"#ifdef GL_ES\nprecision mediump float;\n#endif\nvarying vec2 texCoord;\nuniform vec4 color;\nuniform sampler2D uSampler;\n\nvoid main(void)\n{\n\tgl_FragColor = texture2D(uSampler, texCoord) * color;\n}\n");
	this.constantShaderTxt.posAttribute = gl.getAttribLocation(this.constantShaderTxt, "aVertexPosition");
	this.constantShaderTxt.txtAttribute = gl.getAttribLocation(this.constantShaderTxt, "aVertexTxt");
	this.constantShaderTxt.pvMatrixUniform = gl.getUniformLocation(this.constantShaderTxt, "uPVMatrix");
	this.constantShaderTxt.colorUniform = gl.getUniformLocation(this.constantShaderTxt, "color");
	this.constantShaderTxt.textureUniform = gl.getUniformLocation(this.constantShaderTxt, "uSampler");
};

//noinspection JSValidateJSDoc
/**
 * @param {String}VertexCode
 * @param {String} FragmentCode
 * @returns {WebGLProgram}
 */
WebGLRenderer.prototype.GetProgram = function(VertexCode, FragmentCode)
{
	var gl = this.gl;
	var vs = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vs, VertexCode);
	gl.compileShader(vs);
	var report = gl.getShaderInfoLog(vs);
	if(report.length!=0)
	{
		console.log("VertexShader Messages");
		console.log(report);
		//document.body.appendChild(document.createTextNode("VertexShader Messages:" +report));
		if(!gl.getShaderParameter(vs, gl.COMPILE_STATUS))
		{
			alert("VertexShader Error");
			return null
		}
	}
	var fs = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fs, FragmentCode);
	gl.compileShader(fs);
	var report = gl.getShaderInfoLog(fs);
	if(report.length!=0)
	{
		console.log("FragmentShader Messages");
		console.log(report);
		//document.body.appendChild(document.createTextNode("FragmentShader Messages:" +report));
		if(!gl.getShaderParameter(fs, gl.COMPILE_STATUS))
		{
			alert("FragmentShader Error");
			return null
		}
	}
	//noinspection JSValidateJSDoc
	/** @type {WebGLProgram} */
	var retProgram = gl.createProgram();
	gl.attachShader(retProgram, vs);
	gl.attachShader(retProgram, fs);
	gl.linkProgram(retProgram);

	var report = gl.getProgramInfoLog(retProgram);
	if(report.length!=0)
	{
		console.log("Linking Messages");
		console.log(report);
		//document.body.appendChild(document.createTextNode("Link Messages:" +report));
		if(!gl.getProgramParameter(retProgram, gl.LINK_STATUS))
		{
			alert("Link Error");
			return null
		}
	}
	return retProgram
};

WebGLRenderer.prototype.onResize = function()
{
	this.gl.viewport(0, 0, this.canvas.width, this.canvas.height)
};

WebGLRenderer.prototype.Begin = function()
{
	var gl = this.gl;
	//noinspection JSValidateTypes
	gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT)
};


WebGLRenderer.prototype.SetLineWidth = function(number)
{
	this.gl.lineWidth(number);
	this.currentLineWidth = number;
};

/*
WebGLRenderer.prototype.DrawPoint = function(a, dim, c)
{
	var gl = this.gl;
	var shader = this.pointShader;
	gl.useProgram(shader);
	gl.uniform3f(shader.colorUniform, c[0], c[1], c[2]);
	gl.uniformMatrix4fv(shader.pvMatrixUniform, false, this.camera.GetPV());
	gl.uniform1f(shader.pointSizeUniform, dim);

	//var arr = [ a[0], a[1], a[2] ];
	if(this.drawPointVBuffer==undefined)
	{
		this.drawPointVBuffer = gl.createBuffer();
		this.drawPointArray = new Float32Array(a);
		gl.bindBuffer(gl.ARRAY_BUFFER, this.drawPointVBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, this.drawPointArray, gl.DYNAMIC_DRAW)
	} else
	{
		gl.bindBuffer(gl.ARRAY_BUFFER, this.drawPointVBuffer);
		this.drawPointArray.set(a);
		gl.bufferSubData(gl.ARRAY_BUFFER, 0, this.drawPointArray)
	}
	gl.enableVertexAttribArray(shader.posAttribute);
	gl.vertexAttribPointer(shader.posAttribute, 3, gl.FLOAT, false, 0, 0);

	gl.drawArrays(gl.POINTS, 0, 1);
};
*/
WebGLRenderer.prototype.DrawPoints = function(buffer, dim)
{
	var gl = this.gl;
	var cam = this.camera;
	var shader = this.pointShaderPosCol;

	gl.useProgram(shader);
	gl.uniformMatrix4fv(shader.viewMatrixUniform, false, cam.view);
	gl.uniform3f(shader.zFactsUniform, cam.zFixed, cam.zFact, 6/this.canvas.height);
	gl.uniform3f(shader.camAtUniform, cam.at.x, cam.at.y, cam.at.z);

	gl.uniformMatrix4fv(shader.pvMatrixUniform, false, this.camera.GetPV());
	gl.uniform1f(shader.pointSizeUniform, dim);

	buffer.Setup(6,shader.posAttribute,3,shader.colorAttribute,3);
	gl.drawArrays(gl.POINTS, 0, buffer.length/6);

	gl.disableVertexAttribArray(shader.colorAttribute);
	gl.disableVertexAttribArray(shader.posAttribute);
};

/**
 * Render a line
 * @param {_Vector}a first point
 * @param {_Vector}b second point
 * @param {_Vector}c color
 */
WebGLRenderer.prototype.DrawLine = function(a, b, c)
{
	var gl = this.gl;
	var useLineShader = this.simulateLineWidth && this.currentLineWidth>1;
	var shader = this.constantShader;
	if(useLineShader)
		shader = this.lineShader;
	gl.useProgram(shader);
	gl.uniform3f(shader.colorUniform, c.x, c.y, c.z);

	gl.uniformMatrix4fv(shader.pvMatrixUniform, false, this.camera.GetPV());
	if(useLineShader)
	{
		var cam = this.camera;
		gl.uniformMatrix4fv(shader.viewMatrixUniform, false, cam.view);
		gl.uniform3f(shader.zFactsUniform, cam.zFixed, cam.zFact, 3/this.canvas.height);
		gl.uniform3f(shader.camAtUniform, cam.at.x, cam.at.y, cam.at.z);
		var array = [ a.x, a.y, a.z, 1, a.x, a.y, a.z, -1, b.x, b.y, b.z, -1, b.x, b.y, b.z, 1];
		var array2 = [ b.x, b.y, b.z, b.x, b.y, b.z, a.x, a.y, a.z, a.x, a.y, a.z ];
		if(this.drawLineVBufferSimulated==undefined)
		{
			this.drawLineVBufferSimulated = gl.createBuffer();
			this.drawLineArraySimulated = new Float32Array(array);
			gl.bindBuffer(gl.ARRAY_BUFFER, this.drawLineVBufferSimulated);
			gl.bufferData(gl.ARRAY_BUFFER, this.drawLineArraySimulated, gl.DYNAMIC_DRAW);
			this.drawLineVBufferSimulated2 = gl.createBuffer();
			this.drawLineArraySimulated2 = new Float32Array(array2);
			gl.bindBuffer(gl.ARRAY_BUFFER, this.drawLineVBufferSimulated2);
			gl.bufferData(gl.ARRAY_BUFFER, this.drawLineArraySimulated2, gl.DYNAMIC_DRAW);
		} else
		{
			gl.bindBuffer(gl.ARRAY_BUFFER, this.drawLineVBufferSimulated);
			//noinspection JSValidateTypes
			this.drawLineArraySimulated.set(array);
			gl.bufferSubData(gl.ARRAY_BUFFER, 0, this.drawLineArraySimulated);
			gl.bindBuffer(gl.ARRAY_BUFFER, this.drawLineVBufferSimulated2);
			//noinspection JSValidateTypes
			this.drawLineArraySimulated2.set(array2);
			gl.bufferSubData(gl.ARRAY_BUFFER, 0, this.drawLineArraySimulated2);
		}
		gl.bindBuffer(gl.ARRAY_BUFFER, this.drawLineVBufferSimulated);
		gl.enableVertexAttribArray(shader.posAttribute);
		gl.vertexAttribPointer(shader.posAttribute, 4, gl.FLOAT, false, 0, 0);
		gl.bindBuffer(gl.ARRAY_BUFFER, this.drawLineVBufferSimulated2);
		gl.enableVertexAttribArray(shader.pos2Attribute);
		gl.vertexAttribPointer(shader.pos2Attribute, 3, gl.FLOAT, false, 0, 0);
		gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);

		gl.disableVertexAttribArray(shader.pos2Attribute);
	} else
	{
		var arr = [ a.x, a.y, a.z, b.x, b.y, b.z];
		if(this.drawLineVBuffer==undefined)
		{
			this.drawLineVBuffer = gl.createBuffer();
			this.drawLineArray = new Float32Array(arr);
			gl.bindBuffer(gl.ARRAY_BUFFER, this.drawLineVBuffer);
			gl.bufferData(gl.ARRAY_BUFFER, this.drawLineArray, gl.DYNAMIC_DRAW)
		} else
		{
			gl.bindBuffer(gl.ARRAY_BUFFER, this.drawLineVBuffer);
			//noinspection JSValidateTypes
			this.drawLineArray.set(arr);
			gl.bufferSubData(gl.ARRAY_BUFFER, 0, this.drawLineArray)

		}
		gl.enableVertexAttribArray(shader.posAttribute);
		gl.vertexAttribPointer(shader.posAttribute, 3, gl.FLOAT, false, 0, 0);

		gl.drawArrays(gl.LINES, 0, 2);
	}


	gl.disableVertexAttribArray(shader.posAttribute);
};

/**
 *
 * @param {DynamicGLBuffer} buffer
 * @constructor
 */
WebGLRenderer.prototype.DrawLines = function(buffer)
{
	var gl = this.gl;
	var shader = this.constantShaderPosCol;
	gl.useProgram(shader);
	gl.uniformMatrix4fv(shader.pvMatrixUniform, false, this.camera.GetPV());

	buffer.Setup(6,shader.posAttribute,3,shader.colorAttribute,3);
	gl.drawArrays(gl.LINES, 0, buffer.length/6);

	gl.disableVertexAttribArray(shader.posAttribute);
	gl.disableVertexAttribArray(shader.colorAttribute);
};

WebGLRenderer.prototype.DrawTriangles = function(buffer)
{
	var gl = this.gl;
	var shader = this.shadedShaderPosCol;
	gl.useProgram(shader);
	gl.uniformMatrix4fv(shader.pvMatrixUniform, false, this.camera.GetPV());

	buffer.Setup(9,shader.posAttribute,3,shader.norAttribute,3,shader.colorAttribute,3);
	gl.drawArrays(gl.TRIANGLES, 0, buffer.length/9);

	gl.disableVertexAttribArray(shader.posAttribute);
	gl.disableVertexAttribArray(shader.norAttribute);
	gl.disableVertexAttribArray(shader.colorAttribute);
};
/*
WebGLRenderer.prototype.DrawTriangle = function(a, b, c, na, nb, nc, ca, cb, cc)
{
	var gl = this.gl;
	var usePosCol = cb!=undefined;
	var useNormal = na!=undefined;
	var shader;
	if(useNormal)
		shader = usePosCol ? this.shadedShaderPosCol : this.shadedShader; else
		shader = usePosCol ? this.constantShaderPosCol : this.constantShader;
	gl.useProgram(shader);
	if(!usePosCol)
		gl.uniform3f(shader.colorUniform, ca.x, ca.y, ca.z);
	gl.uniformMatrix4fv(shader.pvMatrixUniform, false, this.camera.GetPV());

	if(this.drawTriangleVBuffers==undefined)
	{
		this.drawTriangleArrays = [];
		for(var i = 0; i<3; i++)
			this.drawTriangleArrays[i] = new Float32Array(9);
	}
	this.drawTriangleArrays[0].set([ a.x, a.y, a.z, b.x, b.y, b.z, c.x, c.y, c.z]);
	if(useNormal) this.drawTriangleArrays[1].set([ na.x, na.y, na.z, nb.x, nb.y, nb.z, nc.x, nc.y, nc.z]);
	if(usePosCol) this.drawTriangleArrays[2].set([ ca.x, ca.y, ca.z, cb.x, cb.y, cb.z, cc.x, cc.y, cc.z]);
	if(this.drawTriangleVBuffers==undefined)
	{
		this.drawTriangleVBuffers = [gl.createBuffer(), gl.createBuffer(), gl.createBuffer()];
		for(var i = 0; i<3; i++)
		{
			gl.bindBuffer(gl.ARRAY_BUFFER, this.drawTriangleVBuffers[i]);
			gl.bufferData(gl.ARRAY_BUFFER, this.drawTriangleArrays[i], gl.DYNAMIC_DRAW)
		}
	} else
	{
		var useIt = [true, useNormal, usePosCol];
		for(var i = 0; i<3; i++)
			if(useIt[i])
			{
				gl.bindBuffer(gl.ARRAY_BUFFER, this.drawTriangleVBuffers[i]);
				gl.bufferSubData(gl.ARRAY_BUFFER, 0, this.drawTriangleArrays[i])
			}

	}
	gl.enableVertexAttribArray(shader.posAttribute);
	gl.bindBuffer(gl.ARRAY_BUFFER, this.drawTriangleVBuffers[0]);
	gl.vertexAttribPointer(shader.posAttribute, 3, gl.FLOAT, false, 0, 0);
	if(useNormal)
	{
		gl.enableVertexAttribArray(shader.norAttribute);
		gl.bindBuffer(gl.ARRAY_BUFFER, this.drawTriangleVBuffers[1]);
		gl.vertexAttribPointer(shader.norAttribute, 3, gl.FLOAT, false, 0, 0);
	}
	if(usePosCol)
	{
		gl.enableVertexAttribArray(shader.colorAttribute);
		gl.bindBuffer(gl.ARRAY_BUFFER, this.drawTriangleVBuffers[2]);
		gl.vertexAttribPointer(shader.colorAttribute, 3, gl.FLOAT, false, 0, 0);
	}
	gl.drawArrays(gl.TRIANGLES, 0, 3);

	gl.disableVertexAttribArray(shader.posAttribute);
	if(useNormal) gl.disableVertexAttribArray(shader.norAttribute);
	if(usePosCol) gl.disableVertexAttribArray(shader.colorAttribute);
};
                */
//http://www.khronos.org/webgl/wiki/WebGL_and_OpenGL_Differences#Non-Power_of_Two_Texture_Support
function isPowerOfTwo(v)
{
	return (v&(v-1))==0;
}

function nextHighestPowerOfTwo(v)
{
	--v;
	for(var i = 1; i<32; i <<= 1)
	{
		v = v|v >> i;
	}
	return v+1;
}

WebGLRenderer.prototype.CreateTexture = function(imageSrc)
{
	var gl = main.renderer.gl;
	var texture = gl.createTexture();
	gl.bindTexture(gl.TEXTURE_2D, texture);
	if(!isPowerOfTwo(imageSrc.width) || !isPowerOfTwo(imageSrc.height))
	{
		// Scale up the texture to the next highest power of two dimensions.
		var canvas = document.createElement("canvas");
		canvas.width = nextHighestPowerOfTwo(imageSrc.width);
		canvas.height = nextHighestPowerOfTwo(imageSrc.height);
		var ctx = canvas.getContext("2d");
		ctx.drawImage(imageSrc, 0, 0, imageSrc.width, imageSrc.height, 0, 0, canvas.width, canvas.height);
		imageSrc = canvas;
	}
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, imageSrc);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
	gl.generateMipmap(gl.TEXTURE_2D);

	gl.bindTexture(gl.TEXTURE_2D, null);
	return texture
};

WebGLRenderer.prototype.DrawTxtQuad = function(pos, uv, txt, col)
{
	var gl = this.gl;
	var shader = this.constantShaderTxt;
	gl.useProgram(shader);
	gl.uniform4f(shader.colorUniform, col.x, col.y, col.z, col.w);
	gl.uniformMatrix4fv(shader.pvMatrixUniform, false, this.camera.GetPV());
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, txt);
	gl.uniform1i(shader.textureUniform, 0);

	var arr = [
		pos[0][0], pos[0][1], pos[0][2], uv[0][0], uv[0][1], pos[1][0], pos[1][1], pos[1][2], uv[1][0], uv[1][1],
		pos[2][0], pos[2][1], pos[2][2], uv[2][0], uv[2][1], pos[3][0], pos[3][1], pos[3][2], uv[3][0], uv[3][1]];
	if(this.drawQuadVBuffers==undefined)
	{
		this.drawQuadVBuffers = gl.createBuffer();
		this.drawQuadArray = new Float32Array(arr);
		gl.bindBuffer(gl.ARRAY_BUFFER, this.drawQuadVBuffers);
		gl.bufferData(gl.ARRAY_BUFFER, this.drawQuadArray, gl.DYNAMIC_DRAW)
	} else
	{
		this.drawQuadArray.set(arr);
		gl.bindBuffer(gl.ARRAY_BUFFER, this.drawQuadVBuffers);
		gl.bufferSubData(gl.ARRAY_BUFFER, 0, this.drawQuadArray)
	}
	gl.bindBuffer(gl.ARRAY_BUFFER, this.drawQuadVBuffers);
	gl.enableVertexAttribArray(shader.posAttribute);
	gl.vertexAttribPointer(shader.posAttribute, 3, gl.FLOAT, false, 4*5, 0);
	gl.enableVertexAttribArray(shader.txtAttribute);
	gl.vertexAttribPointer(shader.txtAttribute, 2, gl.FLOAT, false, 4*5, 4*3);
	gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);

	gl.disableVertexAttribArray(shader.posAttribute);
	gl.disableVertexAttribArray(shader.txtAttribute);
};
