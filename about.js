function AboutForm()
{
	this.InitCommon(false);
	this.title = "About";
	// 291x377
	this.container.style.marginLeft = "-"+(291/2)+"px";
	this.container.style.marginTop = "-"+(377/2)+"px";

	var subDiv = document.createElement("div");
	subDiv.style.padding = "15px";
	subDiv.style.textAlign = "center";
	subDiv.innerHTML = "Maryka 3D<br>\nAn experiment by\n<a href=\"mailto:don_perricone@libero.it\">Antonino Perricone</a><br> <br>\n<a href=\"https://bitbucket.org/aperricone/little3deditor\" target=\"_blank\">\n\tSource code\n</a><br>\n<a href=\"https://bitbucket.org/aperricone/little3deditor/wiki/Home\" target=\"_blank\">\n\tWiki\n</a>\n<br>\n<a href=\"https://bitbucket.org/aperricone/little3deditor/issues\" target=\"_blank\">\n\tIssues report\n</a>\n\n<br>\n\n<a href=\"http://amicoperry.altervista.org\" target=\"_blank\">\n\tPersonal blog in italian language\n</a>\n<br><br>\nIf you really like this program, and<br> \nyou want that its develop continue, you can\n<form action=\"https://www.paypal.com/cgi-bin/webscr\" method=\"post\" target=\"_blank\">\n\t<input type=\"hidden\" name=\"cmd\" value=\"_s-xclick\">\n\t<input type=\"hidden\" name=\"hosted_button_id\" value=\"BLU3F2RMF6BHW\">\n\t<input type=\"image\" src=\"https://www.paypalobjects.com/en_GB/i/btn/btn_donate_LG.gif\" border=\"0\" name=\"submit\" alt=\"PayPal – The safer, easier way to pay online.\">\n\t<img alt=\"\" border=\"0\" src=\"https://www.paypalobjects.com/it_IT/i/scr/pixel.gif\" width=\"1\" height=\"1\">\n</form>\n\n<br>\n<a href=\"http://amicoperry.altervista.org/curriculum_en.html\" target=\"_blank\">PS. I am looking for a job now.</a>\n\n\n\t\n";
	this.container.appendChild(subDiv);

	var btnContainer = document.createElement('div');
	btnContainer.style.textAlign = 'center';
	btnContainer.style.padding = "15px";
	this.okBtn = document.createElement("input");
	this.okBtn.type = "button";
	this.okBtn.value = "OK";
	var tc = this;
	this.okBtn.onclick = function() { tc.Hide();};
	btnContainer.appendChild(this.okBtn);
	this.container.appendChild(btnContainer);
}

AboutForm.prototype = new CommonForm();
AboutForm.prototype.constructor = SimpleNumericForm;


function AddAbout()
{
	var h = main.menuBar.AddItem("Help").AddSubMenu();
	var item = h.AddItem("About");
	var form = new AboutForm();
	item.onClick = function() { form.Show(); };
}

AddAbout();