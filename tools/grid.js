function Grid()
{
	this.visible = true;

	var gridButton = main.toolBar.AddIcon("img/grid.svg", 2);
	gridButton.checked = true;

	var tc = this;
	gridButton.SetCallback(function()
	{
		tc.visible = !tc.visible;
		gridButton.checked = tc.visible;
		main.Draw();
	});
	main.AddDraw(function() { tc.Draw(); })
}

Grid.prototype.Draw = function()
{
	if(!this.visible) return;
	var rr = main.renderer;
	for(var i = -10; i<=10; i += 2)
	{
		var c = (i==0 ? 0 : 0.5);
		rr.DrawLine(V3(-10, 0, i), V3(10, 0, i), V3(c, c, c));
		rr.DrawLine(V3(i, 0, -10), V3(i, 0, 10), V3(c, c, c));
	}
};

grid = new Grid();
