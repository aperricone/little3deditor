function Axis()
{
	this.visible = true;

	var axisButton = main.toolBar.AddIcon("img/axis.svg", 2);
	axisButton.checked = true;

	var tc = this;
	axisButton.SetCallback(function()
	{
		tc.visible = !tc.visible;
		axisButton.checked = tc.visible;
		main.Draw();
	});
	main.AddDraw(function() { tc.Draw(); });
	function CreateDiv(axisName, color)
	{
		var ret = document.createElement('div');
		ret.style.position = 'absolute';
		ret.style.color = color;
		ret.innerText = axisName;
		document.body.appendChild(ret);
		return ret;
	}

	this.xDiv = CreateDiv('X', '#F00');
	this.yDiv = CreateDiv('Y', '#0F0');
	this.zDiv = CreateDiv('Z', '#00F');
}

Axis.prototype.SetupAxisLabel = function(point, destDiv)
{
	var rr = main.renderer;
	var center = rr.camera.To2D(V3());
	var xAxis = rr.camera.To2D(point);
	var xDirex = V3(xAxis).sub(center);
	// p = center + xDirex * t --> t = (p-center)/xDirex
	var lT = (-1-center.x)/xDirex.x;
	var rT = ( 1-center.x)/xDirex.x;
	var tT = ( 1-center.y)/xDirex.y;
	var bT = (-1-center.y)/xDirex.y;
	var minT = 100;
	if(lT>0 && lT<minT) minT = lT;
	if(rT>0 && rT<minT) minT = rT;
	if(tT>0 && tT<minT) minT = tT;
	if(bT>0 && bT<minT) minT = bT;
	var xPos = V3(xDirex).scale(minT).add(center);
	var p = main.InvNormalize2DPoint(xPos.x, xPos.y);
	if(xPos.x>0)
	{
		destDiv.style.left = 'auto';
		destDiv.style.right = window.innerWidth-p.x+'px';
	} else
	{
		destDiv.style.right = 'auto';
		destDiv.style.left = p.x+'px';
	}
	if(xPos.y<0)
	{
		destDiv.style.top = 'auto';
		destDiv.style.bottom = window.innerHeight-p.y+'px';
	} else
	{
		destDiv.style.bottom = 'auto';
		destDiv.style.top = p.y+'px';
	}
	destDiv.style.display = 'block';
};

Axis.prototype.Draw = function()
{
	if(!this.visible)
	{
		this.xDiv.style.display = 'none';
		this.yDiv.style.display = 'none';
		this.zDiv.style.display = 'none';
		return;
	}
	var rr = main.renderer;
	rr.DrawLine(V3(-100, 0, 0), V3(0, 0, 0), V3(0, 1, 1));
	rr.DrawLine(V3(0, -100, 0), V3(0, 0, 0), V3(1, 0, 1));
	rr.DrawLine(V3(0, 0, -100), V3(0, 0, 0), V3(1, 1, 0));

	rr.DrawLine(V3(0, 0, 0), V3(100, 0, 0), V3(1, 0, 0));
	rr.DrawLine(V3(0, 0, 0), V3(0, 100, 0), V3(0, 1, 0));
	rr.DrawLine(V3(0, 0, 0), V3(0, 0, 100), V3(0, 0, 1));

	this.SetupAxisLabel(V3(100, 0, 0), this.xDiv);
	this.SetupAxisLabel(V3(0, 100, 0), this.yDiv);
	this.SetupAxisLabel(V3(0, 0, 100), this.zDiv);
};

axis = new Axis();

