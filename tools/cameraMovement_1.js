/**
 *
 * @param camera
 * @param {function} onUpdate
 * @param {ToolBar}toolbar
 * @constructor
 */
function Camera_Movement(camera, onUpdate, toolbar)
{
	this.camera = camera;
	this.alpha = 15;
	this.beta = 15;
	this.center = V3();
	this.distance = 5;
	this.fov = 54;
	this.aspectRatio = 1;
	/** @type {boolean} */
	this.lock = false;
	/** @type {boolean} */
	this.justLock = false;
	/** @type {function} */
	this.onUpdate = onUpdate;
	// attach
	mouse.centerObj = this;
	mouse.wheelObj = this;

	this._ortho = false;
	var tc = this;
	//noinspection SpellCheckingInspection
	var orthographicBtn = toolbar.AddIcon("img/orthoMode.svg", 2);
	orthographicBtn.SetCallback(function()
	{
		tc.ortho = !tc.ortho;
		orthographicBtn.checked = tc.ortho;
	});

}

Camera_Movement.prototype.reset = function()
{
	this.alpha = 45;
	this.beta = 30;
	this.center = V3();
	this.distance = 5;
	this.fov = 54;
	this.UpdateCamera();
};

Camera_Movement.prototype.__defineGetter__('ortho', function() { return this._ortho; });
Camera_Movement.prototype.__defineSetter__('ortho', function(v)
{
	this._ortho = v;
	this.UpdateCamera();
});

Camera_Movement.prototype.OnMouseUp = function(e)
{
	if(this.justLock) return;
	if(this.lock)
	{
		if(e.which==3) // undo
		{
			this.alpha = this.startAlpha;
			this.beta = this.startBeta;
			this.center = V3(this.startCenter);
			this.UpdateCamera();
		}
		mouse.Unlock();
		this.lock = false;
	} else
	{
		this.lock = true;
		this.justLock = true;
		mouse.LockMouse(this);
		// undo purpose
		this.startAlpha = this.alpha;
		this.startBeta = this.beta;
		this.startCenter = V3(this.center);
		//
		this.exX = e.clientX;
		this.exY = e.clientY;

		main.statusBar.SetText('Test');
	}
};

Camera_Movement.prototype.OnMouseMove = function(e)
{
	if(!this.lock) return;
	this.justLock = false;
	var mx = e.clientX-this.exX;
	var my = e.clientY-this.exY;
	this.exX = e.clientX;
	this.exY = e.clientY;
	if(e.shiftKey)
	{
		this.center.add(V3(this.camera.rg).scale(-mx/(10*this.distance)).add(V3(this.camera.up).scale(my/(10*this.distance))));
	} else
	{
		this.alpha += mx;
		this.beta += my;
	}
	this.UpdateCamera();
};

Camera_Movement.prototype.OnMouseWheel = function(e)
{
	this.distance -= e.wheelDelta*0.01;
	if(this.distance<1) this.distance = 1;
	this.UpdateCamera();
};

Camera_Movement.prototype.AnglesFromAxis = function(axis)
{
	axis = V3(axis).unit();
	var beta = Math.asin(axis.y);
	axis.scale(1/Math.cos(beta));
	this.beta = Math.round(beta*180/Math.PI);
	var alpha = Math.atan2(axis.x, axis.z);
	this.alpha = Math.round(alpha*180/Math.PI);
	if(this.beta>89)
	{
		this.beta = 89;
		this.alpha = 180;
	}
};

Camera_Movement.prototype.UpdateCamera = function()
{
	if(this.beta>89) this.beta = 89;
	if(this.beta< -89) this.beta = -89;
	if(this.lastAlpha!=this.alpha || this.lastBeta!=this.beta || this.lastDistance!=this.distance || this.center.dirty)
	{
		var alpha = this.alpha*Math.PI/180.;
		var beta = this.beta*Math.PI/180.;
		var eye = V3(this.center);
		eye.x += this.distance*Math.cos(beta)*Math.sin(alpha);
		eye.y += this.distance*Math.sin(beta);
		eye.z += this.distance*Math.cos(beta)*Math.cos(alpha);
		this.camera.LookAt(eye, this.center);
		this.lastAlpha = this.alpha;
		this.lastBeta = this.beta;
		this.center.dirty = false;
	}
	if(this.lastFov!=this.fov || this.lastAspectRatio!=this.aspectRatio || this.lastDistance!=this.distance || this.lastOrtho!=this.ortho)
	{
		if(this.ortho)
		{
			var size = 2*this.distance*Math.tan(this.fov*Math.PI/360.0);
			this.camera.SetupOrthographic(size, this.aspectRatio, this.distance*0.1, this.distance*10);
		} else
		{
			this.camera.SetupPerspective(this.fov, this.aspectRatio, this.distance*0.1, this.distance*10);
		}
		this.lastFov = this.fov;
		this.lastAspectRatio = this.aspectRatio;
		this.lastOrtho = this.ortho;
	}

	this.lastDistance = this.distance;
	this.onUpdate();
};
