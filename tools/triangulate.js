function Triangulate(dest)
{
	if(dest==undefined) throw "Invalid parameter";
	if(!('length' in dest)) throw "Invalid parameter";
	if(!_Vector.isVector(dest[0])) throw "Invalid parameter";
	// Easier case
	if(dest.length==3) return [
		[0, 1, 2]
	];
	// new algorithm:
	// 1 - check for planarity - if planar save 2D values in an array, otherwise save 3D values
	// 1a - remove overlapping points
	// 1b - if planar check for intersection of edges, if 2 intersect calls 2 Triangulate and concatenate the results.
	// 2 - list all diagonals, if planar check for the diagonal is inside the polygon, otherwise are all ok
	// 3 - sort diagonals by length
	// 4 - create a list of indices, linear
	// 5 - find the shortest valid diagonal that remove 1 point from list
	// 6 - repeat step 5 until remains 3 indices

	/*1*/
	Triangulate.CheckForPlanarity(dest);
	/*1b*/
	var r = Triangulate.CheckForCandy();
	if(r!=undefined) return r;
	/*2-3*/
	Triangulate.FillDiagonals();
	/*4-5-6*/
	Triangulate.CreateTriangles();
	return Triangulate.triangles;
}

Triangulate.CheckForPlanarity = function(dest)
{
	Triangulate.points = [];
	if(_Vector.isVector(dest[0], 2))
	{
		Triangulate.isPlanar = true;
		Triangulate.points = dest.slice();
	} else if(_Vector.isVector(dest[0], 3))
	{
		var possiblePlane;
		var i = 0, j = 1, k;
		do {
			// it did the round trip
			if(i==0 && j==0) throw "impossible triangulate flat polygon";
			j = i+1;
			if(j==dest.length) j = 0;
			k = j+1;
			if(k==dest.length) k = 0;
			possiblePlane = new Plane(dest[i], dest[j], dest[k]);
			i = j;
		} while(possiblePlane.normal.dot(possiblePlane.normal)==0);

		Triangulate.isPlanar = true;
		for(i = 0; i<dest.length; i++)
		{
			if(Math.abs(possiblePlane.Check(dest[i]))>1e-6)
			{
				Triangulate.isPlanar = false;
				break;
			}
		}
		if(Triangulate.isPlanar)
		{
			// Drive to 2D
			var rg = Math.abs(possiblePlane.normal.x)<0.9 ? V3(1, 0, 0) : V3(0, 0, 1);
			var up = V3().cross(possiblePlane.normal, rg).unit();
			rg = V3().cross(possiblePlane.normal, up).unit();
			Triangulate.points.push(V2(0, 0));
			Triangulate.bbox = new BBox(Triangulate.points[0]);
			for(var i = 1; i<dest.length; i++)
			{
				var point = V3().sub(dest[i], dest[0]);
				var newPoint = V2(rg.dot(point), up.dot(point));
				Triangulate.points.push(newPoint);
				Triangulate.bbox.Grow(Triangulate.points[i]);
			}
		} else
		{
			// made a copy for step 1a
			Triangulate.points = dest.slice();
		}
	} else
		throw "unsupported type";
	for(var i = Triangulate.points.length-2; i>=0; i--)
	{ // 1a
		var delta = Triangulate.points[i].distanceSquare(Triangulate.points[i+1]);
		if(delta<1e-3)
		{
			Triangulate.points.slice(i);
		}
	}
};

/**
 * @return {Array|undefined}
 */
Triangulate.CheckForCandy = function()
{
	return undefined;
	//noinspection UnreachableCodeJS
	if(Triangulate.isPlanar)
	{
		for(var i = 0; i<Triangulate.points.length-1; i++)
			for(var j = i+2; j<Triangulate.points.length; j++)
			{
				var k = j+1;
				if(k==Triangulate.points.length) k = 0;
				if(Triangulate.Intersect(Triangulate.points[i], Triangulate.points[i+1], Triangulate.points[j],
					Triangulate.points[k]))
				{ //noinspection SpellCheckingInspection
					{/*
					 var o1 = [Triangulate.IntersectPoint],o1Translate = [-1];
					 var d=k;
					 while(true)
					 {
					 o1.push(Triangulate.points[d]);
					 o1Translate.push(d);
					 if( d == i) break;
					 d++; if( d==Triangulate.points.length ) d=0;
					 }
					 var o2 = [Triangulate.IntersectPoint],o2Translate = [-1];
					 d++;
					 while(true)
					 {
					 o2.push(Triangulate.points[d]);
					 o2Translate.push(d);
					 if( d == j ) break;
					 d++; if( d==Triangulate.points.length ) d=0;
					 }
					 var r1 = Triangulate(o1);
					 var r2 = Triangulate(o2);
					 for(var l=0; l<r1.length; l++) r1[l] = o1Translate[r1[l]];
					 for(var l=0; l<r2.length; l++) r2[l] = o2Translate[r2[l]];
					 return r1.concat(r2);    */
					}
				}
				//edges.push()

			}
	}
	return undefined;
};

Triangulate.FillDiagonals = function()
{
	Triangulate.diagonals = [];
	for(var i = 0; i<Triangulate.points.length; i++)
	{
		for(var j = i+2; j<Triangulate.points.length; j++)
		{
			var isDiagonal = true;
			var a = Triangulate.points[i];
			var b = Triangulate.points[j];
			if(i==0) isDiagonal = (j!=Triangulate.points.length-1);
			if(Triangulate.isPlanar)
			{
				if(isDiagonal)
					isDiagonal = Triangulate.IsInside(a, b);
			}
			if(isDiagonal)
			{
				var v = {};
				v.len = b.distanceSquare(a);
				v.edge = [i, j];
				Triangulate.diagonals.push(v);
			}
		}
	}
	if(Triangulate.isPlanar)
	{
		Triangulate.diagonals.sort(function(a, b) { return a.len-b.len; });
	} else
	{
		Triangulate.diagonals.sort(function(a, b) { return b.len-a.len; });
	}
};

Triangulate.AddTriangle = function(i, j, k)
{
	var tri = [i, j, k];
	Triangulate.triangles.push(tri);
};

Triangulate.CreateTriangles = function()
{
	Triangulate.triangles = [];
	// 4
	var indices = [];
	for(var i = 0; i<Triangulate.points.length; i++)
	{
		indices.push(i);
	}
	// 6
	while(indices.length>3 && Triangulate.diagonals.length!=0)
	{
		// 5
		for(var idx = 0; idx<Triangulate.diagonals.length; idx++)
		{
			var i = indices.indexOf(Triangulate.diagonals[idx].edge[0]);
			var k = indices.indexOf(Triangulate.diagonals[idx].edge[1]);
			if(i!= -1 && k!= -1 && (k-i)==2)
			{ // valid
				Triangulate.AddTriangle(indices[i], indices[i+1], indices[k]);
				indices.splice(i+1, 1);
				Triangulate.diagonals.splice(idx, 1);
				break;
			}
			if(i== -1 || k== -1)
			{
				Triangulate.diagonals.splice(idx, 1);
				idx--;
			}
		}
	}
	if(indices.length>=3) // sure?
		Triangulate.AddTriangle(indices[0], indices[1], indices[2]);
};

/**
 * @return {boolean}
 */
Triangulate.IsPointInLine = function(point, start, end)
{
	var pointDelta = V2().sub(point, start);
	var lineDirex = V2().sub(end, start);
	// point = start + lineDirex * t (If a is inside the line c-d)
	// t = (point-start)/lineDirex  (
	var t = V3();
	t.x = pointDelta.x/lineDirex.x;
	t.y = pointDelta.y/lineDirex.y;
	if(lineDirex.x==0 && pointDelta.x!=0) return false;
	if(lineDirex.y==0 && pointDelta.y!=0) return false;
	if(lineDirex.x!=0 && lineDirex.y!=0 && Math.abs(t.x-t.y)>1e-9) return false;
	if(lineDirex.x!=0) return ( t.x>1e-3 && t.x<1-1e-3 );
	if(lineDirex.y!=0) return ( t.y>1e-3 && t.y<1-1e-3 );

	return true;
};

/**
 * @return {boolean}
 */
Triangulate.Intersect = function(startA, endA, startB, endB)
{
	if(!_Vector.isVector(startA, 2)) throw "invalid parameter";
	if(!_Vector.isVector(endA, 2)) throw "invalid parameter";
	if(!_Vector.isVector(startB, 2)) throw "invalid parameter";
	if(!_Vector.isVector(endB, 2)) throw "invalid parameter";

	if(startA.distanceSquare(startB)<1e-3) return false;
	if(startA.distanceSquare(endB)<1e-3) return false;
	if(endA.distanceSquare(startB)<1e-3) return false;
	if(endA.distanceSquare(endB)<1e-3) return false;

	var direxA = V2().sub(endA, startA);
	var direxB = V2().sub(endB, startB);
	var deltaStarts = V2().sub(startB, startA);
	// startA + direxA * t = startB + direxB * k
	// direxA * t - direxB * k = startB-startA
	var m = direxA[0]*direxB[1]-direxA[1]*direxB[0];
	var mt = deltaStarts[0]*direxB[1]-deltaStarts[1]*direxB[0];
	var mk = -direxA[0]*deltaStarts[1]+direxA[1]*deltaStarts[0];
	if(Math.abs(m)<1e-3) // parallel
		return false;
	mt /= m;
	mk /= m;
	Triangulate.IntersectPoint = V2(startA).addScaled(direxA, mt);
	return (mt>0 && mt<1 && mk>0 && mk<1);
};

/**
 * @return {boolean}
 */
Triangulate.IsInside = function(start, end)
{
	// check for intersection
	for(var i = 0; i<Triangulate.length; i++)
	{
		var j = i+1;
		if(j==Triangulate.points.length) j = 0;
		if(Triangulate.Intersect(Triangulate.points[i], Triangulate.points[j], start, end))
			return false;
	}
	// check for middle point
	var middle = V2().add(start, end).scale(0.5);
	var outside = V2(middle);
	outside.x = Triangulate.bbox.min.x-1;
	var nInter = 0;
	for(var i = 0; i<Triangulate.points.length; i++)
	{
		var j = i+1;
		if(j==Triangulate.points.length) j = 0;
		var p1 = Triangulate.points[i];
		var p2 = Triangulate.points[j];
		if(Triangulate.Intersect(p1, p2, outside, middle))
			nInter++;
		if(Triangulate.IsPointInLine(p1, outside, middle))
			nInter++;
	}
	// odd intersections --> middle is inside
	return ((nInter&1)!=0);
};

