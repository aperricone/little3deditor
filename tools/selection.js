/**
 * Container for selection
 * @param {SelectorTool.ModeValue}[mode] mode for this selection, if undefined the currect selection is loaded
 * @constructor
 */
Selection = function(mode)
{
	this.items = [];
	if(mode==undefined)
	{
		this.mode = selectorTool.mode;
		var src = undefined;
		switch(this.mode)
		{
		case SelectorTool.ModeValue.VERTICES:
			src = 'vertices';
			break;
		case SelectorTool.ModeValue.EDGES:
			src = 'edges';
			break;
		case SelectorTool.ModeValue.FACES:
			src = 'faces';
			break;
		}
		for(var i = 0; i<main.geometries.length; i++)
		{
			/** @type {Geometry} */
			var geometry = main.geometries[i];
			if(!geometry.lock && geometry.visible)
			{
				/**
				 * @type {{geom: Geometry, indices: Array[Number]}}
				 */
				var thisItem = {geom: geometry, indices: []};
				if(src)
				{
					for(var j = 0; j<geometry[src].length; j++)
					{
						var obj = geometry[src][j];
						if(obj.selected)
							thisItem.indices.push(j);
					}
				} else
				{
					if(geometry.selected)
					{
						thisItem.indices.push(0);
					}
				}
				if(thisItem.indices.length!=0)
				{
					this.items.push(thisItem);
				}
			}
		}
	} else
	{
		this.mode = mode;
	}
};


Selection.prototype.AddObj = function(geom, idx)
{
	var itemIdx = -1;
	for(var i = 0; i<this.items.length; i++)
		if(this.items[i].geom==geom)
		{
			itemIdx = i;
			break;
		}
	if(itemIdx== -1)
	{
		itemIdx = this.items.length;
		this.items.push({geom: geom, indices: []});
	}
	if(this.items[itemIdx].indices.indexOf(idx)== -1)
		this.items[itemIdx].indices.push(idx);
};

Selection.prototype.GetBBox = function()
{
	var ret = new BBox(3);
	switch(this.mode)
	{
	case 0: //vertices
		for(var i = 0; i<this.items.length; i++)
		{
			var item = this.items[i];
			var geom = item.geom;
			for(var j = 0; j<item.indices.length; j++)
			{
				ret.Grow(geom.vertices[item.indices[j]]);
			}
		}
		break;
	case 1: //edge
		for(var i = 0; i<this.items.length; i++)
		{
			var item = this.items[i];
			var geom = item.geom;
			for(var j = 0; j<item.indices.length; j++)
			{
				var edge = geom.edges[item.indices[j]];
				ret.Grow(edge.GetPoint(0));
				ret.Grow(edge.GetPoint(1));
			}
		}
		break;
	case 2: //face
		for(var i = 0; i<this.items.length; i++)
		{
			var item = this.items[i];
			var geom = item.geom;
			for(var j = 0; j<item.indices.length; j++)
			{
				var face = geom.faces[item.indices[j]];
				ret.Grow(face.bbox);
			}
		}
		break;
	case 3: //face
		for(var i = 0; i<this.items.length; i++)
		{
			var item = this.items[i];
			var geom = item.geom;
			ret.Grow(geom.GetBBox());
		}
		break;
	}return ret;
};

Selection.prototype.ChangeMode = function(newMode)
{
	if(newMode==SelectorTool.ModeValue.VERTICES) this.ToPointMode(); else if(newMode==SelectorTool.ModeValue.EDGES) this.ToEdgeMode(); else if(newMode==SelectorTool.ModeValue.FACES) this.ToFaceMode(); else if(newMode==SelectorTool.ModeValue.GEOMETRIES) this.ToObjectMode(); else
	{
		throw "TODO: implement other mode changes"
	}
};

Selection.prototype.ToPointMode = function()
{
	if(this.mode==SelectorTool.ModeValue.VERTICES) return;
	switch(this.mode)
	{
	case SelectorTool.ModeValue.EDGES:
		for(var i = 0; i<this.items.length; i++)
		{
			var item = this.items[i];
			var geom = item.geom;
			var newIndices = [];
			for(var j = 0; j<item.indices.length; j++)
			{
				var edge = geom.edges[item.indices[j]];
				if(newIndices.indexOf(edge.indices[0])== -1) newIndices.push(edge.indices[0]);
				if(newIndices.indexOf(edge.indices[1])== -1) newIndices.push(edge.indices[1]);
			}
			item.indices = newIndices;
			item.indices.sort();
		}
		break;
	case SelectorTool.ModeValue.FACES:
		for(var i = 0; i<this.items.length; i++)
		{
			var item = this.items[i];
			var geom = item.geom;
			var newIndices = [];
			for(var j = 0; j<item.indices.length; j++)
			{
				var face = geom.faces[item.indices[j]];
				for(var k = 0; k<face.indices.length; k++)
				{
					var idx = face.indices[k];
					if(newIndices.indexOf(idx)== -1) newIndices.push(idx);
				}
			}
			item.indices = newIndices;
			item.indices.sort();
		}
		break;
	case SelectorTool.ModeValue.GEOMETRIES:
		for(var i = 0; i<this.items.length; i++)
		{
			var item = this.items[i];
			var geom = item.geom;
			item.indices = [];
			for(var j = 0; j<geom.vertices.length; j++)
			{
				item.indices.push(j);
			}
		}
		break;
	}
	this.mode = SelectorTool.ModeValue.VERTICES;
};


Selection.prototype.ToEdgeMode = function()
{
	if(this.mode==SelectorTool.ModeValue.EDGES) return;
	switch(this.mode)
	{
	case SelectorTool.ModeValue.VERTICES:
		for(var i = 0; i<this.items.length; i++)
		{
			var item = this.items[i];
			var geom = item.geom;
			var newIndices = [];
			for(var j = 0; j<geom.edges.length; j++)
			{
				var edge = geom.edges[j];
				if(item.indices.indexOf(edge.indices[0])!= -1 || item.indices.indexOf(edge.indices[1])!= -1)
				{
					newIndices.push(j);
				}
			}
			item.indices = newIndices;
			//item.indices.sort();
		}
		break;
	case SelectorTool.ModeValue.FACES:
		for(var i = 0; i<this.items.length; i++)
		{
			var item = this.items[i];
			var geom = item.geom;
			var newIndices = [];
			for(var j = 0; j<item.indices.length; j++)
			{
				var face = geom.faces[item.indices[j]];
				for(var k = 0; k<face.indices.length; k++)
				{
					var v0 = face.indices[k];
					var v1 = k+1;
					if(v1==face.indices.length) v1 = 0;
                    v1 =  face.indices[v1];
					var idx = geom.GetEdge(v0, v1);
					if(newIndices.indexOf(idx)== -1)
						newIndices.push(idx);
				}
			}
			item.indices = newIndices;
			item.indices.sort();
		}
		break;
	case SelectorTool.ModeValue.GEOMETRIES:
		for(var i = 0; i<this.items.length; i++)
		{
			var item = this.items[i];
			var geom = item.geom;
			item.indices = [];
			for(var j = 0; j<geom.edges.length; j++)
			{
				item.indices.push(j);
			}
		}
		break;
	}
	this.mode = SelectorTool.ModeValue.EDGES;
};

Selection.prototype.ToFaceMode = function()
{
	if(this.mode==SelectorTool.ModeValue.FACES) return;
	switch(this.mode)
	{
	case SelectorTool.ModeValue.VERTICES:
		for(var i = 0; i<this.items.length; i++)
		{
			var item = this.items[i];
			var geom = item.geom;
			var newIndices = [];
			for(var j = 0; j<item.indices.length; j++)
			{
				var vtx = geom.vertices[item.indices[j]];
				for(var k = 0; k<vtx.faces.length; k++)
				{
					var idx = vtx.faces[k];
					if(newIndices.indexOf(idx)== -1)
						newIndices.push(idx);
				}
			}
			item.indices = newIndices;
			item.indices.sort();
		}
		break;
	case SelectorTool.ModeValue.EDGES:
		for(var i = 0; i<this.items.length; i++)
		{
			var item = this.items[i];
			var geom = item.geom;
			var newIndices = [];
			for(var j = 0; j<item.indices.length; j++)
			{
				var edge = geom.edges[item.indices[j]];
				for(var k = 0; k<edge.faces.length; k++)
				{
					var idx = edge.faces[k];
					if(newIndices.indexOf(idx)== -1)
						newIndices.push(idx);
				}
			}
			item.indices = newIndices;
			item.indices.sort();
		}
		break;
	case SelectorTool.ModeValue.GEOMETRIES:
		for(var i = 0; i<this.items.length; i++)
		{
			var item = this.items[i];
			var geom = item.geom;
			item.indices = [];
			for(var j = 0; j<geom.faces.length; j++)
			{
				item.indices.push(j);
			}
		}
		break;
	}
	this.mode = SelectorTool.ModeValue.FACES;
};

Selection.prototype.ToObjectMode = function()
{
	if(this.mode==SelectorTool.ModeValue.GEOMETRIES) return;
	for(var i = 0; i<this.items.length; i++)
	{
		var item = this.items[i];
		item.indices = [0];
	}
	this.mode = SelectorTool.ModeValue.GEOMETRIES;
};

//region ChangeSelectionUndo

ChangeSelectionUndo = function()
{
	this.oldSel = new Selection();
};

ChangeSelectionUndo.prototype.Done = function()
{
	this.newSel = new Selection();
	undoRedo.AddUndo(this);
};

ChangeSelectionUndo.prototype.Undo = function()
{
	selectorTool.LoadSelection(this.oldSel);
};

ChangeSelectionUndo.prototype.Redo = function()
{
	selectorTool.LoadSelection(this.newSel);
};

// endregion
