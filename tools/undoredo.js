function UndoRedo()
{
	this.undoButton = main.toolBar.AddIcon("img/undo_disable.svg", 0);
	this.redoButton = main.toolBar.AddIcon("img/redo_disable.svg", 0);

	this.undoButton.enable = false;
	this.redoButton.enable = false;
	this.undoStack = [];
	this.redoStack = [];
	this.nModifications = 0;
	var tc = this;
	this.undoButton.SetCallback(function() { tc.Undo(); });
	this.redoButton.SetCallback(function() { tc.Redo(); });

	this.undoMenuItem = main.editMenu.AddItem("Undo");
	this.redoMenuItem = main.editMenu.AddItem("Redo");
	main.editMenu.AddSeparator();
	this.purgeMenuItem = main.editMenu.AddItem("Purge undo memory");
	this.undoMenuItem.enable = false;
	this.redoMenuItem.enable = false;
	this.purgeMenuItem.enable = false;
	this.undoMenuItem.onClick = function() { tc.Undo(); };
	this.redoMenuItem.onClick = function() { tc.Redo(); };
	this.purgeMenuItem.onClick = function() { tc.Purge(); };

	main.AddResetListener(function() { tc.Purge(); })
}

UndoRedo.prototype.Undo = function()
{
	this.nModifications--;
	var last = this.undoStack.pop();
	this.redoStack.push(last);
	last.Undo();
	this.CheckIcons();
};

UndoRedo.prototype.Redo = function()
{
	this.nModifications++;
	var last = this.redoStack.pop();
	this.undoStack.push(last);
	last.Redo();
	this.CheckIcons();
};

UndoRedo.prototype.Purge = function()
{
	this.undoStack = [];
	this.redoStack = [];
	this.CheckIcons();
};

UndoRedo.prototype.CheckIcons = function()
{
	if(this.undoStack.length==0)
	{
		this.undoButton.SetImage("img/undo_disable.svg");
		this.undoButton.enable = false;
		this.undoMenuItem.enable = false;
	} else
	{
		this.undoButton.SetImage("img/undo.svg");
		this.undoButton.enable = true;
		this.undoMenuItem.enable = true;
	}
	if(this.redoStack.length==0)
	{
		this.redoButton.SetImage("img/redo_disable.svg");
		this.redoButton.enable = false;
		this.redoMenuItem.enable = false;
	} else
	{
		this.redoButton.SetImage("img/redo.svg");
		this.redoButton.enable = true;
		this.redoMenuItem.enable = true;
	}
	this.purgeMenuItem.enable = this.undoStack.length!=0 || this.redoStack.length!=0;
};

/**
 *
 * @param obj
 */
UndoRedo.prototype.AddUndo = function(obj)
{
	this.nModifications++;
	if(!('Undo' in obj)) throw "Invalid parameter";
	if(!('Redo' in obj)) throw "Invalid parameter";
	if(typeof(obj.Undo)!="function") throw "Invalid parameter";
	if(typeof(obj.Redo)!="function") throw "Invalid parameter";
	this.redoStack = [];
	this.undoStack.push(obj);
	this.CheckIcons();
};

/**
 * Remove the last undo if it is just added
 */
UndoRedo.prototype.RemoveUndo = function()
{
	this.nModifications--;
	if(this.redoStack.length!=0) throw "Undo is not just added";
	this.undoStack.pop();
	this.CheckIcons();
};

undoRedo = new UndoRedo;

//noinspection FunctionWithInconsistentReturnsJS
window.addEventListener("beforeunload", function()
{
	if(undoRedo.nModifications==0) return;
	return "The file was modified";
});

