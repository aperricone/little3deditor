//region ImagePlaneForm
function ImagePlaneForm()
{
	var tc = this;
	this.InitCommon(false);
	this.title = "Images Plane";

	//370x200
	this.container.style.marginLeft = "-185px";
	this.container.style.marginTop = "-100px";

	this.lines = [];

	this.btnContainer = document.createElement('div');
	this.btnContainer.style.textAlign = 'center';
	this.okBtn = document.createElement("input");
	this.okBtn.type = "button";
	this.okBtn.value = "OK";
	this.okBtn.onclick = function()
	{
		if(tc.onOK) tc.onOK();
		tc.Hide();
	};
	this.btnContainer.appendChild(this.okBtn);
	this.container.appendChild(this.btnContainer);
}

ImagePlaneForm.prototype = new CommonForm();
ImagePlaneForm.prototype.constructor = SimpleNumericForm;

ImagePlaneForm.prototype.AddPossibility = function(name)
{
	var container = document.createElement('div');
	container.className = 'imagePlane';
	// with WebStorm "edit HTML fragment"
	container.innerHTML = "<div style=\"display: inline-block; width: 65px;\">\n\t<img src=\"img/visible.svg\" height=\"16\" width=\"16\">"+name+"</div>\n<div class=\"fileName\"></div>\n<img src=\"img/rotate-cw.svg\" height=\"16\" width=\"16\">\n<img src=\"img/rotate-ccw.svg\" height=\"16\" width=\"16\">\n<img src=\"img/flip-h.svg\" height=\"16\" width=\"16\">\n<img src=\"img/flip-v.svg\" height=\"16\" width=\"16\">\n<input type=\"number\" value=\"1\" step=\"0.1\"/>\n<input type=\"button\" value=\"width\"/>\n";

	this.container.insertBefore(container, this.btnContainer);
	return container;
};

ImagePlaneForm.prototype.checkOK = function()
{
	OpenFileForm.prototype.checkOK.call(this);
	if(!this.okBtn.disabled)
	{
		this.okBtn.disabled = (!this.fileElement.files[0].type.match(/image.*/));
	}
};


//endregion

//region ImagePlaneManager
function ImagePlaneManager()
{
	var tc = this;
	this.visible = true;

	this.form = new ImagePlaneForm();

	main.AddDraw(function() { tc.Draw(); });
	this.planes = [];
	this.planes.push(new ImagePlane(this.form.AddPossibility('Top'), V3(0, -1, 0)));
	this.planes.push(new ImagePlane(this.form.AddPossibility('Bottom'), V3(0, 1, 0)));
	this.planes.push(new ImagePlane(this.form.AddPossibility('Front'), V3(0, 0, -1)));
	this.planes.push(new ImagePlane(this.form.AddPossibility('Back'), V3(0, 0, 1)));
	this.planes.push(new ImagePlane(this.form.AddPossibility('Left'), V3(1, 0, 0)));
	this.planes.push(new ImagePlane(this.form.AddPossibility('Right'), V3(-1, 0, 0)));

	var item = main.toolMenu.AddItem("References images");
	item.onClick = function() { tc.ShowForm(); };
	//item.enable = false;
}

ImagePlaneManager.prototype.ShowForm = function()
{
	this.form.Show();
};

ImagePlaneManager.prototype.Draw = function()
{
	if(!this.visible) return;
	var bb = main.GetBBox();
	for(var i = 0; i<this.planes.length; i++)
	{
		var plane = this.planes[i];
		plane.Draw(bb, true);
	}
	/*if(main.renderer.camera.ortho)
	{
		for(var i = 0; i<this.planes.length; i++)
		{
			var plane = this.planes[i];
			plane.Draw(bb, false);
		}
	} */
};

//endregion

//region ImagePlane

function ImagePlane(container, direx)
{
	var tc = this;
	this.container = container;
	this.container.children[0].onmousedown = function() { tc.ToggleVisibility(); };
	this.container.children[1].onclick = function() { tc.SelectFile(); };
	this.container.children[2].onmousedown = function() { tc.RotateCW(); };
	this.container.children[3].onmousedown = function() { tc.RotateCCW(); };
	this.container.children[4].onmousedown = function() { tc.FlipH(); };
	this.container.children[5].onmousedown = function() { tc.FlipV(); };
	this.container.children[6].onchange = function() { tc.SetSize(); };
	this.container.children[7].onclick = function() { tc.ChangeSizeDest(); };
	this.rotated = false;
	if(Math.abs(direx.x)>0.9)
	{
		this.component = 0;
		this.d1 = V3(0, 0, direx.x);
		this.d2 = V3(0, 1, 0);
	} else if(Math.abs(direx.y)>0.9)
	{
		this.component = 1;
		this.d1 = V3(direx.y, 0, 0);
		this.d2 = V3(0, 0, 1);
	} else if(Math.abs(direx.z)>0.9)
	{
		this.component = 2;
		this.d1 = V3(-direx.z, 0, 0);
		this.d2 = V3(0, 1, 0);
	}
	this.width = 1;
	this.height = 1;
	this.currentSizeDest = 'width';
	this.firstMin = direx[this.component]<0;
	this.vtx = [V3(), V3(), V3(), V3()];
	this.SetVtx();
	this.uv = [ V2(1, 0), V2(0, 0), V2(1, 1), V2(0, 1) ];

	this.texture = null;
	this.visible = true;
}

ImagePlane.prototype.ToggleVisibility = function()
{
	this.visible = !this.visible;
	if(this.visible)
		this.container.children[0].children[0].src = 'img/visible.svg'; else
		this.container.children[0].children[0].src = 'img/hide.svg';
	main.Draw();
};

ImagePlane.prototype.SetImage = function(image)
{
	var rr = main.renderer;
	var img = new Image();
	img.src = image;
	this.width = img.width/200.;
	this.height = img.height/200.;
	this.aspectRatio = img.width/img.height;
	this.container.children[6].value = this[this.currentSizeDest];
	this.SetVtx();
	this.texture = rr.CreateTexture(img);
	main.Draw();
};

ImagePlane.prototype.onImageInput = function(files)
{
	if(files.length==0) return;
	if(!files[0].type.match(/image.*/)) return;
	var reader = new FileReader();
	var tc = this;
	reader.onload = function(e)
	{
		tc.SetImage(e.target.result);
	};
	reader.readAsDataURL(files[0]);
	this.container.children[1].innerText = files[0].name;
};

/** Show the open file dialog */
ImagePlane.prototype.SelectFile = function()
{
	var tc = this;
	var input = document.createElement('input');
	input.type = 'file';
	//input.value = this.container.children[1].innerText;
	input.onchange = function() { tc.onImageInput(input.files) };
	input.click();
};

ImagePlane.prototype.RotateCW = function()
{
	for(var i = 0; i<4; i++)
	{
		this.uv[i].Set(this.uv[i].y, -this.uv[i].x)
	}
	this.rotated = !this.rotated;
	this.SetVtx();
	main.Draw();
};

ImagePlane.prototype.RotateCCW = function()
{
	for(var i = 0; i<4; i++)
	{
		this.uv[i].Set(-this.uv[i].y, this.uv[i].x)
	}
	this.rotated = !this.rotated;
	this.SetVtx();
	main.Draw();
};
ImagePlane.prototype.FlipH = function()
{
	for(var i = 0; i<4; i++)
	{
		this.uv[i][0] = 1-this.uv[i][0];
	}
	main.Draw();
};

ImagePlane.prototype.FlipV = function()
{
	for(var i = 0; i<4; i++)
	{
		this.uv[i][1] = 1-this.uv[i][1];
	}
	main.Draw();
};

ImagePlane.prototype.SetSize = function()
{
	this[this.currentSizeDest] = parseFloat(this.container.children[6].value);
	if(this.currentSizeDest=='width')
	{
		this.height = this.width/this.aspectRatio;
	} else
	{
		this.width = this.height*this.aspectRatio;
	}
	this.SetVtx();
	main.Draw();
};

ImagePlane.prototype.ChangeSizeDest = function()
{
	if(this.currentSizeDest=='width')
	{
		this.currentSizeDest = 'height';
	} else
	{
		this.currentSizeDest = 'width';
	}

	this.container.children[6].value = this[this.currentSizeDest];
	this.container.children[7].value = this.currentSizeDest;

};

ImagePlane.prototype.SetVtx = function()
{
	for(var i = 0; i<4; i++)
	{
		this.vtx[i].Set();
	}
	var a = this.width , b = this.height;
	if(this.rotated)
	{
		b = this.width;
		a = this.height;
	}
	this.vtx[0].addScaled(this.d1, a);
	this.vtx[0].addScaled(this.d2, b);
	this.vtx[1].addScaled(this.d1, -a);
	this.vtx[1].addScaled(this.d2, b);
	this.vtx[2].addScaled(this.d1, a);
	this.vtx[2].addScaled(this.d2, -b);
	this.vtx[3].addScaled(this.d1, -a);
	this.vtx[3].addScaled(this.d2, -b);
};

ImagePlane.prototype.Draw = function(bb, opaque)
{
	if(this.texture==null) return;
	if(!this.visible) return;
	var rr = main.renderer;
	var e = 'min';
	if(opaque)
	{
		if(!this.firstMin) e = 'max';
		for(var i = 0; i<4; i++) this.vtx[i][this.component] = bb[e][this.component]*1.1;
		rr.DrawTxtQuad(this.vtx, this.uv, this.texture, V4(1, 1, 1, 1));
	} else
	{
		if(this.firstMin) e = 'max';
		for(var i = 0; i<4; i++)  this.vtx[i][this.component] = bb[e][this.component]*1.1;
		rr.DrawTxtQuad(this.vtx, this.uv, this.texture, V4(1, 1, 1, 0.5));
	}
};

//endregion

imagePlaneManager = new ImagePlaneManager();
