function GeometriesList()
{
	this.container = document.createElement("div");
	this.container.className = "geometriesList";
	var titleElement = document.createElement("div");
	titleElement.className = "title";
	titleElement.innerText = "Geometries";
	this.container.appendChild(titleElement);
	this.list = document.createElement("div");
	this.list.className = "list";
	this.container.appendChild(this.list);

	document.body.appendChild(this.container);

	var tc = this;
	var showButton = main.rightBar.AddText("Geometries");
	showButton.setAttribute('check', 'true');
	//noinspection SpellCheckingInspection
	showButton.onmousedown = function()
	{
		tc.visible = !tc.visible;
		if(tc.visible)
			this.setAttribute('check', 'true'); else
			this.setAttribute('check', 'false');
	};
	main.AddGeometriesListener(function(g, delta)
	{
		if(delta>0)
			tc.AddGeometry(g); else
			tc.RemoveGeometry(g);
	});
	main.AddResetListener(function() { tc.Clear(); });
	selectorTool.AddChangedListener(function()
	{
		tc.CheckSelection();
	});
	this.visible = true;
}

GeometriesList.prototype.__defineGetter__('visible', function()
{
	return this.container.style.display=='block';
});

GeometriesList.prototype.__defineSetter__('visible', function(v)
{
	if(v)
	{
		main.AddBorder(242);
		this.container.style.display = 'block';
	} else
	{
		main.AddBorder(-242);
		this.container.style.display = 'none';
	}
});

GeometriesList.prototype.RespondInput = [];

GeometriesList.prototype.RespondInput[0] = function(evt)
{ // select/unselect
	geomList.ToggleGeometrySelected(evt.target.geom);
};

//region ChangeNameUndo
GeometriesList.ChangeNameUndo = function(target, newName, oldName)
{
	this.target = target;
	this.newName = newName;
	this.oldName = oldName;
	undoRedo.AddUndo(this);
};

GeometriesList.ChangeNameUndo.prototype.Undo = function()
{
	this.target.geom.name = this.oldName;
	this.target.value = this.oldName;
};

GeometriesList.ChangeNameUndo.prototype.Redo = function()
{
	this.target.geom.name = this.newName;
	this.target.value = this.newName;
};
//endregion

GeometriesList.prototype.RespondInput[1] = function(evt)
{ // change name
	var geom = evt.target.geom;
	new GeometriesList.ChangeNameUndo(evt.target, evt.target.value, geom.name);
	geom.name = evt.target.value;
};

//region ChangeBoolUndo
GeometriesList.ChangeBoolUndo = function(target, booleanName, trueImage, falseImage, value)
{
	this.target = target;
	this.booleanName = booleanName;
	this.trueImage = trueImage;
	this.falseImage = falseImage;
	this.value = value;
	undoRedo.AddUndo(this);
};

GeometriesList.ChangeBoolUndo.prototype.Undo = function()
{
	GeometriesList.ToggleGeomBool(this.target, this.booleanName, this.trueImage, this.falseImage, !this.value)
};

GeometriesList.ChangeBoolUndo.prototype.Redo = function()
{
	GeometriesList.ToggleGeomBool(this.target, this.booleanName, this.trueImage, this.falseImage, this.value)
};
//endregion

GeometriesList.ToggleGeomBool = function(target, booleanName, trueImage, falseImage, value)
{
	var geom = target.geom;
	if(value!=undefined)
		geom[booleanName] = value; else
	{
		geom[booleanName] = !geom[booleanName];
		new GeometriesList.ChangeBoolUndo(target, booleanName, trueImage, falseImage, geom[booleanName]);
	}
	if(geom[booleanName])
	{
		target.src = trueImage;
	} else
	{
		target.src = falseImage;
	}
	main.Draw();

};
GeometriesList.prototype.RespondInput[2] = function(evt)
{ // show/hide
	GeometriesList.ToggleGeomBool(evt.target, 'visible', 'img/visible.svg', 'img/hide.svg');
};

GeometriesList.prototype.RespondInput[3] = function(evt)
{ // lock/unlock
	GeometriesList.ToggleGeomBool(evt.target, 'lock', 'img/lock.svg', 'img/unlock.svg');
};

GeometriesList.prototype.RespondInput[4] = function(evt)
{ // solid/wire
	GeometriesList.ToggleGeomBool(evt.target, 'wire', 'img/wireframe.svg', 'img/geom.svg');
};

GeometriesList.prototype.AddGeometry = function(geom)
{
	var allObj = [];
	allObj.push(document.createElement("img"));
	allObj.push(document.createElement("input"));
	allObj.push(document.createElement("img"));
	allObj.push(document.createElement("img"));
	allObj.push(document.createElement("img"));
	allObj.push(document.createElement("br"));

	allObj[0].src = 'img/geom.svg';
	allObj[0].height = '16';
	allObj[0].width = '16';

	allObj[1].value = geom.name;

	allObj[2].src = 'img/visible.svg';
	allObj[2].height = '16';
	allObj[2].width = '16';

	allObj[3].src = 'img/unlock.svg';
	allObj[3].height = '16';
	allObj[3].width = '16';

	allObj[4].src = 'img/geom.svg';
	allObj[4].height = '16';
	allObj[4].width = '16';

	var line = document.createElement("div");
	line.className = 'line';
	line.geom = geom;
	for(var i = 0; i<allObj.length; i++)
	{
		allObj[i].geom = geom;
		if(i!=1)
			allObj[i].onmousedown = this.RespondInput[i]; else
			allObj[i].onchange = this.RespondInput[i];
		line.appendChild(allObj[i]);
	}
	this.list.appendChild(line);
	return line;
};

GeometriesList.prototype.RemoveGeometry = function(geom)
{
	for(var i = 0; i<this.list.children.length; i++)
	{
		var ele = this.list.children[i];
		if(ele.geom==geom)
		{
			this.list.removeChild(ele);
		}
	}
};

GeometriesList.prototype.Clear = function()
{
	while(this.list.firstChild)
	{
		this.list.removeChild(this.list.firstChild);
	}
};

GeometriesList.prototype.CheckSelection = function()
{
	for(var i = 0; i<this.list.children.length; i++)
	{
		var ele = this.list.children[i];
		if(this.IsGeometrySelected(ele.geom))
		{
			ele.children[0].src = "img/selected.svg";
		} else
		{
			ele.children[0].src = 'img/geom.svg';
		}
	}
};

GeometriesList.prototype.IsGeometrySelected = function(geom)
{
	if(selectorTool.nSelected==0) return false;
	var ele = '';
	switch(selectorTool._mode)
	{
	case SelectorTool.ModeValue.VERTICES:
		ele = 'vertices';
		break;
	case SelectorTool.ModeValue.EDGES:
		ele = 'edges';
		break;
	case SelectorTool.ModeValue.FACES:
		ele = 'faces';
		break;
	case SelectorTool.ModeValue.GEOMETRIES:
		return geom.selected;
	}
	var subElement = geom[ele];
	for(var i = 0; i<subElement.length; i++)
	{
		if(subElement[i].selected) return true;
	}
	return false;
};

GeometriesList.prototype.ToggleGeometrySelected = function(geom)
{
	selectorTool.callCB = false;
	var undo = new ChangeSelectionUndo();
	var newValue = !this.IsGeometrySelected(geom);
	if(selectorTool._mode==SelectorTool.ModeValue.ALL_SUB)
		selectorTool.mode = SelectorTool.ModeValue.FACES;

	var ele = '';
	switch(selectorTool._mode)
	{
	case SelectorTool.ModeValue.VERTICES:
		ele = 'vertices';
		break;
	case SelectorTool.ModeValue.EDGES:
		ele = 'edges';
		break;
	case SelectorTool.ModeValue.FACES:
		ele = 'faces';
		break;
	case SelectorTool.ModeValue.GEOMETRIES:
		selectorTool.SetSelection(geom, newValue);
		break;
	}
	if(ele.length!=0)
	{
		var subElement = geom[ele];
		for(var i = 0; i<subElement.length; i++)
		{
			selectorTool.SetSelection(subElement[i], newValue);
		}
	}
	selectorTool.callCB = true;
	undo.Done();
	selectorTool.callBack();
};

//noinspection JSUnusedGlobalSymbols
var geomList = new GeometriesList();
