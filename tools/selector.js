function SelectorTool()
{
	this.pointBtn = main.toolBar.AddIcon("img/pointMode.svg", 1);
	this.edgeBtn = main.toolBar.AddIcon("img/edgeMode.svg", 1);
	this.faceBtn = main.toolBar.AddIcon("img/faceMode.svg", 1);
	this.objectBtn = main.toolBar.AddIcon("img/objectMode.svg", 1);
	this._mode = SelectorTool.ModeValue.ALL_SUB;
	this.nSelected = 0;
	this._normal = V3();
	this.changeds = [];

	var tc = this;
	this.pointBtn.checked = true;
	this.edgeBtn.checked = true;
	this.faceBtn.checked = true;
	this.pointBtn.SetCallback(function() { tc.SetModeBtn(SelectorTool.ModeValue.VERTICES); });
	this.edgeBtn.SetCallback(function() { tc.SetModeBtn(SelectorTool.ModeValue.EDGES); });
	this.faceBtn.SetCallback(function() { tc.SetModeBtn(SelectorTool.ModeValue.FACES); });
	this.objectBtn.SetCallback(function() { tc.SetModeBtn(SelectorTool.ModeValue.GEOMETRIES); });
	/*
	 //noinspection SpellCheckingInspection
	 main.toolBar.centerContext.onmousedown = function()
	 {
	 if( tc.nSelected != 0)
	 {
	 new SelectorTool.ChangeModeUndo(-1);
	 tc.Clear();
	 }
	 };*/

	mouse.moveObj = this;
	mouse.leftObj = this;

	this.rectDiv = document.createElement('div');
	this.rectDiv.style.borderStyle = 'dashed';
	this.rectDiv.style.position = 'absolute';
	this.rectDiv.style.display = 'none';
	document.body.appendChild(this.rectDiv);
	mouse.attach(this.rectDiv, false);
	this.rectSelection = false;
	this.rectStart = V2();

	this.onSelChanged = [];
	this.callCB = true;
}

/**
 * Enum for SelectorTool mode
 * @readonly
 * @enum {number}
 */
SelectorTool.ModeValue = {
	VERTICES:   0,
	EDGES:      1,
	FACES:      2,
	GEOMETRIES: 3,
	//noinspection SpellCheckingInspection
	ALL_SUB:    4,
	//noinspection SpellCheckingInspection
	UNDEF:      -1
};

SelectorTool.prototype.__defineGetter__("normal", function()
{
	if(this.mode==SelectorTool.GEOMETRIES) return V3();
	return V3(this._normal).unit();
});

SelectorTool.prototype.AddChangedListener = function(fn)
{
	this.onSelChanged.push(fn);
};

SelectorTool.prototype.callBack = function()
{
	if(!this.callCB) return;
	for(var i = 0; i<this.onSelChanged.length; i++)
		this.onSelChanged[i]();

	var text = "";
	if(this.nSelected!=0)
	{
		text = this.nSelected+" ";
		if(this.nSelected==1)
		{
			switch(this.mode)
			{
			case SelectorTool.ModeValue.VERTICES:
				text += "vertex";
				break;
			case SelectorTool.ModeValue.EDGES:
				text += "edge";
				break;
			case SelectorTool.ModeValue.FACES:
				text += "face";
				break;
			case SelectorTool.ModeValue.GEOMETRIES:
				text += "geometry";
				break;
			}
		} else
		{
			switch(this.mode)
			{
			case SelectorTool.ModeValue.VERTICES:
				text += "vertices";
				break;
			case SelectorTool.ModeValue.EDGES:
				text += "edges";
				break;
			case SelectorTool.ModeValue.FACES:
				text += "faces";
				break;
			case SelectorTool.ModeValue.GEOMETRIES :
				text += "geometries";
				break;
			}
		}
		text += " selected."
	}
	mouse.SetInfo(0, text, this);
	main.Draw();
};

SelectorTool.prototype.SetModeBtn = function(newMode)
{
	if(this.mode==newMode)
	{
		if(this.nSelected==0)
			newMode = SelectorTool.ModeValue.ALL_SUB; else
			this.Clear();
	}
	if(this.mode!=newMode)
	{
		var undo = new ChangeSelectionUndo();
		this.mode = newMode;
		undo.Done();
	}
};

SelectorTool.prototype.__defineSetter__("mode", function(newMode)
{
	if(this.marked)
		this.marked.marked = false;
	this.marked = undefined;

	this.pointBtn.checked = ( newMode==SelectorTool.ModeValue.VERTICES || newMode==SelectorTool.ModeValue.ALL_SUB);
	this.edgeBtn.checked = ( newMode==SelectorTool.ModeValue.EDGES || newMode==SelectorTool.ModeValue.ALL_SUB );
	this.faceBtn.checked = ( newMode==SelectorTool.ModeValue.FACES || newMode==SelectorTool.ModeValue.ALL_SUB);
	this.objectBtn.checked = ( newMode==SelectorTool.ModeValue.GEOMETRIES );

	if(this.nSelected!=0)
	{
		this.callCB = false;
		var sel = new Selection();
		sel.ChangeMode(newMode);
		this.LoadSelection(sel);
		this.callCB = true;
	}
	this._mode = newMode;
	this.callBack();
});

SelectorTool.prototype.__defineGetter__("mode", function()
{
	return this._mode;
});

/**
 * It Sets selected state of obj, if the new selected state is undefined, the method toggles it
 * @param {Geometry|Geometry.Vertex|Geometry.Edge|Geometry.Face}obj
 * @param {Boolean}[select=!old_value] New selected state, if undefined a toggle is performed
 * @constructor
 */
SelectorTool.prototype.SetSelection = function(obj, select)
{
	if(select==undefined) select = !obj.selected;
	if(select==obj.selected) return;

	if(this.nSelected==0 && this.mode==SelectorTool.ModeValue.ALL_SUB && select)
	{
		this.callCB = false;
		if(obj.constructor==Geometry.Vertex)
		{
			this.mode = SelectorTool.ModeValue.VERTICES;
		} else if(obj.constructor==Geometry.Edge)
		{
			this.mode = SelectorTool.ModeValue.EDGES;
		} else if(obj.constructor==Geometry.Face)
		{
			this.mode = SelectorTool.ModeValue.FACES;
		}
		this.callCB = true;
	}

	obj.selected = select;
	if(obj.selected)
	{
		this.nSelected++;
		if(this._mode!=SelectorTool.ModeValue.GEOMETRIES)
			this._normal.add(obj.normal);
	} else
	{
		this.nSelected--;
		if(this._mode!=SelectorTool.ModeValue.GEOMETRIES)
			this._normal.sub(obj.normal);
	}
	this.callBack();
};

// region Mouse support

SelectorTool.prototype.ShowRect = function(x, y)
{
	this.rectDiv.style.left = (x<this.rectStart.x ? x : this.rectStart.x)+'px';
	this.rectDiv.style.top = (y<this.rectStart.y ? y : this.rectStart.y)+'px';
	this.rectDiv.style.right = (window.innerWidth-(x>this.rectStart.x ? x : this.rectStart.x))+'px';
	this.rectDiv.style.bottom = (window.innerHeight-(y>this.rectStart.y ? y : this.rectStart.y))+'px';
	this.rectDiv.style.display = 'block';
};

/**
 *
 * @param x0
 * @param y0
 * @param x1
 * @param y1
 * @param select
 */
SelectorTool.prototype.GetUnderRect = function(x0, y0, x1, y1, select)
{
	if(x1<x0)
	{
		var t = x0;
		x0 = x1;
		x1 = t;
	}
	if(y1<y0)
	{
		var t = y0;
		y0 = y1;
		y1 = t;
	}
	if(x1-x0<3) return;
	if(y1-y0<3) return;

	var cam = main.renderer.camera;
	var points= [];
	var p2D = main.Normalize2DPoint(x0, y0);
	points[0] = cam.To3D(V3(p2D.x, p2D.y, 0.5));
	points[4] = cam.To3D(V3(p2D.x, p2D.y, 0));
	var p2D = main.Normalize2DPoint(x1, y0);
	points[1] = cam.To3D(V3(p2D.x, p2D.y, 0.5));
	points[5] = cam.To3D(V3(p2D.x, p2D.y, 0));
	p2D = main.Normalize2DPoint(x1, y1);
	points[2] = cam.To3D(V3(p2D.x, p2D.y, 0.5));
	points[6] = cam.To3D(V3(p2D.x, p2D.y, 0));
	p2D = main.Normalize2DPoint(x0, y1);
	points[3] = cam.To3D(V3(p2D.x, p2D.y, 0.5));
	points[7] = cam.To3D(V3(p2D.x, p2D.y, 0));

	var cam = main.renderer.camera;
	var planes = [];
	planes.push(new Plane(points[0], points[1], points[4]));
	planes.push(new Plane(points[1], points[2], points[5]));
	planes.push(new Plane(points[2], points[3], points[6]));
	planes.push(new Plane(points[3], points[0], points[7]));
	var src = undefined;
	switch(this.mode)
	{
	case SelectorTool.ModeValue.VERTICES:
		src = 'vertices';
		break;
	case SelectorTool.ModeValue.EDGES:
		src = 'edges';
		break;
	case SelectorTool.ModeValue.FACES:
		src = 'faces';
		break;
	}
	var noWire = this.mode==SelectorTool.ModeValue.FACES;
	for(var i = 0; i<main.geometries.length; i++)
	{
		var geom = main.geometries[i];
		var check = ( !geom.lock && geom.visible);
		if(noWire) check = check && !geom.wire;
		if(check)
		{
			if(src)
			{
				var subElement = geom[src];
				for(var j = 0; j<subElement.length; j++)
				{
					/** @type {Geometry.Vertex} */
					var vtx = subElement[j];
					if(select)
					{
						this.SetSelection(vtx, vtx.IsInsideConvexHull(planes));
						vtx.marked = false;
					} else
						vtx.marked = vtx.IsInsideConvexHull(planes);
				}
			} else
			{
				if(select)
				{
					this.SetSelection(geom, geom.IsInsideConvexHull(planes));
					geom.marked = false;
				} else
					geom.marked = geom.IsInsideConvexHull(planes);
			}
		}
	}
};

/**
 * @param {_Vector}start
 * @param {_Vector}direx
 * @param {Number}sizeOf_3px
 * @param {String}element
 * @param {Boolean}[noWire=false]
 * @returns {{obj: (Geometry.Vertex|Geometry.Edge|Geometry.Face), dist: Number}}
 */
SelectorTool.prototype.GetNearest = function(start, direx, sizeOf_3px, element, noWire)
{
	/** @type {Geometry.Vertex|Geometry.Edge|Geometry.Face} */
	var nearest;
	var nearestDist = Infinity;
	var cam = main.renderer.camera;
	for(var i = 0; i<main.geometries.length; i++)
	{
		var geom = main.geometries[i];
		var check = ( !geom.lock && geom.visible);
		if(noWire) check = check && !geom.wire;
		if(check)
		{
			var subElement = geom[element];
			for(var j = 0; j<subElement.length; j++)
			{
				/** @type {Geometry.Vertex} */
				var vtx = subElement[j];
				var r = vtx.RayDistance(start, direx);
				if(r.time<nearestDist && r.distance<sizeOf_3px*(cam.zFixed+cam.zFact*r.time))
				{
					nearest = vtx;
					nearestDist = r.time;
				}
			}
		}
	}
	return {obj: nearest, dist: nearestDist };
};

/**
 * @return {Geometry|Geometry.Vertex|Geometry.Edge|Geometry.Face|undefined}
 */
SelectorTool.prototype.GetUnderCursor = function(x, y)
{
	/** @type {Camera} */
	var cam = main.renderer.camera;
	var p2D = main.Normalize2DPoint(x, y);
	var bPoint = cam.To3D(V3(p2D.x, p2D.y, 0.5));
	var direction;
	var aPoint;
	if(cam.ortho)
	{
		//var distance = V3(bPoint).sub(cam.pos).dot(cam.at);
		direction = V3(cam.at).scale(-1);
		aPoint = V3(bPoint).addScaled(direction, -1000)
	} else
	{
		aPoint = cam.pos;
		direction = V3(bPoint).sub(aPoint).unit();
	}
	var sizeOf_3px = 3/main.canvas.height;
	var nearestPoint = this.GetNearest(aPoint, direction, sizeOf_3px, 'vertices');
	var nearestEdge = this.GetNearest(aPoint, direction, sizeOf_3px, 'edges');
	var nearestFace = this.GetNearest(aPoint, direction, sizeOf_3px, 'faces', true);

	var dist = Infinity;
	if(nearestFace.obj && nearestFace.dist<dist) dist = nearestFace.dist;
	if(nearestEdge.obj && nearestEdge.dist<dist) dist = nearestEdge.dist;
	if(nearestPoint.obj && nearestPoint.dist<dist) dist = nearestPoint.dist;
	dist *= 1.001;
	if(nearestFace.dist>=dist) nearestFace.obj = undefined;
	if(nearestEdge.dist>=dist) nearestEdge.obj = undefined;
	if(nearestPoint.dist>=dist) nearestPoint.obj = undefined;

	switch(this.mode)
	{
	case SelectorTool.ModeValue.VERTICES:
		return nearestPoint.obj;
	case SelectorTool.ModeValue.EDGES:
		return nearestEdge.obj;
	case SelectorTool.ModeValue.FACES:
		return nearestFace.obj;
	case SelectorTool.ModeValue.GEOMETRIES:
		return nearestFace.obj ? nearestFace.obj.parent : undefined;
	case SelectorTool.ModeValue.ALL_SUB:
	{
		/*Debug text
		 var text = "Face: "
		 if( nearestFace.obj ) text += nearestFace.dist; else text+= "--";
		 text += " ; Edge: "
		 if( nearestEdge.obj ) text += nearestEdge.dist; else text+= "--";
		 text += " ; Point: "
		 if( nearestPoint.obj ) text += nearestPoint.dist; else text+= "--";
		 mouse.SetInfo(1,text,this);
		 */

		if(nearestPoint.obj) return nearestPoint.obj; else if(nearestEdge.obj) return nearestEdge.obj; else if(nearestFace.obj) return nearestFace.obj; else
			break;
	}
	}
	return undefined;
};

SelectorTool.prototype.OnMouseMove = function(evt)
{
	if(this.rectSelection)
	{
		this.GetUnderRect(this.rectStart.x, this.rectStart.y, evt.clientX, evt.clientY, false);
		this.ShowRect(evt.clientX, evt.clientY);
		main.Draw();
	} else
	{
		var oldMarked = this.marked;
		if(this.marked) this.marked.marked = false;
		this.marked = undefined;
		var obj = this.GetUnderCursor(evt.clientX, evt.clientY);
		if(obj)
		{
			obj.marked = true;
			this.marked = obj;
		}
		if(mouse.buttonsDown[0] && obj)
		{
			if(obj.selected!=this.select)
			{
				this.changeds.push(this.marked);
				this.SetSelection(this.marked, this.select);
			}
		}
		if(oldMarked!=this.marked)
		{
			main.Draw();
		}
	}
};

SelectorTool.prototype.OnMouseDown = function(evt)
{
	this.currentUndo = new ChangeSelectionUndo();
	if(this.marked)
	{
		this.changeds = [this.marked];
		this.select = !this.marked.selected;
		this.SetSelection(this.marked, this.select);
	} else
	{
		if(this.mode==SelectorTool.ModeValue.ALL_SUB)
			this.mode = SelectorTool.ModeValue.VERTICES;
		// start rect select
		this.rectSelection = true;
		this.rectStart.Set(evt.clientX, evt.clientY);
		this.ShowRect(evt.clientX, evt.clientY);
	}
};

SelectorTool.prototype.OnMouseUp = function(evt)
{
	if(this.rectSelection)
	{
		this.GetUnderRect(this.rectStart.x, this.rectStart.y, evt.clientX, evt.clientY, true);
		main.Draw();
		this.rectSelection = false;
		this.rectDiv.style.display = 'none';
		//this.currentUndo.Done();
		this.currentUndo = undefined;
	} else
	{
		if(this.changeds.length!=0)
		{
			this.currentUndo.Done();
		}
		this.changeds = [];
		this.currentUndo = undefined;
	}
};

SelectorTool.prototype.Clear = function()
{
	for(var i = 0; i<main.geometries.length; i++)
	{
		/** @type {Geometry} */
		var geometry = main.geometries[i];
		for(var j = 0; j<geometry.vertices.length; j++)
		{
			geometry.vertices[j].selected = false;
		}
		for(var j = 0; j<geometry.edges.length; j++)
		{
			geometry.edges[j].selected = false;
		}
		for(var j = 0; j<geometry.faces.length; j++)
		{
			geometry.faces[j].selected = false;
		}
		geometry.selected = false;
	}
	this.nSelected = 0;
	this.normal = V3();

	this.callBack();
};

/**
 * @param {Selection} obj
 * @constructor
 */
SelectorTool.prototype.LoadSelection = function(obj)
{
	this.callCB = false;
	this.Clear();
	if(obj)
	{
		this.mode = obj.mode;
		for(var i = 0; i<obj.items.length; i++)
		{
			var item = obj.items[i];
			var geometry = item.geom;
			switch(obj.mode)
			{
			case 0: //point mode
				for(var j = 0; j<item.indices.length; j++)
				{
					var vtx = geometry.vertices[item.indices[j]];
					vtx.selected = true;
					this.nSelected++;
					this._normal.add(vtx.normal);
				}
				break;
			case 1: // edge mode
				for(var j = 0; j<item.indices.length; j++)
				{
					var edge = geometry.edges[item.indices[j]];
					edge.selected = true;
					this.nSelected++;
					this._normal.add(edge.normal);
				}
				break;
			case 2: // face mode
				for(var j = 0; j<item.indices.length; j++)
				{
					var face = geometry.faces[item.indices[j]];
					face.selected = true;
					this.nSelected++;
					this._normal.add(face.normal);
				}
				break;
			case 3: //object mode
				geometry.selected = true;
				this.nSelected++;
				break;
			}
			geometry.UpdateTriangles();
		}
	}
	this.callCB = true;

	this.callBack();
};

// endregion

var selectorTool = new SelectorTool();
