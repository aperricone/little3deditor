function Views()
{
	var v = main.menuBar.AddItem("View");
	var sm = v.AddSubMenu();
	var tc = this;
	sm.AddItem("Aligned X+").onClick = function() { tc.SetView(V3(1, 0, 0)); };
	sm.AddItem("Aligned Y+").onClick = function() { tc.SetView(V3(0, 1, 0)); };
	sm.AddItem("Aligned Z+").onClick = function() { tc.SetView(V3(0, 0, 1)); };
	sm.AddItem("Aligned X-").onClick = function() { tc.SetView(V3(-1, 0, 0)); };
	sm.AddItem("Aligned Y-").onClick = function() { tc.SetView(V3(0, -1, 0)); };
	sm.AddItem("Aligned Z-").onClick = function() { tc.SetView(V3(0, 0, -1)); };
	var sep = sm.AddSeparator();
	var normal = sm.AddItem("Aligned Normal");
	normal.onClick = function() { tc.SetView(selectorTool.normal); };

	sep.hide();
	normal.hide();

	selectorTool.AddChangedListener(function()
	{
		var n = selectorTool.normal;
		if(n.dot(n)==0)
		{
			sep.hide();
			normal.hide();
		} else
		{
			sep.show();
			normal.show();
		}
	});
}

Views.prototype.SetView = function(axis)
{
	var cameraMan = main.cameraMan;
	cameraMan.AnglesFromAxis(axis);
	cameraMan.UpdateCamera(); // updates cam.rg and cam.up
	var cam = main.renderer.camera;
	var bb = main.GetBBox();
	var delta = bb.size;
	var absUp = V3(cam.up);
	for(var i = 0; i<3; i++)
	{
		absUp[i] = Math.abs(absUp[i])
	}
	var size = delta.dot(absUp);
	if(cameraMan.ortho)
	{
		cameraMan.distance = size;
	} else
	{
		// top-bottom is the size at z = near
		//  z=near --> size = top-bottom = 2*zNear*Math.tan(fovY*Math.PI/360.0)
		//  z=1 --> size = 2*Math.tan(fovY*Math.PI/360.0)
		//  z=distance --> size = 2*distance*Math.tan(fovY*Math.PI/360.0)
		// distance = size / (2*Math.tan(fovY*Math.PI/360.0))
		cameraMan.distance = 1.2*size/( 2*Math.tan(cameraMan.fov*Math.PI/360.0) );
	}
	cameraMan.UpdateCamera();
};

views = new Views();