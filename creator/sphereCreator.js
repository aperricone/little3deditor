function SphereCreator()
{
	this.Init("Sphere");
}


SphereCreator.prototype = new Creator();
SphereCreator.prototype.constructor = SphereCreator;

SphereCreator.prototype.ShowForm = function()
{
	var tc = this;
	simpleNumericForm.title = "Create a sphere";
	simpleNumericForm.AddNumericInput("Radius", 1.0, 0.1);
	simpleNumericForm.AddNumericInput("Sections", 16);
	simpleNumericForm.AddNumericInput("Slices", 8);
	simpleNumericForm.onOK = function() { tc.CreateSphere() };
	simpleNumericForm.Show();
};

/**
 *
 * @param [radius]
 * @param [nSections]
 * @param [nSlice]
 * @constructor
 */
SphereCreator.prototype.CreateSphere = function(radius, nSections, nSlice)
{
	this.nCreated++;
	radius = radius || simpleNumericForm.GetValue(0);
	nSlice = nSlice || Math.round(simpleNumericForm.GetValue(1));
	nSections = nSections || Math.round(simpleNumericForm.GetValue(2));
	var sphere = new Geometry(main.renderer);
	var stepA = Math.PI/nSections;
	var stepB = Math.PI*2/nSlice;
	var a = stepA;
	sphere.AddVertex(0, radius, 0);
	var v = [0, 0, 0];
	for(var i = 1; i<nSections; i++)
	{
		var b = 0;
		for(var j = 0; j<nSlice; j++)
		{
			var sa = radius*Math.sin(a);
			v[1] = radius*Math.cos(a);

			v[0] = sa*Math.cos(b);
			v[2] = sa*Math.sin(b);

			sphere.AddVertex(v);

			b += stepB;
		}
		a += stepA;
	}
	var lastId = sphere.AddVertex(0, -radius, 0);

	/**
	 * @return {number}
	 */
	function GetId(section, slice)
	{
		if(section==0)
		{
			return 0;
		} else if(section==nSections)
		{
			return lastId;
		}
		if(slice==nSlice) slice = 0;
		return 1+(section-1)*nSlice+slice;
	}

	var id = [0, 0, 0, 0];
	for(var i = 0; i<nSections; i++)
		for(var j = 0; j<nSlice; j++)
		{
			id[0] = GetId(i, j);
			id[1] = GetId(i, j+1);
			id[2] = GetId(i+1, j);
			id[3] = GetId(i+1, j+1);
			if(i==0)
			{
				sphere.AddFace(id[0], id[3], id[2]);
			} else if(i==nSections-1)
			{
				sphere.AddFace(id[0], id[1], id[2]);
			} else
			{
				sphere.AddFace(id[0], id[1], id[3], id[2]);
			}
		}
	sphere.FillCrossReferences();
	sphere.UpdateTriangles();
	sphere.name = 'Sphere'+this.nCreated;
	main.AddGeometry(sphere);

	new CreateUndo();
};

sphereCreator = new SphereCreator();
