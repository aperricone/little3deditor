function CreateUndo()
{
	undoRedo.AddUndo(this);
}

CreateUndo.prototype.Undo = function()
{
	this.created = main.RemoveGeometry();
};

CreateUndo.prototype.Redo = function()
{
	main.AddGeometry(this.created);
};

function RemoveUndo(geoms)
{
	undoRedo.AddUndo(this);
	this.geoms = geoms;
}

RemoveUndo.prototype.Undo = function()
{
	for(var i = 0; i<this.geoms.length; i++)
	{
		main.AddGeometry(this.geoms[i]);
	}
};

RemoveUndo.prototype.Redo = function()
{
	for(var i = 0; i<this.geoms.length; i++)
	{
		main.RemoveGeometry(this.geoms[i]);
	}
};

