function CylinderCreator()
{
	this.Init("Cylinder");
}

CylinderCreator.prototype = new Creator();
CylinderCreator.prototype.constructor = CylinderCreator;

CylinderCreator.prototype.ShowForm = function()
{
	var tc = this;
	simpleNumericForm.title = "Create a Cylinder";
	simpleNumericForm.AddNumericInput("Radius", 1.0, 0.1);
	simpleNumericForm.AddNumericInput("Height", 1.0, 0.1);
	simpleNumericForm.AddNumericInput("Sides", 16.0, 1);
	simpleNumericForm.onOK = function() { tc.createCylinder() };
	simpleNumericForm.Show();
};

CylinderCreator.prototype.createCylinder = function(radius, height, nSides)
{
	this.nCreated++;
	radius = radius || simpleNumericForm.GetValue(0);
	height = height || simpleNumericForm.GetValue(1);
	nSides = nSides || Math.round(simpleNumericForm.GetValue(2));
	var cylinder = new Geometry(main.renderer);

	var step = 2*Math.PI/nSides;
	var a =0;
	var v = [0, 0, 0];
	for(var i = 0; i<nSides; i++)
	{
		v[0] = radius*Math.sin(a);
		v[1] = 0;
		v[2] = radius*Math.cos(a);
		cylinder.AddVertex(v);
		v[1] = height;
		cylinder.AddVertex(v);
		a += step;
	}

	var top = [],bottom = [];
	for(var i = 0; i<nSides; i++)
	{
		top.push(i*2+1);
		bottom.push((nSides-1-i)*2);
		var j = i+1;
		if( j== nSides ) j = 0;
		cylinder.AddFace(i*2+1, i*2, j*2, j*2+1);
	}
	top = cylinder.AddFace(top);
	bottom = cylinder.AddFace(bottom);
	cylinder.SetFaceEdgesHardness(top, true);
	cylinder.SetFaceEdgesHardness(bottom, true);
	cylinder.FillCrossReferences();
	cylinder.UpdateTriangles();
	cylinder.name = 'Cylinder'+this.nCreated;

	main.AddGeometry(cylinder);
	new CreateUndo();
};

cubeCreator = new CylinderCreator();
