function Creator()
{
}

Creator.prototype.Init = function(name)
{
	var tc = this;
	var item = main.contextMenu.AddItem(name);
	item.onClick = function() { tc.ShowForm(); };

	selectorTool.AddChangedListener(function()
	{
		if(selectorTool.nSelected==0)
			item.show();
		else
			item.hide()
	});

	this.nCreated = 0;
}

Creator.prototype.ShowForm = function()
{
	throw "This must be overloaded!"
};
