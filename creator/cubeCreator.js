function CubeCreator()
{
	this.Init("Cube");
}

CubeCreator.prototype = new Creator();
CubeCreator.prototype.constructor = CubeCreator;

CubeCreator.prototype.ShowForm = function()
{
	var tc = this;
	simpleNumericForm.title = "Create a cube";
	simpleNumericForm.AddNumericInput("X Edge lengths", 2.0, 0.1);
	simpleNumericForm.AddNumericInput("Y Edge lengths", 2.0, 0.1);
	simpleNumericForm.AddNumericInput("Z Edge lengths", 2.0, 0.1);
	simpleNumericForm.onOK = function() { tc.createCube() };
	simpleNumericForm.Show();
};

CubeCreator.prototype.createCube = function(xSize, ySize, zSize)
{
	this.nCreated++;
	xSize = xSize || simpleNumericForm.GetValue(0);
	ySize = ySize || simpleNumericForm.GetValue(1);
	zSize = zSize || simpleNumericForm.GetValue(2);

	xSize /= 2;
	ySize /= 2;
	zSize /= 2;
	var cube = new Geometry(main.renderer);
	cube.AddVertex(-xSize, -ySize, -zSize); //0
	cube.AddVertex(xSize, -ySize, -zSize); //1
	cube.AddVertex(-xSize, -ySize, zSize); //2
	cube.AddVertex(xSize, -ySize, zSize); //3
	cube.AddVertex(-xSize, ySize, -zSize); //4
	cube.AddVertex(xSize, ySize, -zSize); //5
	cube.AddVertex(-xSize, ySize, zSize); //6
	cube.AddVertex(xSize, ySize, zSize); //7
	cube.AddFace(0, 1, 3, 2);
	cube.AddFace(4, 6, 7, 5);
	cube.AddFace(0, 4, 5, 1);
	cube.AddFace(2, 3, 7, 6);
	cube.AddFace(1, 5, 7, 3);
	cube.AddFace(0, 2, 6, 4);
	for(var i = 0; i<6; i++)
	{
		cube.SetFaceEdgesHardness(i, true);
	}
	cube.FillCrossReferences();
	cube.UpdateTriangles();
	cube.name = 'Cube'+this.nCreated;

	main.AddGeometry(cube);
	new CreateUndo();
};

cubeCreator = new CubeCreator();
