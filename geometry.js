function Geometry(rr)
{
	/** @type {Geometry.Vertex[]} */
	this.vertices = [];
	/** @type {Geometry.Edge[]} */
	this.edges = [];
	this.edgesMap = {};
	/** @type {Geometry.Face[]} */
	this.faces = [];

	this.name = '';
	this.selected = false;
	this.lock = false;
	this.visible = true;
	this.wire = false;

	this.drawer = new Geometry.GeometryDraw(this,rr);
}

//region Vertex

Geometry.Vertex = function(geomParent)
{
	this.parent = geomParent;
	this.faces = [];
	this.selected = false;
	this.marked = false;
	this.normal = V3(0, 1, 0);
	_Vector.call(this, 3);
};

Geometry.Vertex.prototype = new _Vector(0);
Geometry.Vertex.prototype.constructor = Geometry.Vertex;

/**
 *
 * @param start
 * @param direx
 * @returns {Object}
 */
Geometry.Vertex.prototype.RayDistance = function(start, direx)
{
	var deltaStart = V3().sub(this, start);
	var time = deltaStart.dot(direx)/direx.dot(direx);
	var delta = V3(direx).scale(time).sub(deltaStart);
	return { distance: Math.sqrt(delta.dot(delta)), time: time };
};

Geometry.Vertex.prototype.IsInsideConvexHull = function(planes)
{
	for(var i = 0; i<planes.length; i++)
	{
		var plane = planes[i];
		if(plane.Check(this)<0)
			return false;
	}
	return true;
};

//endregion

//region Edge

Geometry.Edge = function(geomParent, a, b)
{
	this.parent = geomParent;
	if(a<b)
		this.indices = [a, b]; else
		this.indices = [b, a];
	this.faces = [undefined, undefined];
	this.hardness = false;
	this.selected = false;
	this.marked = false;
};

Geometry.Edge.prototype.RayDistance = function(start, direx)
{
	var thisA = this.parent.vertices[this.indices[0]];
	var thisB = this.parent.vertices[this.indices[1]];
	var thisDirex = V3().sub(thisB, thisA);
	// let's call
	// start -> s
	// direx -> d
	// thisA -> a
	// thisDirex -> b
	// timeOnArgs -> timeOnArgs
	// timeOnThis -> timeOnThis
	// see:
	// P = s + d*timeOnArgs (1)
	// Q = a + b*timeOnThis (2)
	// find P1 and Q1 -> (P1-Q1).d == 0 (3) && (P1-Q1).b == 0 (4)
	// so: P1-Q1 perpendicular to both rays, it means they are the 2 nearest point of rays
	// P1-Q1 = s + d*timeOnArgs - (a + b*timeOnThis) = (s-a) + d*timeOnArgs - b*timeOnThis (5) //substitute P and Q with formulas (1) and (2)
	// (P1-Q1).d = (s-a).d + d.d*timeOnArgs - b.d*timeOnThis = 0 // substitute in (3) the (5)
	// (P1-Q1).b = (s-a).b + d.b*timeOnArgs - b.b*timeOnThis = 0 // substitute in (4) the (5)
	// l = -(s-a).d --- m = d.d --- n =-b.d
	// o = -(s-a).b --- p = d.b --- q =-b.b
	var startDiff = V3().sub(start, thisA);
	var l = -startDiff.dot(direx);
	var m = direx.dot(direx);
	var n = -thisDirex.dot(direx);
	var o = -startDiff.dot(thisDirex);
	var p = direx.dot(thisDirex);
	var q = -thisDirex.dot(thisDirex);
	// m*timeOnArgs + n*timeOnThis = l
	// p*timeOnArgs + q*timeOnThis = o // 2 equation with 2 unknowns
	var det = m*q-p*n;
	var detT = l*q-o*n;
	var detR = m*o-p*l;
	if(det==0) return { distance: Infinity, time: Infinity };
	var timeOnArgs = detT/det;
	var timeOnThis = detR/det;
	if(timeOnThis<0 || timeOnThis>1) return { distance: Infinity, time: Infinity };
	if(timeOnArgs<0) return { distance: Infinity, time: Infinity };
	var P1 = V3(direx).scale(timeOnArgs).add(start);
	var Q1 = V3(thisDirex).scale(timeOnThis).add(thisA);
	var delta = V3(P1).sub(Q1);

	return { distance: Math.sqrt(delta.dot(delta)), time: timeOnArgs };
};

/**
 *
 * @param idx
 * @returns {Geometry.Vertex|undefined}
 */
Geometry.Edge.prototype.GetPoint = function(idx)
{
	if(idx==0)
		return this.parent.vertices[this.indices[0]];
	if(idx==1)
		return this.parent.vertices[this.indices[1]];
	return undefined;
};

Geometry.Edge.prototype.__defineGetter__("normal", function()
{
	var n = V3();
	for(var i = 0; i<this.faces.length; i++)
	{
		var face = this.parent.faces[this.faces[i]];
		n.add(face.normal);
	}
	return n.unit();
});

Geometry.Edge.prototype.IsInsideConvexHull = function(planes)
{
	for(var j = 0; j<this.indices.length; j++)
	{
		var vtx = this.GetPoint(j);
		for(var i = 0; i<planes.length; i++)
		{
			var plane = planes[i];
			if(plane.Check(vtx)<0)
				return false;
		}
	}
	return true;
};

//endregion

//region Face

/**
 *
 * @param {Geometry} geomParent
 * @param {Number[]} indices
 */
Geometry.Face = function(geomParent, indices)
{
	/**
	 * @type {Geometry}
	 */
	this.parent = geomParent;
	/**
	 * @type {Number[]}
	 */
	this.indices = indices.slice();
	//this.Triangulate();
	this.selected = false;
	this.marked = false;

	this.normals = [];
	this.center = V3();
	this.dirty = true;
	for(var i = 0; i<this.indices.length; i++)
	{
		this.normals.push(V3())
	}
};

Geometry.Face.prototype.Triangulate = function()
{
	var tmp = [];
	this.center.Set();
	for(var i = 0; i<this.indices.length; i++)
	{
		var point = this.GetPoint(i);
		this.center.add(point);
		tmp.push(point);
	}
	this.center.scale(1/this.indices.length);
    try
    {
	    this.triangles = Triangulate(tmp);
    } catch(e)
    {
        this.triangles = [];
    }
	this.UpdateTriangles();
};

/**
 * Ask to parent for point in face
 * @param {Number} idx index of point inside internal array
 * @returns {Geometry.Vertex}
 */
Geometry.Face.prototype.GetPoint = function(idx)
{
	return this.parent.vertices[this.indices[idx]];
};

Geometry.Face.prototype.UpdateTriangles = function()
{
	this.normal = V3();
	for(var i = 0; i<this.triangles.length; i++)
	{
		var tri = this.triangles[i];
		var a = this.GetPoint(tri[0]);
		var b = this.GetPoint(tri[1]);
		var c = this.GetPoint(tri[2]);
		if(!('plane' in tri))
		{
			tri.plane = new Plane();
			tri.planeAB = new Plane();
			tri.planeBC = new Plane();
			tri.planeCA = new Plane();
		}
		tri.plane.Set(a, b, c);
		tri.planeAB.Set(b, a, V3(a).add(tri.plane.normal));
		tri.planeBC.Set(c, b, V3(b).add(tri.plane.normal));
		tri.planeCA.Set(a, c, V3(c).add(tri.plane.normal));
		this.normal.add(tri.plane.normal)
	}
	this.normal.unit();
};

Geometry.Face.prototype.RayDistance = function(start, direx)
{
	//var nearestTime; support concave face?
	for(var i = 0; i<this.triangles.length; i++)
	{
		var tri = this.triangles[i];

		var t = tri.plane.IntersectWithLine(start, direx);
		if(t<0 || t==Infinity) continue;
		var point = V3(direx).scale(t).add(start);
		if(tri.planeAB.Check(point)<0) continue;
		if(tri.planeBC.Check(point)<0) continue;
		if(tri.planeCA.Check(point)<0) continue;
		return { distance: 0, time: t };
	}
	return { distance: Infinity, time: Infinity };
};

Geometry.Face.prototype.IsInsideConvexHull = function(planes)
{
	for(var j = 0; j<this.indices.length; j++)
	{
		var vtx = this.GetPoint(j);
		for(var i = 0; i<planes.length; i++)
		{
			var plane = planes[i];
			if(plane.Check(vtx)<0)
				return false;
		}
	}
	return true;
};

// endregion

//region Geometry
/**
 *
 * @param {...number | number[]} _points
 */
Geometry.prototype.AddFace = function(_points)
{
	var id = this.faces.length;
	//noinspection JSValidateTypes
	var indices = Array.isArray(_points) ? _points : [].slice.call(arguments, 0);
	this.faces.push(new Geometry.Face(this, indices));
	return id;
};

Geometry.prototype.SetFaceEdgesHardness = function(id, hardness)
{
	var face = this.faces[id];
	for(var j = 0; j<face.indices.length; j++)
	{
		var k = j+1;
		if(k==face.indices.length) k = 0;
		var idx = this.AddEdge(face.indices[j], face.indices[k]);
		this.edges[idx].hardness = hardness;
	}
};

Geometry.prototype.AddEdge = function(e1, e2)
{
	var key;
	if(e1<e2)
		key = e1+","+e2; else
		key = e2+","+e1;
	if(key in this.edgesMap)
	{
		return this.edgesMap[key];
	}
	var r = this.edges.length;
	this.edges.push(new Geometry.Edge(this, e1, e2));
	this.edgesMap[key] = r;
	return r;
};

/**
 * @return {number}
 */
Geometry.prototype.GetEdge = function(e1, e2)
{
	var key;
	if(e1<e2)
		key = e1+","+e2; else
		key = e2+","+e1;
	if(key in this.edgesMap)
	{
		return this.edgesMap[key];
	}
	return -1;
};

Geometry.prototype.AddVertex = function(p)
{
	var r = this.vertices.length;
	var vtx = new Geometry.Vertex(this);
	vtx.Set.apply(vtx, arguments);
	this.vertices.push(vtx);
	return r;
};


Geometry.prototype.UpdateTriangles = function()
{
	this.dirty = false;
	// It dirties all faces with dirty vertices, dirties this too if 1 vertex is dirty
	for(var i = 0; i<this.vertices.length; i++)
	{
		var vtx = this.vertices[i];
		if(vtx.dirty)
		{
			this.dirty = true;
			for(var j = 0; j<vtx.faces.length; j++)
			{
				var face = this.faces[vtx.faces[j]];
				face.dirty = true;
			}
		}
	}
	// It cleans all dirty faces, triangulating them
	for(var i = 0; i<this.faces.length; i++)
	{
		var face = this.faces[i];
		if(face.dirty)
		{
			this.dirty = true;
			face.Triangulate();
			// It propagates the dirtiness to vertices
			for(var j = 0; j<face.indices.length; j++)
			{
				var vtx = this.vertices[face.indices[j]];
				vtx.dirty = true;
			}
		}
		face.dirty = false;
	}
	if(!this.dirty)  return;
	// It recalculates the vertices normal
	for(var i = 0; i<this.vertices.length; i++)
	{
		var vtx = this.vertices[i];
		if(vtx.dirty)
		{
			// Clear vtx normal
			vtx.normal.Set();
			// Add all faces normal
			for(var j = 0; j<vtx.faces.length; j++)
			{
				var face = this.faces[vtx.faces[j]];
				vtx.normal.add(face.normal);
			}
			// DON't MERGE THIS 2 FOR: THE SECOND ONE NEEDS NORMAL WITH ALL FACES!
			for(var j = 0; j<vtx.faces.length; j++)
			{
				var faceId = vtx.faces[j];
				var face = this.faces[ faceId];
				var idx = face.indices.indexOf(i);
				var prevIdx = idx-1;
				if(prevIdx== -1) prevIdx = face.indices.length-1;
				var nextIdx = idx+1;
				if(nextIdx==face.indices.length) nextIdx = 0;
				prevIdx = face.indices[prevIdx];
				nextIdx = face.indices[nextIdx];
				var edges = [this.GetEdge(prevIdx, i), this.GetEdge(nextIdx, i) ];
				edges[0] = this.edges[edges[0]];
				edges[1] = this.edges[edges[1]];
				if(edges[0].hardness==true && edges[1].hardness==true)
				{ // both edges are hard
					face.normals[idx].Set(face.normal);
				} else if(edges[0].hardness==false && edges[1].hardness==false)
				{ // both edges are soft
					face.normals[idx].Set(vtx.normal);
				} else if(edges[0].hardness==true || edges[1].hardness==true)
				{
					face.normals[idx].Set(face.normal);
					for(var k = 0; k<2; k++)
					{
						if(edges[k].hardness==false)
						{
							var otherFaceId = edges[k].faces[0]!=faceId ? edges[k].faces[0] : edges[k].faces[1];
							var otherFace = this.faces[otherFaceId];
							face.normals[idx].add(otherFace.normal);
						}
					}
				}
				face.normals[idx].unit();
			}
			// this is local normal
			vtx.normal.unit();
			vtx.dirty = false;
		}
	}
};


Geometry.prototype.FillCrossReferences = function()
{
	// for all vertices clears faces id
	for(var i = 0; i<this.vertices.length; i++)
	{
		var vtx = this.vertices[i];
		vtx.faces = [];
	}
	// for all edge clears faces id
	for(var i = 0; i<this.edges.length; i++)
	{
		var edge = this.edges[i];
		edge.faces = [undefined, undefined];
	}
	// for all faces sets references
	for(var i = 0; i<this.faces.length; i++)
	{
		var face = this.faces[i];
		for(var j = 0; j<face.indices.length; j++)
		{
			var vtx = this.vertices[face.indices[j]];
			vtx.faces.push(i);

			var k = j+1;
			if(k==face.indices.length) k = 0;
			var idx = this.AddEdge(face.indices[j], face.indices[k]);
			var edge = this.edges[idx];
			var destinationId = 0;
			var msg = "right";
			if(edge.indices[0]!=face.indices[j])
			{
				destinationId = 1;
				msg = "left";
			}
			if(edge.faces[destinationId]!=undefined) throw "this edge already had a "+msg+" face";
			edge.faces[destinationId] = i;
		}
	}
	// check vertices
	for(var i = this.vertices.length-1; i>=0; i--)
	{
		var vtx = this.vertices[i];
		if(vtx.faces.length==0)
		{
			this.RemoveVertex(i);
		}
	}
	// check edges
	var needAnotherCall = false;
	this.edgesMap = {};
	for(var i = 0; i<this.edges.length; i++)
	{
		var edge = this.edges[i];
		if(edge.faces[0]==undefined && edge.faces[1]==undefined)
		{
			this.edges.splice(i, 1);
			i--;
		} else
		{
			var key = edge.indices[0]+","+edge.indices[1];
			this.edgesMap[key] = i;
		}
	}
	for(var i = 0; i<this.edges.length; i++)
	{
		var edge = this.edges[i];
		if(edge.faces[0]==undefined || edge.faces[1]==undefined)
		{
			// fill hole
			var newFace;
			if(edge.faces[0]==undefined)
			{
				newFace = [edge.indices[0], edge.indices[1]];
			} else
			{
				newFace = [edge.indices[1], edge.indices[0]];
			}
			var prevIdx = newFace[0];
			var currIdx = newFace[1];
			var otherEdges = [i];
			while(currIdx!=newFace[0])
			{
				for(var j = i+1; j<this.edges.length; j++)
				{
					var edge = this.edges[j];
					if(edge.faces[0]==undefined || edge.faces[1]==undefined)
					{
						if((edge.indices[0]==currIdx && edge.indices[1]!=prevIdx) || (edge.indices[1]==currIdx && edge.indices[0]!=prevIdx))
						{
							var id = edge.indices[0]==currIdx ? 1 : 0;
							prevIdx = currIdx;
							currIdx = edge.indices[id];
							newFace.push(currIdx);
							otherEdges.push(j);
							break;
						}
					}
				}
			}
			newFace.splice(-1, 1);
			var newFaceId = this.AddFace(newFace);
			for(var j = 0; j<otherEdges.length; j++)
			{
				var edge = this.edges[otherEdges[j]];
				if(edge.faces[0]==undefined)
					edge.faces[0] = newFaceId; else
					edge.faces[1] = newFaceId;
			}
			needAnotherCall = true;
		}
	}
	if(needAnotherCall)
	{
		this.FillCrossReferences();
		this.UpdateTriangles();
	}
};

Geometry.prototype.RemoveVertex = function(idx)
{
	var vtx = this.vertices[idx];
	// remove from vertices list
	this.vertices.splice(idx, 1);
	var faceToRemove = [];
	// remove from faces
	for(var i = 0; i<vtx.faces.length; i++)
	{
		var faceId = vtx.faces[i];
		var face = this.faces[ faceId];
		if(face)
		{
			if(face.indices.length==3)
			{
				faceToRemove.push(faceId)
			} else
			{
				var vertexIdx = face.indices.indexOf(idx);
				var vBefore = vertexIdx==0 ? face.indices[face.indices.length-1] : face.indices[vertexIdx-1];
				var vAfter = vertexIdx==face.indices.length-1 ? face.indices[0] : face.indices[vertexIdx+1];
				face.indices.splice(vertexIdx, 1);
				face.dirty = true;
				var edgeBefore = this.GetEdge(vBefore, idx);
				var edgeAfter = this.GetEdge(idx, vAfter);
				if(edgeBefore!= -1 && edgeAfter!= -1 && this.edges[edgeBefore].hardness==true && this.edges[edgeAfter].hardness==true)
				{
					var newEdge = this.AddEdge(vBefore, vAfter);
					this.edges[newEdge].hardness = true;
				}
			}
		}
	}
	// change face indices
	for(var i = 0; i<this.faces.length; i++)
	{
		var face = this.faces[i];
		for(var j = 0; j<face.indices.length; j++)
		{
			if(face.indices[j]>idx) face.indices[j]--;
		}
	}
	// change edge indices
	for(var i = 0; i<this.edges.length; i++)
	{
		var edge = this.edges[i];
		var remap = false;
		var key = edge.indices[0]+","+edge.indices[1];
		for(var j = 0; j<2; j++)
		{
			if(edge.indices[j]>idx)
			{
				edge.indices[j]--;
				remap = true;
			}
		}
		if(remap)
		{
			if(this.edgesMap[key]==i)
				delete this.edgesMap[key];

			key = edge.indices[0]+","+edge.indices[1];
			this.edgesMap[key] = i;
		}
	}
	// remove degenerate faces
	faceToRemove.sort();
	for(var i = faceToRemove.length-1; i>=0; i--)
	{
		var idx = faceToRemove[i];
		this.faces.splice(idx, 1);
		for(var j = 0; j<this.vertices.length; j++)
		{
			var vtx = this.vertices[j];
			for(var k = vtx.faces.length-1; k>=0; k--)
			{
				if(vtx.faces[k]==idx)
				{
					vtx.faces.splice(k, 1);
				} else if(vtx.faces[k]>idx)
				{
					vtx.faces[k]--;
				}
			}
		}
	}
};

Geometry.prototype.GetBBox = function()
{
	var ret = new BBox(this.vertices[0]);
	for(var i = 1; i<this.vertices.length; i++)
	{
		ret.Grow(this.vertices[i]);
	}
	return ret;
};

Geometry.prototype.Copy = function(other)
{
	// copied from constructor
	this.vertices = [];
	this.edges = [];
	this.edgesMap = {};
	this.faces = [];
	this.name = other.name;
	this.selected = other.selected;
	this.lock = other.lock;
	this.visible = other.visible;
	this.wire = other.wire;

	// copy Vertices
	for(var i = 0; i<other.vertices.length; i++)
	{
		this.AddVertex(other.vertices[i]);
		this.vertices[i].selected = other.vertices[i].selected;
	}
	// copy Faces
	for(var i = 0; i<other.faces.length; i++)
	{
		this.AddFace(other.faces[i].indices);
		this.faces[i].selected = other.faces[i].selected;
	}
	// copy edges
	for(var i = 0; i<other.edges.length; i++)
	{
		this.AddEdge(other.edges[i].indices[0], other.edges[i].indices[1]);
		this.edges[i].hardness = other.edges[i].hardness;
		this.edges[i].selected = other.edges[i].selected;
	}
	// end
	this.FillCrossReferences();
	this.UpdateTriangles();
};

Geometry.prototype.IsInsideConvexHull = function(planes)
{
	for(var j = 0; j<this.vertices.length; j++)
	{
		var vtx = this.vertices[j];
		for(var i = 0; i<planes.length; i++)
		{
			var plane = planes[i];
			if(plane.Check(vtx)<0)
				return false;
		}
	}
	return true;
};


//endregion
